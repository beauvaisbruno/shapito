// @flow

import { Dimensions, PixelRatio } from "react-native";

/**
 *
 * @param pc percent of the screen width
 * @returns {number} pixel
 */
export const wp = (pc: number) => {
  return PixelRatio.roundToNearestPixel((
    Dimensions.get("window").width * pc) / 100);
};

export const padding = (pc: number) => {
  const value = wp(pc);
  return {
    paddingBottom: value,
    paddingRight: value,
    paddingLeft: value,
    paddingTop: value,
  };
};

export const marginT = (pc: number) => {
  const value = wp(pc);
  return {
    marginRight: value,
    marginLeft: value,
    marginTop: value,
  };
};
export const margin = (pc: number) => {
  const value = wp(pc);
  return {
    marginBottom: value,
    marginRight: value,
    marginLeft: value,
    marginTop: value,
  };
};

export const colors = {
  errorMsg: "#4c0000",
  successMsg: "#084c00",
  successBg: "#89ff76",
  link: "#00104c",
  text: "#1f1f1f",
  textGrey: "#666666",
  buttonBg: "#c8c8c8",
  userNoColor: "#939aff",
  disabledBg: "#bcbcbc",
  lightBg: "#e5e5e5",
  lineBorder: "#bababa",
  avatarBorderDefault: "#1f1f1f",

};

export const css = {
  input: {
    fontSize: wp(5), ...padding(4),
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: wp(5),
    textAlign: "center",
  },
  button: {
    fontSize: wp(5),
    ...padding(4),
    borderRadius: wp(5),
    textAlign: "center",
    marginTop: wp(5),
  },
  textError: {
    fontSize: wp(5),
    textAlign: "center", ...margin(5),
    color: colors.errorMsg,
  },

};

