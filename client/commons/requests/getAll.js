//@flow

import type { GetAllResponse } from "../../../server/functions/src/getAll";
import { getAppState, setAppState } from "../appState";
import { remote } from "./remote";

export function getAll (force: boolean = false): Promise<void> {
  return new Promise((resolve, reject) => {
    const lastGetAllTime = getAppState(appState => appState.lastGetAllTime);
    if (!force
      && lastGetAllTime !== undefined
      && Date.now() < lastGetAllTime + 60 * 1000) {
      console.log("getAll blocked, wait: ", (lastGetAllTime + 60 * 1000) - Date.now(), "ms");
      resolve();
      return;
    }
    setAppState(appState => appState.lastGetAllTime = Date.now());
    remote("getAll")
      .then(({ user, contacts, items }: GetAllResponse) => {
        setAppState((appState) => {
          appState.user = user;
          appState.contacts = { ...appState.contacts, ...contacts };
          appState.items = { ...appState.items, ...items };
        });
        resolve();
      }).catch((error) => {
      reject(error);
    });
  });
}
