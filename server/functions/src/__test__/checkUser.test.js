import { checkUser } from "../commons/checkUser";
import { firestore } from "../commons/firebase";
import { resetFirestore } from "./resetFirestore";

describe("checkUser", () => {
  beforeEach(async () => {
    await resetFirestore();
    jest.clearAllMocks();
  });
  const USER_EMAIL = "user@mail.com";
  const USER_UID = "uid";
  const user = {
    uid: USER_UID,
    email: USER_EMAIL,
  };
  it("checkUser good uid", async (done) => {
    await firestore.collection("TheUsers").doc(USER_EMAIL).set(user);

    const result = await checkUser(({
      request: { email: USER_EMAIL },
      context: { auth: { uid: USER_UID } },
    }));

    expect(result).toEqual(user);
    done();
  });
  it("checkUser wrong uid", async (done) => {
    await firestore.collection("TheUsers").doc(USER_EMAIL).set(user);
    try {
      await checkUser({
        request: { email: USER_EMAIL },
        context: { auth: { uid: "realUid" } },
      });
      fail();
    } catch (error) {
      done();
    }
  });
});
