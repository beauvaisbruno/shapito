#!/usr/bin/env sh

set -e # exit on first failed commandset
set -x # print all executed commands to the log

if [ "$FCI_BUILD_STEP_STATUS" == "success" ]
then
  git tag $BUILD_NUMBER
  git push "https://beauvaisbruno:$REPOSITORY_PASSWORD@bitbucket.org/BeauvaisBruno/shapito.git" --tags
fi
