//@flow
import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { colors, margin, padding, wp } from "../../commons/css";
import { avatars } from "../../model/avatars/avatar";
import type { TheUser } from "../../model/TheUser";

export function FriendContact ({
  contact,
  onContactSelected,
}: {
  contact: TheUser,
  onContactSelected: (TheUser)=>void,
}) {

  let avatarSource = require("../../model/avatars/defaultAvatar.png");
  if (contact.avatar) avatarSource = avatars[contact.avatar];
  let color = colors.avatarBorderDefault;
  if (contact.color) color = contact.color;

  return <TouchableOpacity
    onPress={() => {
      onContactSelected(contact);
    }}
    key={contact.email}
    style={{
      width: wp(100),
      borderBottomColor: colors.lineBorder,
      borderBottomWidth: wp(0.1),
      flexDirection: "row",
      alignItems: "center",
    }}>

    <Image
      source={avatarSource}
      style={{
        width: wp(21),
        height: wp(21),
        borderColor: color,
        borderWidth: wp(1),
        borderRadius: wp(21), ...margin(2), ...padding(1),
      }}
    />
    <View style={{ flex: 1, ...margin(2) }}>
      <Text style={{
        fontSize: wp(5),
        fontWeight: "bold",
      }}>{contact.pseudo}</Text>
      <Text style={{ fontSize: wp(5) }}>{contact.email}</Text>

    </View>
  </TouchableOpacity>;
}
