//@flow
import AsyncStorage from "@react-native-async-storage/async-storage";
import clonedeep from "lodash.clonedeep";
import { useEffect, useReducer, useRef } from "react";
import type { AppState } from "./defaultAppState";
import { defAppState } from "./defaultAppState";
import DEV from "./DEV";
import { values } from "./util";

/** USAGE
 useEffect(() => {
    // first launch values
    initAppState(defaultAppStateValues);
    // replace defaultAppStateValues by previous session values
    asyncRehydrateAppState().then(() => {
      //callback to mount components using appState values
    });
  }, []);
 ...
 // update component when appState.someValue change (use !== comparator)
 const someValue = useAppState(appState => appState.someValue);
 ...
 // update appState.someValue
 setAppState(appState => appState.someValue = "newValue");
 ...
 // get someValue as a deep clone
 getAppState(appState => appState.someValue);
 */

let selectors: {
  [string]: {
    selectorFn: AppState=>any,
    selectorValue: any,
    update: ()=>void,
  }
} = {};
let nextSelectorId = 0;
let appState: AppState = defAppState;

export const initAppState = (initState: AppState) => {
  // console.log("initAppState, items: ",Object.keys(appState.items).length,"=>",Object.keys(initState.items).length);
  appState = clonedeep(initState);
  selectors = {};
  nextSelectorId = 0;
};

const APP_STATE_PERSIST_KEY = "app_state_persist_key";

export const rehydrateAppStateAsync = async () => {
  try {
    const jsonValue = await AsyncStorage.getItem(APP_STATE_PERSIST_KEY);
    // console.log("asyncRehydrateAppState, jsonValue: ", jsonValue);
    if (jsonValue != null) {
      appState = { ...appState, ...JSON.parse(jsonValue) };
    } else {
      console.log("asyncRehydrateAppState, stored value is empty");
    }
  } catch (e) {
    console.log("asyncRehydrateAppState, error: ", e);
  }
};

/**
 * Get someValue as a deep clone
 * getAppState(appState => appState.someValue);
 */
export function getAppState<T> (getFn: (AppState)=>T): T {
  return clonedeep(getFn(appState));
};

export function getAppRootState (): AppState {
  return clonedeep(appState);
};

/**
 * Rerender the component when the value returned by selectorFn changes
 * @returns {*}
 */
export function useAppState<T> (selectorFn: (AppState)=>T, comp?: Node): T {
  const selectorValue = selectorFn(appState);
  // console.log("useAppState: ", selectorValue);
  const selectorId = useRef(nextSelectorId);
  const update = useReducer(function (s) {
    return s + 1;
  }, 0)[1];

  if (selectorId.current === nextSelectorId) {
    // console.log("useAppState, create nextSelectorId: ", nextSelectorId);
    nextSelectorId++;
  }

  useEffect(() => {
    selectors[selectorId.current + ""] = {
      selectorFn,
      selectorValue,
      update,
    };
    // console.log("useAppState, selectors: ", selectors);
    return () => {
      if (selectors[selectorId.current + ""]) delete selectors[selectorId.current + ""];
    };
  });

  return selectorValue;
}

/**
 * Update the global appState and rerender components using updated fields
 * @param mutableFn
 */
export function setAppState (mutableFn: (AppState)=>any) {
  const prev = Object.keys(appState.items).length;
  mutableFn(appState);
  // console.log("setAppState, items: ",prev,"=>",Object.keys(appState.items).length);
  // console.log("setAppState, selectors: ", selectors);
  // let updateCount = 0;
  values(selectors).forEach(selectorObj => {
    if (selectorObj.selectorFn(appState) !== selectorObj.selectorValue) {
      selectorObj.update();
      // updateCount++;
    }
  });
  // console.log("setAppState, updateCount: ",updateCount,", appState: ", appState);
  if (!DEV.story) persistAppState(appState);
}

export const persistAppState = (value: AppState) => {
  setTimeout(() => {
    const jsonValue = JSON.stringify(value);
    AsyncStorage.setItem(APP_STATE_PERSIST_KEY, jsonValue)
      .then(() => {
        // console.log("persistAppState done: ", jsonValue);

      })
      .catch(e => {
        console.log("persistAppState, error: ", e);
      });
  }, 1);
};
