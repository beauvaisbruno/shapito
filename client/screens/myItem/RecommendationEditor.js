//@flow
import React, { useState } from "react";
import { Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { setAppState, useAppState } from "../../commons/appState";
import { LinkToOsContacts } from "../../commons/components/LinkToOsContacts";
import { margin, wp } from "../../commons/css";
import type { LoaderType } from "../../commons/hooks";
import { useLoader } from "../../commons/hooks";
import { remote } from "../../commons/requests/remote";
import type { TheItem } from "../../model/TheItem";
import type { TheUser } from "../../model/TheUser";
import { Invitation } from "../invitation/Invitation";
import { FriendContact } from "./FriendContact";
import type { MyItemSubScreen } from "./MyItem";

function ContactsSelector ({
  item,
  Loader,
  onContactSelected,
  setSubScreen,
  setShowInviteOsContact,
}: {
  item: TheItem,
  setSubScreen: (MyItemSubScreen)=>void,
  Loader: LoaderType,
  onContactSelected: (TheUser)=>void,
  setShowInviteOsContact: (boolean)=>void,
}) {

  const contacts = useAppState(appState => appState.contacts);
  let userContactsEmails = useAppState(appState => appState.user.contactEmails);
  userContactsEmails = [...userContactsEmails].sort((email1, email2) => {
    return contacts[email1].pseudo.localeCompare(contacts[email2].pseudo);
  });

  let selectedContacts = item.recommendedToEmails;
  if (!selectedContacts) selectedContacts = [];
  const rows = [];
  userContactsEmails.forEach((email) => {
    try {
      rows.push(<FriendContact {...{
        contact: contacts[email],
        key: email,
        isSelected: selectedContacts.includes(email),
        selectedIcon: item.noteIcon,
        onContactSelected,
      }} />);
    } catch (error) {
      console.log("error: ", error, ", email: ", email);
    }
  });

  return (<View style={{
    flex: 1,
  }}>
    <Loader />
    <ScrollView
      style={{
        flex: 1,
      }}
      contentContainerStyle={{
        paddingBottom: wp(20),
      }}
    >
      {rows}
      <LinkToOsContacts {...{
        onPress: () => setShowInviteOsContact(true),
        empty: rows.length === 0,
      }} />
    </ScrollView>
    <View
      style={{
        position: "absolute",
        bottom: 0,
        width: wp(100),
        justifyContent: "space-evenly",
        flexDirection: "row",
        paddingBottom: wp(2),
        paddingTop: wp(2),
        backgroundColor: "rgba(255,255,255,0.8)",
      }}
    >
      <TouchableOpacity
        onPress={() => {
          setSubScreen("Main");
        }}
      >
        <Image source={require("../../commons/img/back.png")}
               style={{
                 width: wp(15),
                 height: wp(15),
               }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          setShowInviteOsContact(true);
        }}
      >
        <Image source={require("../../commons/img/inviteFriend.png")}
               style={{
                 width: wp(15),
                 height: wp(15),
               }}
        />
      </TouchableOpacity>
    </View>
  </View>);
}

export function RecommendationEditor ({
  itemId,
  setSubScreen,
}: {
  itemId: string,
  setSubScreen: (MyItemSubScreen)=>void,
}) {

  const item = useAppState(appState => appState.items[itemId]);
  const [showInviteOsContact, setShowInviteOsContact] = useState(false);
  const { Loader, setLoading, setError } = useLoader();

  const updateRecommendation = ({
    contact,
    operation,
  }) => {
    // console.log("updateItem, item: ", item);
    setLoading(true);
    console.log("updateRecommendation contact: ", contact);
    remote("updateRecommendation", {
      itemId,
      contact,
      operation,
    }).then(({ item, contact }) => {
      setLoading(false);
      setAppState(appState => {
        appState.items[item.itemId] = item;
        appState.contacts[contact.email] = contact;
      });
    }).catch((error) => {
      setError(true);
    });
  };

  if (showInviteOsContact) {
    return <Invitation {...{
      onContactSelected: (contact) => {
        console.log("1updateRecommendation contact: ", contact);
        updateRecommendation({
          contact,
          operation: "add",
        });
        setShowInviteOsContact(false);
      }, back: () => setShowInviteOsContact(false),
    }} />;
  }

  return <>
    <View style={{
      flexDirection: "row",
      alignItems: "center",
      ...margin(2),
      marginBottom: wp(2),
    }}>
      <Image source={require("../../commons/img/recommandation.png")}
             style={{
               width: wp(10),
               height: wp(10),
             }} />
      <Text style={{
        fontSize: wp(5),
        marginLeft: wp(2),
      }}>{"Recommandé pour"}</Text>
    </View>
    <ContactsSelector {...{
      item,
      Loader,
      setSubScreen,
      setShowInviteOsContact,
      onContactSelected: (contact) => {
        if (item.recommendedToEmails.includes(contact.email)) {
          updateRecommendation({
            contact,
            operation: "remove",
          });
        } else {
          updateRecommendation({
            contact,
            operation: "add",
          });
        }
      },
    }} />
  </>;

}

