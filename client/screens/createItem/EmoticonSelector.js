//@flow
import React, { useState } from "react";
import { Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { colors, css, margin, wp } from "../../commons/css";
import type { Emoticon } from "../../model/emoticons/emoticons";
import { emoticons } from "../../model/emoticons/emoticons";
import type { ItemType } from "../../model/ItemTypes/itemTypes";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import type { ItemDescription } from "../../model/TheItem";
import { CancelBtn } from "./CancelBtn";

export const EmoticonSelector = ({
  description,
  type,
  setEmoticons,
}: {
  description: ItemDescription,
  type: ItemType,
  setEmoticons: (Array<Emoticon>)=>void,
}) => {
  const [selecteds, setSelecteds] = useState([]);
  const rows = [];
  Object.keys(emoticons).forEach((icon) => {
    const item = emoticons[icon];
    rows.push(<TouchableOpacity
      style={{
        width: wp(25),
        height: wp(25),
        justifyContent: "center",
        alignItems: "center",
        borderRadius: wp(5),
      }} key={icon}
      onPress={() => {
        const index = selecteds.indexOf(icon);
        if (index > -1) {
          selecteds.splice(index, 1);
        } else {
          selecteds.push(icon);
        }
        setSelecteds([...selecteds]);
      }}
    >
      <View {...{
        style: {
          width: wp(24),
          height: wp(24),
          justifyContent: "center",
          alignItems: "center",
          borderRadius: wp(5),
          borderColor: colors.successBg,
          ...(selecteds.includes(icon) ? {
            borderWidth: wp(0.2),
            backgroundColor: colors.successBg,
          } : ({}: any)),
        },
      }}>
        <Image source={item.icon} style={{
          width: wp(20),
          height: wp(20),
        }} />
      </View>
    </TouchableOpacity>);
  });
  const addButtonDisabled = selecteds.length < 1 || selecteds.length > 6;
  return (<View style={{
    flex: 1,
    alignItems: "center",
  }}>
    <View style={{
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center", ...margin(2),
      width: wp(96), // flex: 1,
    }}>
      <Image style={{
        width: wp(15),
        height: wp(15),
      }} source={itemTypes[type].icon} />
      <Text style={{
        fontSize: description.title.length > 14 ? wp(5) : wp(10),
        color: colors.text,
      }}>{description.title}</Text>
      <CancelBtn />
    </View>
    <Text style={{
      fontSize: wp(7),
      color: colors.text,
      textAlign: "center", // flex: 1,
    }}>De quoi ça parle ?</Text>
    <Text style={{
      textAlign: "center",
      fontStyle: "italic",
      fontSize: wp(3),
      color: colors.text,
    }}>
      {"Sélectionnez " + selecteds.length + "/6 émoticônes puis cliquez sur ajouter"}
    </Text>
    <ScrollView
      style={{
        marginTop: wp(4),
        flexGrow: 1,
      }}
      contentContainerStyle={{
        paddingBottom: wp(20),
      }}
    >
      <View style={{
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center",
        flexWrap: "wrap",
        width: wp(100),
      }}>
        {rows}
      </View>
    </ScrollView>
    <TouchableOpacity
      disabled={addButtonDisabled}
      onPress={() => {
        setEmoticons(selecteds);
      }}
    >
      <Text
        style={{
          ...css.button,
          backgroundColor: addButtonDisabled ? colors.disabledBg : colors.successBg,
          width: wp(90), ...margin(1),
          color: "white",
          fontWeight: "bold",
        }}
      >
        Ajouter
      </Text>
    </TouchableOpacity>
  </View>);
};
