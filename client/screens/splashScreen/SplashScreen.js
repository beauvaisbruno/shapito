//@flow
import React from "react";
import { View } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import { wp } from "../../commons/css";

export const SplashScreen = (props: {||}) => {
  return (<View
    style={{
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    }}
  >
    <AutoHeightImage
      width={wp(60)}
      source={require("../../commons/img/splashscreen.gif")} />
  </View>);
};

