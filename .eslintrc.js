module.exports = {
  root: true,
  extends: "@react-native-community",
  rules: {
    quotes: ["warn", "double"],
    "prettier/prettier": ["off"],
    "react-hooks/exhaustive-deps": ["off"],
    "react-native/no-inline-styles": ["off"],
    curly: "off",

  },

};
