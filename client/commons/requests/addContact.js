//@flow
import type { AddContactRequest, AddContactResponse } from "../../../server/functions/src/addContact";
import type { TheUser } from "../../model/TheUser";
import { setAppState } from "../appState";
import { colors } from "../css";
import { remote } from "./remote";

export const addContact = (contact: {| email: string, pseudo: string |}): Promise<TheUser> => {
  return new Promise((resolve, reject) => {
    remote("addContact", ({
      contact: {
        color: colors.userNoColor,
        avatar: "defaultAvatar",
        ...contact,
      },
    }: AddContactRequest))
      .then(({ contact: resultContact, user }: AddContactResponse) => {
        setAppState(appState => {
          appState.contacts = {
            ...appState.contacts,
            [resultContact.email]: resultContact,
          };
          appState.user = user;
        });
        resolve(resultContact);
      }).catch((error) => {
      reject(error);
    });
  });
};
