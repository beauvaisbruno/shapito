//@flow
import { storiesOf } from "@storybook/react-native";
import React from "react";
import { generateContacts, generateMyItems, generateOtherItems } from "../../commons/story/generate";
import { MockScreen } from "../../commons/story/MockScreen";
import { pseudoRndReset } from "../../commons/util";
import type { OtherProfileSubScreen } from "./OtherProfile";

const user = ({
  email: "user@shapito.com",
  color: "#3c51a0",
  avatar: "marie",
  pseudo: "Shapito User",
}: any);

const contact = {
  email: "contact@shapito.com",
  color: "#408c45",
  avatar: "duchesse",
  pseudo: "Shapito Contact",
};
pseudoRndReset();
const contacts = generateContacts({
  nbr: 2,
  contactEmails: ["contact@shapito.com"],
});
const items = {
  ...generateMyItems({
    nbr: 10,
    recommendedByEmail: contact.email,
    recommendedToEmails: { [user.email]: user, ...contacts },
  }), ...generateOtherItems({
    nbr: 10,
    recommendedToEmail: contact.email,
    recommendedByEmails: { [user.email]: user, ...contacts },
  }),
};

storiesOf("OtherProfile", module).add("RecommendedByHim", () => {
  return (<MockScreen
    {...{
      screen: "OtherProfile",
      remote: {
        getAllFromContact: {
          user,
          contacts,
          items,

        },
      },
      appState: {
        user,
        contacts: { [contact.email]: contact },
      },
      screenProps: {
        contact,
      },
    }}
  />);
}).add("RecommendedForMe", () => {
  return (<MockScreen
    {...{
      screen: "OtherProfile",
      remote: {
        getAllFromContact: {
          user,
          contacts,
          items,

        },
      },
      appState: {
        user,
        contacts: { [contact.email]: contact },
      },
      screenProps: {
        contact,
        initSubScreen: ("recommendedForMe": OtherProfileSubScreen),
      },
    }}
  />);
}).add("empty", () => {
  return (<MockScreen
    {...{
      screen: "OtherProfile",
      remote: {
        getAllFromContact: {
          user,
          contacts: {},
          items: {},

        },
      },
      appState: {
        user,
        contacts: { [contact.email]: contact },
      },
      screenProps: {
        contact,
      },
    }}
  />);
});
