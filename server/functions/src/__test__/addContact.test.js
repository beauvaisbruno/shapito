import { jest } from "@jest/globals";
import testFunctions from "firebase-functions-test";
import { createTransport } from "nodemailer";
import { firestore } from "../commons/firebase";
import { getInvitationEmail } from "../commons/invitationEmail";
import { addContact } from "../index";
import { resetFirestore } from "./resetFirestore";

jest.mock("nodemailer");
jest.setTimeout(10000);

describe("addContact", () => {
  beforeEach(async () => {
    await resetFirestore();
    jest.clearAllMocks();
    // console.log("reset done");
  });
  const testWrapper = testFunctions();

  const USER_EMAIL = "user@mail.com";
  const USER_UID = "uid";

  const user = {
    uid: USER_UID,
    email: USER_EMAIL,
    pseudo: "userPseudo",
  };

  const CONTACT_EMAIL = "contact@mail.com";
  const contact = {
    email: CONTACT_EMAIL,
  };
  it("new contact", async (done) => {

    const sendMail = jest.fn();
    createTransport.mockReturnValue({ sendMail });

    const EMAIL_PASSWORD = "email";
    const GMAIL_PASSWORD = "password";
    testWrapper.mockConfig({ gmail: { password: GMAIL_PASSWORD, email: EMAIL_PASSWORD } });

    await firestore.collection("TheUsers").doc(USER_EMAIL).set(user);

    const response = await testWrapper.wrap(addContact)({
      contact: { email: CONTACT_EMAIL },
      email: USER_EMAIL,
    }, {
      auth: { uid: USER_UID },
    });

    expect(createTransport).toHaveBeenCalledWith({
      service: "gmail",
      auth: {
        user: EMAIL_PASSWORD,
        pass: GMAIL_PASSWORD,
      },
    });

    expect(sendMail.mock.calls[0][0]).toEqual({
      to: CONTACT_EMAIL,
      subject: user.pseudo + " vous a invité à utiliser Shapito.",
      text: getInvitationEmail(user),
      html: getInvitationEmail(user),
    });

    expect(response).toEqual({
      contact: {
        email: CONTACT_EMAIL,
        contactEmails: [USER_EMAIL],
        recommendedByMeItemIds: [],
        recommendedForMeItemIds: [],
      },
      user: {
        ...user,
        contactEmails: [CONTACT_EMAIL],
      },
    });
    done();
  });
  it("update contact", async (done) => {

    await firestore.collection("TheUsers").doc(USER_EMAIL).set(user);
    await firestore.collection("TheUsers").doc(CONTACT_EMAIL).set(contact);

    const response = await testWrapper.wrap(addContact)({
      contact: { email: CONTACT_EMAIL },
      email: USER_EMAIL,
    }, {
      auth: { uid: USER_UID },
    });

    expect(createTransport.mock.calls.length).toBe(0);

    expect(response).toEqual({
      contact: {
        email: CONTACT_EMAIL,
        contactEmails: [USER_EMAIL],
      },
      user: {
        ...user,
        contactEmails: [CONTACT_EMAIL],
      },
    });
    done();
  });
});
