import { jest } from "@jest/globals";
import testFunctions from "firebase-functions-test";
import { createTransport } from "nodemailer";
import { firestore } from "../commons/firebase";
import { getRecommendationEmail } from "../commons/invitationEmail";
import { updateRecommendation } from "../updateRecommendation";
import { resetFirestore } from "./resetFirestore";

jest.mock("nodemailer");

describe("updateRecommendation", () => {
  beforeEach(async () => {
    await resetFirestore();
    jest.clearAllMocks();
    // console.log("reset done");
  });
  const testWrapper = testFunctions();

  const OWNER_EMAIL = "owner@email.com";
  const OWNER_UID = "uid";
  const user = {
    uid: OWNER_UID,
    email: OWNER_EMAIL,
    pseudo: "ownerPseudo",
  };
  const NEW_CONTACT_EMAIL = "new@email.com";
  const newContact = {
    email: NEW_CONTACT_EMAIL,
    avatar: "defaultAvatar",
  };

  const OLD_CONTACT_EMAIL = "old@email.com";
  const ITEM_ID = "someItemId";
  const item = {
    itemId: ITEM_ID,
    recommendedToEmails: [OLD_CONTACT_EMAIL],
    recommendedByEmail: OWNER_EMAIL,
  };
  const oldContact = {
    email: OLD_CONTACT_EMAIL,
    recommendedForMeItemIds: ["someItemId"],
  };

  it("add recommendation", async (done) => {
    const sendMail = jest.fn();
    createTransport.mockReturnValue({ sendMail });

    const EMAIL_PASSWORD = "email";
    const GMAIL_PASSWORD = "password";
    testWrapper.mockConfig({ gmail: { password: GMAIL_PASSWORD, email: EMAIL_PASSWORD } });

    await firestore.collection("TheUsers").doc(OWNER_EMAIL).set(user);
    await firestore.collection("TheUsers").doc(NEW_CONTACT_EMAIL).set(newContact);
    await firestore.collection("TheItems").doc(ITEM_ID).set(item);

    const result = await testWrapper.wrap(updateRecommendation)({
      email: OWNER_EMAIL,
      itemId: ITEM_ID,
      operation: "add",
      contact: { email: NEW_CONTACT_EMAIL },
    }, {
      auth: { uid: OWNER_UID },
    });

    expect(createTransport).toHaveBeenCalledWith({
      service: "gmail",
      auth: {
        user: EMAIL_PASSWORD,
        pass: GMAIL_PASSWORD,
      },
    });

    expect(sendMail.mock.calls[0][0]).toEqual({
      to: NEW_CONTACT_EMAIL,
      subject: user.pseudo + " vous a fait une recommandation sur Shapito.",
      text: getRecommendationEmail({ user, contact: newContact }),
      html: getRecommendationEmail({ user, contact: newContact }),
    });

    expect(result).toEqual({
      item: {
        ...item,
        recommendedToEmails: [OLD_CONTACT_EMAIL, NEW_CONTACT_EMAIL],
      },
      contact: {
        ...newContact,
        recommendedForMeItemIds: [ITEM_ID],
      },
    });
    done();

  });

  it("remove recommendation", async (done) => {
    await firestore.collection("TheUsers").doc(OWNER_EMAIL).set(user);
    await firestore.collection("TheUsers").doc(OLD_CONTACT_EMAIL).set(oldContact);
    await firestore.collection("TheItems").doc(ITEM_ID).set(item);

    const result = await testWrapper.wrap(updateRecommendation)({
      email: OWNER_EMAIL,
      itemId: ITEM_ID,
      operation: "remove",
      contact: { email: OLD_CONTACT_EMAIL },
    }, {
      auth: { uid: OWNER_UID },
    });

    expect(result).toEqual({
      item: {
        ...item,
        recommendedToEmails: [],
      },
      contact: {
        ...oldContact,
        recommendedForMeItemIds: [],
      },
    });
    expect(createTransport.mock.calls.length).toBe(0);
    done();

  });
});
