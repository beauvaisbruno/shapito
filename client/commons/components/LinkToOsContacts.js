//@flow

import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { colors, padding, wp } from "../css";

export function LinkToOsContacts ({ onPress, empty }: {| onPress: () => void, empty: boolean |}) {
  return <View style={{
    flex: 1,
    alignItems: "center",
    justifyContent: "space-evenly",
  }}>
    {empty && <Text style={{
      fontSize: wp(5),
      textAlign: "center",
    }}>{"Vous n'avez aucun de vos amis qui utilise Shapito pour l'instant."}</Text>}
    <TouchableOpacity
      style={{
        justifyContent: "center",
        alignItems: "center",
        width: wp(75),
        backgroundColor: colors.lightBg,
        ...padding(5),
        borderRadius: wp(5),
        marginTop: wp(2),
      }}
      onPress={() => {
        onPress();
      }}
    >
      <Text style={{
        fontSize: wp(5),
        textAlign: "center",
      }}>{"Ajouter un ami depuis votre carnet d'adresse."}</Text>
      <Image source={require("../../commons/img/inviteFriend.png")}
             style={{
               marginTop: wp(2),
               width: wp(15),
               height: wp(15),
             }}
      />
    </TouchableOpacity>
  </View>;
}
