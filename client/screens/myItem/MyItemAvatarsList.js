//@flow

import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { getAppState, useAppState } from "../../commons/appState";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { margin, wp } from "../../commons/css";
import { avatars } from "../../model/avatars/avatar";
import type { TheItemArrayProps } from "../../model/TheItem";
import type { MyItemSubScreen } from "./MyItem";

export function MyItemAvatarsList ({
  itemId,
  field,
  icon,
  label,
  setSubScreen,
}: {
  itemId: string,
  setSubScreen: (MyItemSubScreen)=>void,
  field: $Keys<TheItemArrayProps>,
  icon: number,
  label: string,
}) {
  const item = useAppState(appState => appState.items[itemId]);
  const contacts = getAppState(appState => appState.contacts);

  const touchableStyle = {
    alignItems: "center",
    width: wp(20),
    marginBottom: wp(2),
  };
  const iconStyle = {
    width: wp(15),
    height: wp(15),
  };

  if (!item[field] || item[field].length === 0) {
    return null;
  }
  const rows = [];

  item[field].forEach((email) => {
    try {
      const contact = contacts[email];
      rows.push(<TouchableOpacity
        onPress={() => {
          setScreen("OtherProfile", { contact });
        }}
        style={touchableStyle}
        key={email}>
        <Image source={contact.avatar ? avatars[contact.avatar] : avatars.defaultAvatar}
               style={iconStyle} />
        <Text style={{
          textAlign: "center",
          fontSize: wp(3),
        }}>{contact.pseudo}</Text>
      </TouchableOpacity>);
    } catch (error) {
      console.log("error: ", error, ", email: ", email);
    }
  });
  if (field === "recommendedToEmails") {
    rows.push(<TouchableOpacity
      key={"addBtn"}
      style={touchableStyle}
      onPress={() => {
        setSubScreen("RecommendationEditor");
      }}
    >
      <Image source={require("../../commons/img/inviteFriend.png")}
             style={iconStyle} />
    </TouchableOpacity>);
  }

  return (<View>
    <View style={{
      flexDirection: "row",
      alignItems: "center",
      ...margin(2),
      marginBottom: wp(2),
    }}>
      <Image source={icon}
             style={{
               width: wp(10),
               height: wp(10),
             }} />
      <Text style={{
        fontSize: wp(5),
        marginLeft: wp(2),
      }}>{label}</Text>
    </View>
    <View style={{
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "flex-start",
    }}>
      {rows}
    </View>
  </View>);
}

