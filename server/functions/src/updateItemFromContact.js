//@flow
import type { TheItem } from "../../../client/model/TheItem";
import type { TheUser } from "../../../client/model/TheUser";

const {
  throwError,

} = require("./commons/util");
const { checkUser } = require("./commons/checkUser");
const functions = require("firebase-functions");
const { firestore } = require("./commons/firebase");
export type UpdateItemFromContactRequest = {
  fieldToFill: string,
  itemId: string,
}
export type UpdateItemFromContactResponse = { item: TheItem };

async function updateItemFromContactFn ({
  fieldToFill,
  itemId,
  user,
}: {
  ...UpdateItemFromContactRequest,
  user: TheUser
}) {
  // console.log("updateItemFromContactFn...");
  let setResult;
  let getResult;
  try {
    getResult = await firestore.collection("TheItems").doc(itemId).get();
    if (!getResult.exists) {
      throwError("item not exist", {
        // getResult,
        user,
        itemId,
      });
    }
    let remoteItem: TheItem = getResult.data();

    if (!remoteItem.recommendedToEmails.includes(user.email)) {
      throwError("updateItemFromContactFn, recommendedToEmails not include user", {
        user,
        itemId,
      });
    }
    if (!["seenEmails", "wantToWatchEmails", "watchedEmails"].includes(fieldToFill)) {
      throwError("updateItemFromContactFn, fieldToFill not allowed", {
        user,
        fieldToFill,
        itemId,
      });
    }

    if (!remoteItem[fieldToFill]) remoteItem[fieldToFill] = [];
    if (!remoteItem[fieldToFill].includes(user.email)) remoteItem[fieldToFill].push(user.email);

    setResult = await firestore.collection("TheItems").doc(itemId).set(remoteItem);

    return ({
      item: remoteItem,
    }: UpdateItemFromContactResponse);
  } catch (error) {
    throwError("updateItemFn error", {
      // setResult,
      // getResult,
      error,
      user,
      fieldToFill,
      itemId,
    });
  }
}

exports.updateItemFromContact = functions.https.onCall(async (request, context) => {
  return await updateItemFromContactFn({
    ...request,
    user: await checkUser({ request, context }),
  });
});
