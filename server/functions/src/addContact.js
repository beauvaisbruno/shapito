//@flow
import type { NewContact, Profile, TheUser } from "../../../client/model/TheUser";

const {
  throwError,
} = require("./commons/util");
const { checkUser } = require("./commons/checkUser");
const functions = require("firebase-functions");
const { sendEmail } = require("./commons/mailer");
const { getInvitationEmail } = require("./commons/invitationEmail");
const { firestore } = require("./commons/firebase");

function addcontactEmail ({
  user,
  contactEmail,
}: {
  user: TheUser,
  contactEmail: string,
}) {
  if (!user.contactEmails) user.contactEmails = [];
  if (!user.contactEmails.includes(contactEmail)) {
    user.contactEmails = [...user.contactEmails, contactEmail];
  }
  return user;
}

export type AddContactRequest = { contact: {| ...NewContact, ...Profile |} };
export type AddContactResponse = {
  contact: TheUser,
  user: TheUser,
};

exports.addContactFn = async function addContactFn ({
  contact: requestContact,
  user,
}: {
  ...AddContactRequest,
  user: TheUser,
}): Promise<AddContactResponse> {

  let resultContactGet = null;
  let updateUserResult = null;
  let updateContactResult = null;
  try {

    user = addcontactEmail({
      user,
      contactEmail: requestContact.email,
    });

    updateUserResult = await firestore.collection("TheUsers").doc(user.email).set({
      ...user,
    });

    resultContactGet = await firestore.collection("TheUsers").doc(requestContact.email).get();

    if (!resultContactGet.exists) {
      const newContact: TheUser = {
        ...requestContact,
        contactEmails: [user.email],
        recommendedForMeItemIds: [],
        recommendedByMeItemIds: [],
        // email:requestContact.email, //redundant
      };
      resultContactGet = await firestore.collection("TheUsers").doc(requestContact.email)
        .set(newContact);

      //send invitation email
      // console.log("newContact: ", newContact);
      sendEmail({
        email: newContact.email,
        subject: user.pseudo + " vous a invité à utiliser Shapito.",
        body: getInvitationEmail(user),
      });
      return {
        contact: newContact,
        user,
      };

    }
    let resultContact: TheUser = resultContactGet.data();
    resultContact = addcontactEmail({
      user: resultContact,
      contactEmail: user.email,
    });
    updateContactResult = await firestore.collection("TheUsers").doc(requestContact.email)
      .set(resultContact);
    return {
      contact: resultContact,
      user,
    };
  } catch (error) {
    throwError("Impossible to add contact", {
      error,
      user,
    });
  }
  throw Error("unreachable");
};

//exports.addMessage = functions.https.onRequest(async (req, res) => {
exports.addContact = functions.https.onCall(async (request, context) => {
  return await exports.addContactFn({
    ...request,
    user: await checkUser({ request, context }),
  });

});
