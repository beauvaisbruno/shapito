// @flow
import type { Node } from "react";
import React, { useRef } from "react";
import { Dimensions, ScrollView, View } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import { wp } from "../../commons/css";
import { EmailConnection } from "./EmailConnection";
import { GoogleSignInButton } from "./GoogleSignInButton";

export const Auth: ()=>Node = () => {

  const scrollRef: { current: null | React$ElementRef<typeof ScrollView> } = useRef();

  return (<ScrollView
    keyboardShouldPersistTaps={"handled"}
    style={{
      height: Dimensions.get("window").height,
    }}
    ref={ref => {scrollRef.current = ref;}}
    contentContainerStyle={{
      alignItems: "center",
      justifyContent: "space-evenly",
      height: Dimensions.get("window").height,
    }}
  >
    <View
      style={{
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <AutoHeightImage
        width={wp(60)}
        source={require("../../commons/img/header.png")} />
    </View>
    <View
      style={{
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <EmailConnection {...{ scrollRef }} />
      <GoogleSignInButton />
    </View>
  </ScrollView>);
};
