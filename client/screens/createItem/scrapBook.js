//@flow
import axios from "react-native-axios";
import cheerio from "react-native-cheerio";
import { fetchIso8859_1 } from "../../commons/util";
import type { ItemDescription, ItemSearch } from "../../model/TheItem";

const $log = (text) => {
  console.log(text.replace(/^\s*[\r\n]/gm, "").replace(/^ +/gm, ""));
};

export const scrapBook = async ({ title }: { title: string }) => {
  const url = "https://www.babelio.com/aj_recherche.php?id_user=undefined&isMobile=false&term=" +
    encodeURI(title.replace(" ", "+"));
  const { data: jsonArr } = await axios.get(url);
  console.log(url, ", =>", jsonArr.length);

  const itemSearches: Array<ItemSearch> = [];
  jsonArr.forEach((book) => {
    try {
      const itemDescription: ItemDescription = {
        type: "book",
        title: book.titre,
      };
      itemDescription.thumbUrl = book.couverture;
      if (book.couverture && !book.couverture.includes("http")) {
        itemDescription.thumbUrl = "http://www.babelio.com/" + itemDescription.thumbUrl;
      }
      itemDescription.externalLinkUrl = "http://www.babelio.com/" + book.url;
      const itemSearch = {};
      itemSearch.scrapOne = async () => {
        try {
          if (!itemDescription.externalLinkUrl) {
            throw Error("externalLinkUrl undefined, itemDescription: " + itemDescription.toString());
          }
          const html = await fetchIso8859_1(itemDescription.externalLinkUrl);

          const $ = cheerio.load(html);
          itemDescription.noteValue = $(".rating").text();
          itemDescription.scrapType = "babelio";
          itemDescription.thumbUrl = "https://www.babelio.com" + $(".livre_con img").attr("src");
          itemDescription.summary = $(".livre_resume").text().trim();

          const regExRes = /\(([^)]+)\)/.exec($(".livre_refs").text());
          if (regExRes) {
            const match = regExRes.pop();
            if (match && !match.includes("-1")) {
              itemDescription.publicationDate = match;
            }
          }

        } catch (error) {
          console.log("book scrapOne, error: ", error, ", itemDescription: ", itemDescription);
        }
        // console.log("<scrapOne info: ", itemDescription);
        return itemDescription;
      };
      itemSearch.description = itemDescription;
      //spread to pass flow check
      itemSearches.push({ ...itemSearch });
    } catch (error) {
      console.log("scrapTitleAndThumb, error: ", error);
    }
  });
  return itemSearches;
};

