//@flow

export const emoticons: { [string]: { label: string, icon: number } } = {
  "action": {
    label: "Action",
    icon: require("./action.png"),
  },
  "adventure": {
    label: "Aventure",
    icon: require("./adventure.png"),
  },
  "anime": {
    label: "Aventure",
    icon: require("./anime.png"),
  },
  "princess": {
    label: "Princesse",
    icon: require("./princess.png"),
  },
  "family": {
    label: "Tous Public",
    icon: require("./family.png"),
  },
  // "comedy": {
  //   label: "Comédie",
  //   icon: require("./clown.png"),
  // },
  "clown": {
    label: "Comédie",
    icon: require("./clown.png"),
  },
  "lol": {
    label: "Amusant",
    icon: require("./lol.png"),
  },
  "stupid": {
    label: "Stupide",
    icon: require("./stupid.png"),
  },
  "documentary": {
    label: "Documentaire",
    icon: require("./documentary.png"),
  },
  "drama": {
    label: "Drame",
    icon: require("./drama.png"),
  },
  "romantic": {
    label: "Romantique",
    icon: require("./romantic.png"),
  },
  "cry": {
    label: "Triste",
    icon: require("./cry.png"),
  },
  "horror": {
    label: "Horreur",
    icon: require("./horror.png"),
  },
  "thriller": {
    label: "Slasher",
    icon: require("./thriller.png"),
  },
  "scifi": {
    label: "Sci-Fi",
    icon: require("./scifi.png"),
  },
  "space": {
    label: "Espace",
    icon: require("./espace.png"),
  },
  "strange": {
    label: "Étrange",
    icon: require("./strange.png"),
  },
  "labyrinth": {
    label: "Enigme",
    icon: require("./labyrinth.png"),
  },
  "detective": {
    label: "Enquête",
    icon: require("./detective.png"),
  },
  "courtroom": {
    label: "Enquête",
    icon: require("./courtroom.png"),
  },
  "gangster": {
    label: "Gangster",
    icon: require("./gangster.png"),
  },
  "drug": {
    label: "Drogue",
    icon: require("./drug.png"),
  },
  "knight": {
    label: "Chevalier",
    icon: require("./knight.png"),
  },
  "sea": {
    label: "Ocean",
    icon: require("./sea.png"),
  },
  "vampyre": {
    label: "Vampire",
    icon: require("./vampyre.png"),
  },
  "ghost": {
    label: "Fantôme",
    icon: require("./ghost.png"),
  },
  "western": {
    label: "Western",
    icon: require("./western.png"),
  },
  "pirate": {
    label: "Pirate",
    icon: require("./pirate.png"),
  },
  "treasure": {
    label: "Trésor",
    icon: require("./treasure.png"),
  },
  "art": {
    label: "Art",
    icon: require("./art.png"),
  },
  "monster": {
    label: "Monstre",
    icon: require("./monster.png"),
  },
  "alien": {
    label: "Alien",
    icon: require("./alien.png"),
  },
  "spaceopera": {
    label: "Space Opéra",
    icon: require("./spaceopera.png"),
  },
  "jedi": {
    label: "Jedi",
    icon: require("./jedi.png"),
  },
  "money": {
    label: "Argent",
    icon: require("./money.png"),
  },
  "animal": {
    label: "Animalier",
    icon: require("./animal.png"),
  },

};

export type Emoticon = $Keys<typeof emoticons>;
