//@flow
import { storiesOf } from "@storybook/react-native";
import React from "react";
import { MockScreen } from "../../commons/story/MockScreen";

storiesOf("profileEdit", module).add("ProfileEdit", () => {
  return (
    <MockScreen
      {...{
        screen: "ProfileEdit",
        appState: {
          user: {
            email: "shapitotest.1@gmail.com",
            color: "#ff5388",
            avatar: "toulouse",
            pseudo: "Bruno",
          },
        },
      }}
    />);
});
