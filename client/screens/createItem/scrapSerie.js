//@flow
import axios from "react-native-axios";
import cheerio from "react-native-cheerio";
import type { ItemDescription, ItemSearch } from "../../model/TheItem";

const $log = (text) => {
  console.log(text.replace(/^\s*[\r\n]/gm, "").replace(/^ +/gm, ""));
};

export const scrapSerie = async ({ title }: { title: string }) => {
  const { data: html } = await axios.get("https://www.allocine.fr/recherche/?q=" +
    encodeURI(title.replace(" ", "+")));

  const $ = cheerio.load(html);
  const movies = $("section[class='section series-results'] li");
  // $log(movies.html());

  const itemSearches: Array<ItemSearch> = [];
  movies.each(function (i, elem) {
    try {
      const itemDescription: ItemDescription = {
        type: "serie",
        title: $(this).find(".meta-title-link").text(),
      };
      itemDescription.scrapType = "allocine";
      itemDescription.thumbUrl = $(this).find(".thumbnail-container").children("img").eq(0)
        .attr("data-src").replace("https", "http");

      const itemSearch = {};
      itemSearch.cheerioEl = $(this);

      itemSearch.scrapOne = async () => {
        const cheerioOneItem = itemSearch.cheerioEl;
        if (!cheerioOneItem) {
          throw Error("No cherrio element defined for info: " + itemSearch.toString());
        }
        try {
          itemDescription.publicationDate = cheerioOneItem.find(".meta-body .date").text();
          itemDescription.summary = cheerioOneItem.find(".synopsis .content-txt").text().trim();
          cheerioOneItem.find(".rating-item-content").each(function (i, elem) {
            const noteType = $(this).find(".rating-title").text();
            //Spectateurs
            if (noteType.toLowerCase().includes("spect")) {
              itemDescription.noteValue = $(this).find(".stareval-note").text();
            }
          });
        } catch (error) {
          console.log("scrapSerie, error: ", error, ", itemDescription: ", itemDescription,
            cheerioOneItem);
        }
        return itemDescription;
      };
      itemSearch.description = itemDescription;
      //spread to pass flow check
      itemSearches.push({ ...itemSearch });
    } catch (error) {
      console.log("scrapSerie, error: ", error);
    }
  });
  // console.log("description: ", description);

  return itemSearches;
};

