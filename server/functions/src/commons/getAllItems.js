//@flow
import type { TheItem } from "../../../../client/model/TheItem";
import type { TheUser } from "../../../../client/model/TheUser";

const {
  throwError,

} = require("./util");
const { firestore } = require("./firebase");

exports.getAllItems = async (user: TheUser): Promise<{ [email: string]: TheItem }> => {
  const items = {};

  const docRefs = [];
  console.log("getAll recommendedByMeItemIds: ", user.recommendedByMeItemIds);

  if (user.recommendedByMeItemIds) {
    user.recommendedByMeItemIds.forEach((itemId) => {
      docRefs.push(firestore.collection("TheItems").doc(itemId));
    });
  }
  if (user.recommendedForMeItemIds) {
    user.recommendedForMeItemIds.forEach((itemId) => {
      docRefs.push(firestore.collection("TheItems").doc(itemId));
    });
  }

  // console.log("getAll contacts docRefs: ", docRefs.length);
  if (docRefs.length > 0) {
    const docs = await firestore.getAll(...docRefs);

    docs.forEach((doc) => {
      // console.log("item exists: ", doc.exists, ", id: ", doc.id);
      if (!doc.exists) {
        console.error("item not exist, id: ", doc.id, ", user: ", user);
        return;
      }
      const data: TheItem = doc.data();
      // console.log("item data: ", data);
      items[data.itemId] = data;
    });
  }
  // console.log("items: ", items);
  return items;
};
