//@flow
import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { colors, padding, wp } from "../../commons/css";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import type { TheItem } from "../../model/TheItem";
import type { MyItemSubScreen } from "./MyItem";

export const MyItemEmptyListLinks = ({
  item,
  setSubScreen,
}: {
  item: TheItem,
  setSubScreen: (MyItemSubScreen)=>void,
}) => {
  const thisLabel = itemTypes[item.type].thisLabel;
  return (
    <View style={{
      flex: 1,
      alignItems: "center",
      justifyContent: "space-evenly",
    }}>
      <Text style={{
        fontSize: wp(5),
        textAlign: "center",
      }}>{"Vous n'avez fait aucune recommandation concernant " + thisLabel + "."}</Text>
      <TouchableOpacity
        style={{
          justifyContent: "center",
          alignItems: "center",
          width: wp(75),
          backgroundColor: colors.lightBg,
          ...padding(5),
          borderRadius: wp(5),
          marginTop: wp(2),
        }}
        onPress={() => {
          setSubScreen("RecommendationEditor");
        }}
      >
        <Text style={{
          fontSize: wp(5),
          textAlign: "center",
        }}>{"Recommander " + thisLabel + " à un ami en particulier."}</Text>
        <Image source={require("../../commons/img/inviteFriend.png")}
               style={{
                 marginTop: wp(2),
                 width: wp(15),
                 height: wp(15),
               }}
        />
      </TouchableOpacity>
    </View>
  );
};
