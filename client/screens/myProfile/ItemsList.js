//@flow
import type { Element } from "react";
import React from "react";
import { Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import { useAppState } from "../../commons/appState";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { colors, margin, padding, wp } from "../../commons/css";
import { darkenColor, lightenColor, lightenColor2, values } from "../../commons/util";
import { avatars } from "../../model/avatars/avatar";
import { emoticons } from "../../model/emoticons/emoticons";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import { noteIcons } from "../../model/noteIcons/noteIcons";
import type { TheItem } from "../../model/TheItem";
import { EmptyListLinks } from "./EmptyListLinks";

const iconPc = 9.5;
const iconStyle = {
  height: wp(iconPc),
  width: wp(iconPc), ...margin(0.5),
};

export const ItemsList = ({
    itemsFilter,
    refreshControl,
    emptyMessage,
  }: {
    itemsFilter: (TheItem)=>boolean,
    refreshControl: Element<any>,
    emptyMessage: string,
  },
) => {

  const items = useAppState(appState => appState.items);
  const contacts = useAppState(appState => appState.contacts);
  const rows = [];
  // console.log("recommendedForMe : ",sortedItem.length);

  //TODO memoization
  const filteredItems = values(items).filter(itemsFilter);

  const sortedItem = filteredItems.sort((item1, item2) => {
    // show most recent first
    return item2.creationDate - item1.creationDate;
  });

  // console.log("recommendedForMe: ",sortedItem.length,"/",Object.keys(items).length);
  let itemsCount = 0;
  sortedItem.forEach(item => {
    try {
      const contact = contacts[item.recommendedByEmail];
      //Keep Duplicated code
      const emoticonsRows = [];
      item.emoticons.forEach((emoticon) => {
        emoticonsRows.push(<Image key={emoticon} style={iconStyle}
                                  source={emoticons[emoticon].icon} />);
      });

      let backgroundColor = colors.userNoColor;
      if (contact.color) {
        backgroundColor = lightenColor(contact.color);
      }
      //to always have adjacent card with different background color
      if (itemsCount % 4 === 0 || itemsCount % 4 === 3) {
        backgroundColor = lightenColor2(backgroundColor);
      }
      itemsCount++;

      rows.push(<TouchableOpacity
        key={item.itemId}
        style={{
          width: wp(50),
          minHeight: wp(32),
          backgroundColor,
        }}
        onPress={() => {
          setScreen("OtherItem", {
            itemId: item.itemId,
          });
        }}>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1, ...padding(1) }}>
            <Text
              style={{
                fontSize: wp(4),
                flexWrap: "wrap",
                textAlign: "center",
              }}>
              {item.title}
            </Text>
            <View style={{ flexDirection: "row" }}>

              <View style={{ alignItems: "center" }}>
                <View style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  width: wp(32),
                }}>
                  <Image style={iconStyle} source={noteIcons[item.noteIcon].icon} />
                  <Image style={iconStyle} source={itemTypes[item.type].icon} />
                  <Image style={iconStyle}
                         source={avatars[contact.avatar] || avatars.defaultAvatar} />
                </View>

                <View style={{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "center",
                }}>
                  {emoticonsRows}
                </View>
              </View>
            </View>
          </View>

          <View>
            <AutoHeightImage
              source={{ uri: item.thumbUrl }}
              width={wp(15)}
              fallbackSource={itemTypes[item.type].defaultThumb}
            />
            <Text style={{
              fontSize: wp(3),
              flexWrap: "wrap",
              width: wp(15),
              textAlign: "center",
              // color: "black",
              color: contact.color ? darkenColor(contact.color) : "black",
            }}>
              {contact.pseudo.length < 8 ? contact.pseudo : contact.pseudo.substring(0, 8) + "…"}
            </Text>
          </View>
        </View>
      </TouchableOpacity>);
    } catch (error) {
      console.error("error: ", error, ", item: ", item);
    }
  });

  if (rows.length === 0) {
    return <ScrollView
      {...{
        style: {
          flex: 1,
          flexGrow: 1,
        },
      }}
    >
      <Text style={{
        fontSize: wp(5),
        fontStyle: "italic",
        color: colors.textGrey,
        textAlign: "center",
        width: wp(90),
        alignSelf: "center",
      }}>
        {emptyMessage}
      </Text>
      <EmptyListLinks />
    </ScrollView>;
  }

  return (<ScrollView
    {...{
      persistentScrollbar: true,
      refreshControl,
      style: {
        flex: 1,
        flexGrow: 1,
      },
      contentContainerStyle: {
        paddingBottom: wp(20),
      },
    }}
  >
    <View style={{
      flex: 1,
      flexDirection: "row",
      flexWrap: "wrap",
      paddingBottom: wp(20),
    }}>
      {rows}
    </View>
  </ScrollView>);
};

