//@flow

import React, { useEffect, useRef, useState } from "react";
import { Image, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import { colors, css, margin, wp } from "../../commons/css";
import { DEV } from "../../commons/DEV";
import { useKeyboard, useLoader } from "../../commons/hooks";
import type { ItemType } from "../../model/ItemTypes/itemTypes";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import type { ItemDescription, ItemSearch } from "../../model/TheItem";
import { CancelBtn } from "./CancelBtn";

const ItemScraper = ({
  title,
  type,
  setDescription,
  defCandidates,
}: {
  title: string,
  type: ItemType,
  setDescription: (ItemDescription)=>void,
  defCandidates: Array<ItemSearch>,
}) => {
  const [candidates, setCandidates] = useState(defCandidates);
  const { Loader, setLoading, setError } = useLoader();

  const timeout = useRef();
  // console.log("candidates: ",candidates);

  useEffect(() => {
    if (timeout.current) clearTimeout(timeout.current);
    if (title.length > 3) {
      timeout.current = setTimeout(() => {
        // console.log("scrap " + type);
        setLoading(true);
        itemTypes[type].scraper({ title }).then((candidates: Array<ItemSearch>) => {
          console.log("done");
          setLoading(false);
          setCandidates(candidates);
        }).catch((error) => {
          console.warn("error: ", error);
          setError(true);
        });
      }, 100);
    }

  }, [title]);

  const rows = [];
  const btnStyle = {
    marginTop: wp(2),
    width: wp(32),
    alignItems: "center",
  };
  const imgStyle = {
    width: wp(25),
    height: wp(25),
    resizeMode: "contain",
  };
  const textStyle = {
    fontSize: wp(5),
    color: colors.text,
    textAlign: "center",
  };
  if (title.trim() !== "") {
    rows.push(<TouchableOpacity
      key={title + "def"}
      style={btnStyle}
      onPress={() => {
        setDescription({
          title,
          type: type,
        });
      }}
    >
      <Image
        source={itemTypes[type].defaultThumb}
        style={imgStyle} />
      <Text style={textStyle}>{title}</Text>
    </TouchableOpacity>);
  }
  candidates.forEach((itemSearch: ItemSearch) => {
    // console.log("item: ",item.thumbUrl);
    const description = itemSearch.description;
    rows.push(<TouchableOpacity
      key={description.title + "" + rows.length}
      style={btnStyle}
      onPress={() => {
        if (itemSearch.scrapOne) {
          itemSearch.scrapOne().then(description => {
            setDescription(description);
          }).catch(error => {
            console.log("scrapOne error: ", error);
            setDescription(description);
          });
        } else {
          setDescription(description);
        }
      }}
    >
      <Image
        source={{
          uri: description.thumbUrl,
        }}
        style={imgStyle} />
      <Text style={textStyle}>{description.title}</Text>
    </TouchableOpacity>);
  });

  return (<ScrollView
    persistentScrollbar={true}
    style={{ flex: 1 }}
    keyboardShouldPersistTaps={"handled"}
  >
    {rows.length > 0 &&
    <Text style={{
      textAlign: "center",
      fontStyle: "italic",
      marginTop: wp(2),
      fontSize: wp(3),
      color: colors.text,
    }}>
      {"Sélectionnez le titre ci-dessous"}
    </Text>}
    <Loader />
    <View style={{
      flex: 1,
      marginTop: wp(2),
      flexDirection: "row",
      justifyContent: "space-evenly",
      alignItems: "flex-start",
      flexWrap: "wrap",
    }}>
      {rows}
    </View>
  </ScrollView>);
};

export const ItemSearcher = ({
  type,
  setDescription,
  defCandidates = [],
}: {
  type: ItemType,
  setDescription: (ItemDescription)=>void,
  defCandidates?: Array<ItemSearch>,
}) => {
  const [title, titleChange] = useState(DEV.fillForm ? "sta" : "");
  const { screenHeight, isKeyboard } = useKeyboard();

  return (<View style={{
    ...(isKeyboard ? { height: screenHeight } : { flex: 1 }),
  }}>
    {!isKeyboard && <View style={{
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center", ...margin(2),
      width: wp(96),
    }}>
      <Image style={{
        width: wp(15),
        height: wp(15),
      }} source={itemTypes[type].icon} />
      <Text style={{
        fontSize: wp(10),
        color: colors.text,
      }}>{itemTypes[type].label}</Text>
      <CancelBtn />
    </View>}
    <View style={{
      alignSelf: "center",
    }}>
      <View style={{ alignItems: "center" }}>
        {!isKeyboard && <Text style={{
          fontSize: wp(7),
          color: colors.text,
          marginTop: wp(5),
        }}>Titre {itemTypes[type].labelOf}</Text>}

        <TextInput
          style={{
            ...css.input,
            textAlign: "center",
            width: wp(96),
          }}
          onChangeText={text => titleChange(text)}
          value={title}
          autoCompleteType={"name"}
          placeholder={"Commencez à tapez le titre"}
        />
      </View>
    </View>
    <ItemScraper {...{
      title,
      type,
      setDescription,
      defCandidates,
    }} />
  </View>);
};
