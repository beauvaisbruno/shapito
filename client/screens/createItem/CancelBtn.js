//@flow
import React from "react";
import { Image, TouchableOpacity } from "react-native";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { wp } from "../../commons/css";

export const CancelBtn = (props: {||}) => {
  return (<TouchableOpacity
    onPress={() => {
      setScreen("MyProfile");
    }}
  >
    <Image
      source={require("../../commons/img/deleteBtn.png")}
      style={{
        width: wp(15),
        height: wp(15),
      }}
    />
  </TouchableOpacity>);
};
