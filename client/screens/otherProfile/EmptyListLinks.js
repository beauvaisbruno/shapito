//@flow
import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { colors, padding, wp } from "../../commons/css";

export const EmptyListLinks = (props: {||}) => {
  return (<View style={{
    flex: 1,
    alignItems: "center",
    justifyContent: "space-evenly",
  }}>
    <TouchableOpacity
      style={{
        justifyContent: "center",
        alignItems: "center",
        width: wp(75),
        backgroundColor: colors.lightBg,
        ...padding(5),
        borderRadius: wp(5),
      }}
      onPress={() => {
        setScreen("CreateItem");
      }}
    >
      <Text style={{
        fontSize: wp(5),
        textAlign: "center",
      }}>{"Recommander un film, une série, un livre, ..."}</Text>
      <Image source={require("../../commons/img/createBtn.png")}
             style={{
               marginTop: wp(2),
               width: wp(15),
               height: wp(15),
             }}
      />
    </TouchableOpacity>
    <TouchableOpacity
      style={{
        justifyContent: "center",
        alignItems: "center",
        width: wp(75),
        backgroundColor: colors.lightBg,
        ...padding(5),
        borderRadius: wp(5),
      }}
      onPress={() => {
        setScreen("Friends");
      }}
    >
      <Text style={{
        fontSize: wp(5),
        textAlign: "center",
      }}>{"Inviter un ami"}</Text>
      <Image source={require("../../commons/img/contact.png")}
             style={{
               marginTop: wp(2),
               width: wp(15),
               height: wp(15),
             }}
      />
    </TouchableOpacity>
  </View>);
};
