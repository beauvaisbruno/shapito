//@flow
import type { TheUser } from "../../../../client/model/TheUser";

const { CallableContext } = require("firebase-functions/lib/providers/https");
const { throwError } = require("./util");
const { firestore } = require("./firebase");

exports.checkUser = async ({
  request,
  context,
}: {
  request: { email: string },
  context: CallableContext,
}): Promise<TheUser> => {
  const email = request.email;
  if (!email) throwError("No email specified", { email });

  if (!context.auth || !context.auth.uid) {
    throwError("User not authenticated", { uid: context.auth.uid });
  }

  let resultUserGet = null;
  resultUserGet = await firestore.collection("TheUsers").doc(email).get();

  if (!resultUserGet.exists) throwError("user doesn't exist", { email });

  const user = (resultUserGet.data(): TheUser);
  // console.log("user", user);
  if (!user.uid) {
    // console.log("User exists but has no uid");
    return user;
  }
  if (user.uid !== context.auth.uid) {
    throwError("Wrong uid", {
      // resultUserGet,
      user,
      userUid: user.uid,
      currentUid: context.auth.uid,
    });
  }
  return user;
};
