//@flow

import React from "react";
import { Image, Text, View } from "react-native";
import { getAppState, useAppState } from "../../commons/appState";
import { margin, wp } from "../../commons/css";
import { useSubScreen } from "../../commons/hooks";
import { lightenColor } from "../../commons/util";
import { avatars } from "../../model/avatars/avatar";
import { IconsEditor } from "./IconsEditor";
import { Main } from "./Main";
import { RecommendationEditor } from "./RecommendationEditor";

const Header = ({ itemId }: { itemId: string }) => {
  const item = useAppState(appState => appState.items[itemId]);
  const user = getAppState(appState => appState.user);
  return (<View style={{
    backgroundColor: lightenColor(user.color),
    flexDirection: "row",
    alignItems: "center",
  }}>
    <Image source={avatars[user.avatar]}
           style={{
             width: wp(15),
             height: wp(15), ...margin(5),
           }} />
    <Text
      style={{
        fontSize: item.title.length < 20 ? wp(9) : wp(7),
        textAlign: "center",
        flex: 1,
      }}>{item.title}
    </Text>
  </View>);
};

export type MyItemSubScreen = "IconsEditor" | "RecommendationEditor" | "Main";

export const MyItem = ({
  itemId,
  defaultSubScreen = "Main",
}: {
  itemId: string,
  defaultSubScreen: MyItemSubScreen,
}) => {
  console.log("itemId: ", itemId);

  const { SubScreen } = useSubScreen(defaultSubScreen, {
    Main: (setSubScreen) => <Main {...{ itemId, setSubScreen }} />,
    IconsEditor: (setSubScreen) => <IconsEditor {...{ itemId, setSubScreen }} />,
    RecommendationEditor: (setSubScreen) => <RecommendationEditor {...{ itemId, setSubScreen }} />,
  });

  return (<View style={{ flex: 1 }}>
    <Header {...{ itemId }} />
    {SubScreen}
  </View>);
};
