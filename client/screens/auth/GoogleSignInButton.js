//@flow
import React from "react";
import { Text, TouchableOpacity } from "react-native";
import { css, wp } from "../../commons/css";
import { useLoader } from "../../commons/hooks";
import { googleSignin } from "../../commons/requests/connection";

export const GoogleSignInButton = () => {
  const { Loader, loading, error, setLoading, setError } = useLoader();

  return (
    <>
      {error && <Loader />}
      <TouchableOpacity
        disabled={loading !== false}
        onPress={async () => {
          setLoading(true);
          googleSignin().then(() => {
            setLoading(false);
          }).catch((error) => {
            console.warn("GoogleSignIn error: ", error);
            setError(true);
          });
        }
        }
      >
        {loading && <Text
          style={{
            ...css.button,
            backgroundColor: "#ec7373",
            width: wp(90),
          }}
        >
          Connexion...
        </Text>}
        {!loading && <Text
          style={{
            ...css.button,
            backgroundColor: "#ec7373",
            width: wp(90),
          }}
        >
          Connexion avec Google
        </Text>
        }
      </TouchableOpacity>
    </>);
};
