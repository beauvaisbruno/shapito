//@flow
import type { TheItem } from "../../../client/model/TheItem";
import type { TheUser } from "../../../client/model/TheUser";

const {
  throwError,
  mergeToFieldArr,
  removeFromFieldArr,
} = require("./commons/util");
const { checkUser } = require("./commons/checkUser");
const functions = require("firebase-functions");
const { getRecommendationEmail } = require("./commons/invitationEmail");
const { sendEmail } = require("./commons/mailer");
const { firestore } = require("./commons/firebase");

exports.getItem = async (itemId: string) => {
  return await firestore.collection("TheItems").doc(itemId).get();
};

exports.getContact = async (contact: TheUser) => {
  return await firestore.collection("TheUsers").doc(contact.email).get();
};

export type UpdateRecommendationRequest = {
  itemId: string,
  operation: string,
  contact: TheUser
};
export type UpdateRecommendationResponse = {
  item: TheItem,
  contact: TheUser,
};

exports.updateRecommendationFn = async ({
  itemId,
  operation,
  contact,
  user,
}: {
  ...UpdateRecommendationRequest,
  user: TheUser
}): Promise<UpdateRecommendationResponse> => {
  // console.log("updateRecommendationFn... ", { operation });
  let setResult;
  let itemGetResult;
  let contactGetResult;
  let updateUserResult;
  try {
    itemGetResult = await exports.getItem(itemId);
    if (!itemGetResult.exists) {
      throwError("item not exist", {
        // itemGetResult,
        user,
        itemId,
      });
    }
    let remoteItem: TheItem = itemGetResult.data();
    contactGetResult = await exports.getContact(contact);
    if (!contactGetResult.exists) {
      throwError("contact not exist", {
        // contactGetResult,
        contact,
        user,
        itemId,
      });
    }
    let remoteContact: TheUser = contactGetResult.data();

    if (remoteItem.recommendedByEmail !== user.email) {
      throwError("user not allowed to update recommendedToEmails", {
        user,
        itemId,
        operation,
      });
    }
    if (!operation) {
      throwError("operation not specified", {
        operation,
        user,
        itemId,
      });
    }

    if (operation === "remove") {
      remoteItem = removeFromFieldArr(remoteItem, "recommendedToEmails", contact.email);
      remoteContact = removeFromFieldArr(remoteContact, "recommendedForMeItemIds",
        remoteItem.itemId);
    }
    if (operation === "add") {
      // console.log("remoteItem: ", remoteItem);
      remoteItem = mergeToFieldArr(remoteItem, "recommendedToEmails", contact.email);
      // console.log("remoteItem: ", remoteItem);
      remoteContact = mergeToFieldArr(remoteContact, "recommendedForMeItemIds", remoteItem.itemId);
    }

    setResult = await firestore.collection("TheItems").doc(itemId).set(remoteItem);
    updateUserResult = await firestore.collection("TheUsers").doc(contact.email).set(remoteContact);

    if (operation === "add") {
      sendEmail({
        email: remoteContact.email,
        subject: user.pseudo + " vous a fait une recommandation sur Shapito.",
        body: getRecommendationEmail({ user, contact: remoteContact }),
      });
    }

    return {
      item: remoteItem,
      contact: remoteContact,
    };
  } catch (error) {
    throwError("updateRecommendation error", {
      // setResult,
      // itemGetResult,
      // updateUserResult,
      error,
      user,
      operation,
      contact,
      itemId,
    });
  }
  throw Error("Unreachable");
}

exports.updateRecommendation = functions.https.onCall(async (request, context) => {
  return exports.updateRecommendationFn({
    ...request,
    user: await checkUser({ request, context }),
  });
});
