import initStoryshots, { Stories2SnapsConverter } from "@storybook/addon-storyshots";
import { render, waitFor } from "@testing-library/react-native";
import React from "react";
import { DEV } from "../DEV";
import { loadStories } from "./storyLoader";

let count = 0;
DEV.story = true;
DEV.fillForm = false;
DEV.mockOsContacts = false;
DEV.networkDelay = 1;

// jest.useFakeTimers();
// jest.setTimeout(20000);
initStoryshots({
  asyncJest: true,
  framework: "react-native",
  config: ({ configure }) =>
    configure(() => {
      // require("../../screens/myProfile/MyProfile.stories");
      loadStories();
    }, module),
  test: async ({
    story,
    context,
    done,
  }) => {
    // console.log(">start...", count);
    const snapshotFilename = new Stories2SnapsConverter().getSnapshotFileName(context);

    const storyElement = story.render();

    const wrapper = render(storyElement);

    const start = Date.now();
    await waitFor(() => {
      if (Date.now() < start + 100) throw Error("wait");
    });

    expect(wrapper).toMatchSpecificSnapshot(snapshotFilename);
    done();
    // console.log("!done", count - 1);
  },
});
