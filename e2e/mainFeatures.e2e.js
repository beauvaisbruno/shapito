/* eslint-env jest */
import { by, device, element, expect, waitFor } from "detox";
// import { beforeAll, describe, it } from "jest-circus";
// import DEV from "../client/commons/DEV";
// import { avatars } from "../client/model/avatars/avatar";

// DEV.fillForm = false;
const USER_PSEUDO = "shapito1";
const USER_EMAIL = "user1@shapito.com";
const USER_PASSWORD = "shapito";
const FIRST_AVATAR = "berlioz";

describe("standard flow", () => {
  beforeAll(async () => {
    await device.launchApp();
    // await device.reloadReactNative();
  });

  it("should login", async () => {
    await waitFor(element(by.id("connectWithEmailBtn"))).toBeVisible().withTimeout(3000);
    await element(by.id("connectWithEmailBtn")).tap();
    await element(by.id("connectionEmailInput")).typeText(USER_EMAIL);
    await element(by.id("connectionPasswordInput")).typeText(USER_PASSWORD);
    await element(by.id("connectWithEmailBtn")).tap();
  });
  //
  it("should create profile", async () => {
    await waitFor(element(by.id("profilePseudoInput"))).toBeVisible().withTimeout(3000);
    await element(by.id("profilePseudoInput")).typeText(USER_PSEUDO);
    await element(by.id("profilePseudoBtn")).tap();
    await expect(element(by.id(FIRST_AVATAR))).toBeVisible();
    await element(by.id(FIRST_AVATAR)).tap();
    await element(by.id("colorSelectBtn")).tap();

    await waitFor(element(by.id("MyProfile"))).toBeVisible().withTimeout(3000);
    await expect(element(by.id("myProfileEmail"))).toHaveText(USER_EMAIL);
    await expect(element(by.id("myProfilePseudo"))).toHaveText(USER_PSEUDO);
    await expect(element(by.id("myProfileAvatar"))).toHaveLabel(FIRST_AVATAR);
  });
  //TODO test all other features: addFriend, createItem, logoutThenLogin, ...

});
