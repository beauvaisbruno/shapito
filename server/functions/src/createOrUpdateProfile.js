//@flow
import type { Profile, TheUser } from "../../../client/model/TheUser";

const functions = require("firebase-functions");
const {
  throwError,

} = require("./commons/util");

const { firestore } = require("./commons/firebase");

function updateProfile ({ remoteUser, profile, context }): TheUser {
  if (remoteUser.uid !== context.auth.uid) {
    throwError("Wrong uid", {
      remoteUser,
      remoteUserUid: remoteUser.uid,
      currentUid: context.auth.uid,
    });
  }
  console.log("updateProfile");
  return {
    ...remoteUser,
    ...profile,
  };
}

function createUser ({ request, context }): TheUser {
  console.log("createUser");
  return {
    contactEmails: [],
    recommendedForMeItemIds: [],
    recommendedByMeItemIds: [],
    ...request.profile,
    email: request.email,
    uid: context.auth.uid,
  };
}

function updateProfileNoUid ({ remoteUser, profile, context }): TheUser {
  console.log("updateProfileNoUid");
  return {
    ...remoteUser,
    ...profile,
    uid: context.auth.uid,
  };
}

export type CreateOrUpdateProfileRequest = {| profile: Profile |};
export type CreateOrUpdateProfileResponse = {| user: TheUser |};

exports.createOrUpdateProfile = functions.https.onCall(async (request: {
  email: string,
  ...CreateOrUpdateProfileRequest
}, context) => {

  if (!context.auth || !context.auth.uid) {
    throwError("User not authenticated",
      { uid: context.auth.uid });
  }

  const resultUserGet = await firestore.collection("TheUsers").doc(request.email).get();
  let user;
  if (resultUserGet.exists) {
    const remoteUser: TheUser = resultUserGet.data();
    if (remoteUser.uid === undefined) {
      user = await updateProfileNoUid({ remoteUser, profile: request.profile, context });
    } else {
      user = await updateProfile({ remoteUser, profile: request.profile, context });
    }
  } else {
    user = await createUser({ request, context });
  }
  const createResult = await firestore.collection("TheUsers").doc(request.email).set(user);
  return ({ user }: CreateOrUpdateProfileResponse);
});
