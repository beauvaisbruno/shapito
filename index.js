import React from "react";
import { AppRegistry } from "react-native";
import { name as appName } from "./app.json";
import AppLoader from "./client/commons/components/AppLoader";
import { DEV } from "./client/commons/DEV";

console.log("------------start, DEV: ", DEV);

if (__DEV__ && DEV.story) {
  AppRegistry.registerComponent(appName,
    () => require("./client/commons/story/storybook").StorybookUI);
} else {
  AppRegistry.registerComponent(appName, () => AppLoader);
}
