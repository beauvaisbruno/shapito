//@flow
import { storiesOf } from "@storybook/react-native";
import React from "react";
import { generateItemSearches } from "../../commons/story/generate";
import { MockScreen } from "../../commons/story/MockScreen";
import { pseudoRndReset } from "../../commons/util";
import { EmoticonSelector } from "./EmoticonSelector";
import { ItemSearcher } from "./ItemScraper";
import { NoteSelector } from "./NoteSelector";

pseudoRndReset();
const candidates = generateItemSearches({ nbr: 10 });

storiesOf("CreateItem", module).add("TypeSelector", () => {
  return (<MockScreen
    {...{
      screen: "CreateItem",
    }}
  />);
}).add("ItemSearcher", () => {
  return (<ItemSearcher
    {...{
      type: "film",
      setDescription: (infos) => {console.log("description: ", infos);},
      defCandidates: candidates,
    }}
  />);
}).add("NoteSelector", () => {
  return (<NoteSelector
    {...{
      type: "film",
      setNoteIcon: (note) => {console.log("note: ", note);},
      description: {
        title: "A Star is Born",
        type: "book",
        thumbUrl: "http://fr.web.img3.acsta.net/c_310_420/pictures/19/04/05/16/00/1578857.jpg",
      },
    }}
  />);
}).add("EmoticonSelector", () => {
  return (<EmoticonSelector
    {...{
      type: "film",
      setEmoticons: (emoticons) => {console.log("emoticons: ", emoticons);},
      description: candidates[0].description,
    }}
  />);
});
