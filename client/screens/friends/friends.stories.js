//@flow

import { storiesOf } from "@storybook/react-native";
import React from "react";
import { MockScreen } from "../../commons/story/MockScreen";

storiesOf("Friends", module).add("Friends", () => {
  return (<MockScreen
    {...{
      screen: "Friends",
      screenProps: {
        onContactSelected: (contact) => {
          console.log("onContactSelected contact: ", contact);
        },
      },
      appState: {
        user: {
          "email": "shapitotest.1@gmail.com",
          "contactEmails": ["shapitotest.1@gmail.com", "shapitotest.2@gmail.com"],
        },
        contacts: {
          "shapitotest.1@gmail.com": {
            "avatar": "duchesse",
            "color": "#6543cf",
            "contactEmails": ["shapitotest.1@gmail.com"],
            "email": "shapitotest.1@gmail.com",
            "pseudo": "Bruno1",
          },
          "shapitotest.2@gmail.com": {
            "contactEmails": ["shapitotest.1@gmail.com"],
            "email": "shapitotest.2@gmail.com",
            "pseudo": "Bruno2",
          },
        },
      },
      remote: {
        addContact: {

          user: {
            "email": "shapitotest.1@gmail.com",
            "contactEmails": ["shapitotest.1@gmail.com", "shapitotest.2@gmail.com", "shapitotest.3@gmail.com"],
          },
          contact: {
            "contactEmails": ["shapitotest.1@gmail.com"],
            "email": "shapitotest.3@gmail.com",
            "pseudo": "Bruno3",
          },
        },
      },
    }}
  />);
});
