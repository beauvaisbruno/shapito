//@flow
import React, { useEffect, useState } from "react";
import { Image, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import { fromHsv, TriangleColorPicker } from "react-native-color-picker";
import { getAppState, setAppState } from "../../commons/appState";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { colors, css, margin, padding, wp } from "../../commons/css";
import { useLoader } from "../../commons/hooks";
import { remote } from "../../commons/requests/remote";
import { lightenColor } from "../../commons/util";
import { avatars } from "../../model/avatars/avatar";
import type { CreateOrUpdateProfileRequest, CreateOrUpdateProfileResponse } from "../../../server/functions/src/createOrUpdateProfile";
import type { Avatar } from "../../model/avatars/avatar";
import type { TheUser } from "../../model/TheUser";

const AvatarSelector = ({ setAvatar }
  : { setAvatar: (Avatar)=>void }) => {
  const rows = [];

  Object.keys(avatars).forEach(avatarKey => {
    if (avatarKey === "defaultAvatar") return;
    rows.push(<TouchableOpacity
      testID={avatarKey}
      key={"" + avatarKey}
      onPress={() => setAvatar(avatarKey)}
    >
      <Image
        source={avatars[avatarKey]}
        style={{
          width: wp(20),
          height: wp(20), ...margin(5),
        }}
      />
    </TouchableOpacity>);
  });

  return (<View
    style={{
      flex: 1,
      alignItems: "center",
    }}
  >
    <Text
      style={{
        fontSize: wp(7),
        color: colors.text,
        textAlign: "center",
        marginTop: wp(5),
      }}
    >
      {"Choisissez un avatar"}
    </Text>

    <ScrollView>
      <View
        style={{
          flex: 1, ...padding(5),
          flexDirection: "row",
          alignSelf: "center",
          flexWrap: "wrap",
          marginTop: wp(5),
        }}
      >
        {rows}
      </View>
    </ScrollView>
  </View>);
};

const Color = ({
  avatar,
  user,
  setColor,
}: {
  avatar: Avatar,
  user: TheUser,
  setColor: (string)=>void,
}) => {
  const [color, colorChange] = useState(user.color);

  return (<View
    style={{
      flex: 1,
      backgroundColor: lightenColor(color),
      alignItems: "center",
    }}
  >
    <Text
      style={{
        fontSize: wp(7),
        color: colors.text,
        marginTop: wp(5),
      }}
    >
      {"Choisissez une couleur"}
    </Text>
    <TriangleColorPicker
      onColorChange={newColor => colorChange(fromHsv(newColor))}
      style={{
        width: wp(70),
        height: wp(70), ...margin(5),
      }}
      hideControls
    />
    <Image
      source={avatars[avatar]}
      style={{
        width: wp(40),
        height: wp(40), ...margin(5),
        borderColor: color,
        borderWidth: wp(2),
        borderRadius: wp(20),
      }}
    />
    <TouchableOpacity
      testID="colorSelectBtn"
      onPress={() => {
        setColor(color);
      }}
    >
      <Text
        style={{
          ...css.button,
          backgroundColor: colors.buttonBg,
          width: wp(90),
        }}
      >
        Choisir
      </Text>
    </TouchableOpacity>
  </View>);
};

const Pseudo = ({
  setPseudo,
  user,
}: {
  setPseudo: (string)=>void,
  user: TheUser,
}) => {
  const [pseudo, pseudoChange] = useState(user.pseudo);
  return (<View
    style={{
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center",
    }}
  >
    <Text
      style={{
        fontSize: wp(5),
        color: colors.text,
        textAlign: "center",
      }}
    >
      {"Choisissez un pseudo"}
    </Text>
    <TextInput
      testID="profilePseudoInput"
      style={{
        ...css.input,
        width: wp(90),
        marginTop: wp(5),
      }}
      onChangeText={text => pseudoChange(text)}
      value={pseudo}
      autoCompleteType={"name"}
      placeholder={"Votre pseudo"}
    />
    <TouchableOpacity
      testID="profilePseudoBtn"
      onPress={() => {
        setPseudo(pseudo.trim());
      }}
    >
      <Text
        style={{
          ...css.button,
          backgroundColor: colors.buttonBg,
          width: wp(90),
        }}
      >
        Choisir
      </Text>
    </TouchableOpacity>
  </View>);
};
export const ProfileEdit = (props: {||}) => {
  const [pseudo, setPseudo] = useState(false);
  const [avatar, setAvatar] = useState(false);
  const [color, setColor] = useState(false);
  const { loading, error, setLoading, setError } = useLoader();
  const user = getAppState(appState => appState.user);

  useEffect(() => {
    console.log("ProfileEdit user: ", getAppState(s => s.user));
  }, []);

  useEffect(() => {
    if (color && pseudo && avatar) {
      setLoading(true);
      remote("createOrUpdateProfile", ({
        profile: {
          pseudo,
          avatar,
          color,
        },
      }: CreateOrUpdateProfileRequest)).then(({ user }: CreateOrUpdateProfileResponse) => {
        setLoading(false);
        setAppState(appState => appState.user = user);
        setScreen("MyProfile");
      }).catch((error) => {
        setError(true);
      });
    }
  }, [color]);

  const SelectedComp = () => {
    if (!pseudo) return <Pseudo {...{ setPseudo, user }} />;
    if (!avatar) return <AvatarSelector {...{ setAvatar, user }} />;
    if (!color) return <Color {...{ setColor, user, avatar }} />;

    return <View style={{
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    }}>
      {loading && <View>
        <AutoHeightImage
          style={{ alignSelf: "center" }}
          width={wp(30)}
          source={require("../../commons/img/loading.gif")} />
        <Text style={{
          fontSize: wp(5),
          color: colors.text,
          textAlign: "center",
          width: wp(75),
        }}>
          {"Modification du profile..."}
        </Text>
      </View>}
      {error && <TouchableOpacity
        style={{
          width: wp(75),
          alignItems: "center",
          backgroundColor: colors.lightBg,
          ...padding(5),
          borderRadius: wp(5),
        }}
        onPress={() => {
          setScreen("MyProfile");
        }}
      >
        <Text style={{
          textAlign: "center",
          fontSize: wp(5),
          color: colors.text,
          marginBottom: wp(2),
        }}>
          {"Modification du profile impossible."}
        </Text>
        <Image source={require("../../commons/img/back.png")}
               style={{
                 width: wp(15),
                 height: wp(15),
               }}
        />
      </TouchableOpacity>}
    </View>;
  };

  return (<View style={{ flex: 1 }}>
    <SelectedComp />
  </View>);

};

