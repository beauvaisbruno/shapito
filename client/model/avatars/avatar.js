//@flow

export const avatarsNoDef: { [string]: number } = {
  berlioz: require("./berlioz.png"),
  duchesse: require("./duchesse.png"),
  marie: require("./marie.png"),
  toulouse: require("./toulouse.png"),
  omalley: require("./omalley.png"),
  alice: require("./alice.png"),
  siamois: require("./siamois.png"),
  garfield: require("./garfield.png"),
  dog: require("./dog.png"),
  grumpy: require("./grumpy.png"),
  butterfly: require("./butterfly.png"),
  king: require("./king.png"),
  bonnet: require("./bonnet.png"),
  catwomen: require("./catwomen.png"),
  patibulaire: require("./patibulaire.png"),
};

export const avatars: { [string]: number } = {
  ...avatarsNoDef,
  defaultAvatar: require("./defaultAvatar.png"),
};

export type Avatar = $Keys<typeof avatars>;

