//@flow
import { storiesOf } from "@storybook/react-native";
import React from "react";
import { getAppState } from "../../commons/appState";
import { generateContacts, generateMyItems } from "../../commons/story/generate";
import { MockScreen } from "../../commons/story/MockScreen";
import { pseudoRndReset } from "../../commons/util";

pseudoRndReset();
const contacts = generateContacts({ nbr: 5 });
const user = {
  email: "shapitotest.1@gmail.com",
  color: "#2143bc",
  avatar: "duchesse",
  pseudo: "Bruno",
  updateTime: Date.now() - 1000,
  contactEmails: Object.keys(contacts),
};
const items = {
  ...generateMyItems({
    nbr: 1,
    itemId: "someItemId",
    recommendedByEmail: user.email,
    recommendedToEmails: contacts,
  }),
};

const remote = {
  updateItem: ({
    item,
  }) => {
    console.log("item: ", item);
    return {
      item,
    };
  },
  addContact: ({
    user,
    contact,
  }) => {
    user.contactEmails = [...user.contactEmails, contact.email];
    return {
      user,
      contact,
    };
  },
  updateRecommendation: ({
    contact,
    itemId,
    operation,
  }) => {
    let item = getAppState(appState => appState.items[itemId]);
    if (operation === "add") {
      if (!item.recommendedToEmails.includes(contact.email)) {
        item.recommendedToEmails.push(contact.email);
      }
    }
    if (operation === "remove") {
      if (item.recommendedToEmails.includes(contact.email)) {
        item.recommendedToEmails.splice(item.recommendedToEmails.indexOf(contact.email), 1);
      }
    }
    return {

      item,
    };
  },
};
storiesOf("MyItem", module).add("MyItem", () => {
  // console.log("contacts: ", contacts);
  return (<MockScreen
    {...{
      screen: "MyItem",
      appState: {
        user,
        items,
        contacts,
      },
      screenProps: {
        itemId: "someItemId",
      },
      remote,
    }}
  />);
}).add("MyItem empty", () => {
  // console.log("contacts: ", contacts);
  return (<MockScreen
    {...{
      screen: "MyItem",
      appState: {
        user,
        items: generateMyItems({
          nbr: 1,
          itemId: "someItemId",
          recommendedByEmail: user.email,
          recommendedToEmails: {},
        }),
        contacts: [],
      },
      screenProps: {
        itemId: "someItemId",
      },
      remote,
    }}
  />);
}).add("IconsEditor", () => {
  return (<MockScreen
    {...{
      screen: "MyItem",
      appState: {
        user,
        items,
        contacts,
      },
      screenProps: {
        defaultSubScreen: "IconsEditor",
        itemId: "someItemId",
      },
      remote,
    }}
  />);
}).add("RecommendationEditor", () => {
  return (<MockScreen
    {...{
      screen: "MyItem",
      appState: {
        user,
        items,
        contacts,
      },
      screenProps: {
        defaultSubScreen: "RecommendationEditor",
        itemId: "someItemId",
      },
      remote,
    }}
  />);
});
