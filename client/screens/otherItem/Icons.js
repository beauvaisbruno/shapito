//@flow

import React from "react";
import { Image, Text, View } from "react-native";
import { useAppState } from "../../commons/appState";
import { margin, wp } from "../../commons/css";
import { emoticons } from "../../model/emoticons/emoticons";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import { noteIcons } from "../../model/noteIcons/noteIcons";

export const Icons = ({
  itemId,
}: {
  itemId: string,
}) => {
  const item = useAppState(appState => appState.items[itemId]);
  const rows = [];

  item.emoticons.forEach((emoticon) => {
    rows.push(<Image
      key={emoticon}
      style={{
        width: wp(12),
        height: wp(12),
        ...margin(1),
      }}
      source={emoticons[emoticon].icon}
    />);
  });

  const topIconStyle = {
    width: wp(15),
    height: wp(15),
  };

  return (<View style={{ flex: 1 }}>
    <View style={{
      flexDirection: "row",
    }}>
      <View style={{ alignItems: "center", ...margin(5) }}>
        <Image source={itemTypes[item.type].icon}
               style={topIconStyle} />
        <Text style={{ fontSize: wp(3) }}>{itemTypes[item.type].label}</Text>
      </View>
      <View style={{ alignItems: "center", ...margin(5) }}>
        <Image source={noteIcons[item.noteIcon].icon}
               style={topIconStyle} />
        <Text style={{ fontSize: wp(3) }}>{noteIcons[item.noteIcon].label}</Text>
      </View>
    </View>
    <View style={{
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "space-evenly",
    }}>
      {rows}
    </View>
  </View>);
};
