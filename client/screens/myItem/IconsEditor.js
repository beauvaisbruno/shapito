//@flow
import React, { useState } from "react";
import { Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import type { UpdateItemRequest, UpdateItemResponse } from "../../../server/functions/src/updateItem";
import { setAppState, useAppState } from "../../commons/appState";
import { colors, css, margin, wp } from "../../commons/css";
import { useLoader } from "../../commons/hooks";
import { remote } from "../../commons/requests/remote";
import { emoticons } from "../../model/emoticons/emoticons";
import { noteIcons } from "../../model/noteIcons/noteIcons";
import type { MyItemSubScreen } from "./MyItem";

export function IconsEditor ({
  itemId,
  setSubScreen,
}: {
  itemId: string,
  setSubScreen: (MyItemSubScreen)=>void,
}) {

  const item = useAppState(appState => appState.items[itemId]);
  const [emoticonNames, setEmoticons] = useState(item.emoticons);

  const { Loader, setLoading, setError } = useLoader();


  const emoticonComps = [];
  Object.keys(emoticons).forEach((emoticon) => {
    emoticonComps.push(<TouchableOpacity
      key={emoticon}
      onPress={() => {
        if (emoticonNames.includes(emoticon)) {
          emoticonNames.splice(emoticonNames.indexOf(emoticon), 1);
          setEmoticons([...emoticonNames]);
        } else {
          emoticonNames.push(emoticon);
          setEmoticons([...emoticonNames]);
        }
      }}><Image
      style={{
        width: wp(15),
        height: wp(15), ...margin(3),
        borderRadius: wp(5),
        borderColor: colors.successBg,
        ...(emoticonNames.includes(emoticon) ? {
          borderWidth: wp(0.2),
          backgroundColor: colors.successBg,
        } : ({}: any)),
      }}
      source={emoticons[emoticon].icon}
    /></TouchableOpacity>);
  });

  const [selectedNoteIcon, setSelectedNoteIcon] = useState(item.noteIcon);

  const noteComps = [];
  Object.keys(noteIcons).forEach((noteIcon) => {
    noteComps.push(<TouchableOpacity
      style={{ alignItems: "center" }}
      key={noteIcon}
      onPress={() => {
        setSelectedNoteIcon(noteIcon);
      }}><Image
      style={{
        width: wp(15),
        height: wp(15), ...margin(3),
        borderRadius: wp(5),
        borderColor: colors.successBg,
        ...(selectedNoteIcon === noteIcon ? {
          borderWidth: wp(0.2),
          backgroundColor: colors.successBg,
        } : ({}: any)),
      }}
      source={noteIcons[noteIcon].icon}
    />
      <Text>{noteIcons[noteIcon].label}</Text>
    </TouchableOpacity>);
  });

  return (<View style={{
    flex: 1,
    alignItems: "center",
  }}>
    <ScrollView
      style={{
        flex: 1,
      }}
      contentContainerStyle={{
        paddingBottom: wp(20),
      }}
    >

      <View style={{
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-evenly",
      }}>
        {noteComps}
      </View>
      <View style={{
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-evenly",
      }}>
        {emoticonComps}
      </View>
    </ScrollView>
    <Loader />
    <TouchableOpacity
      onPress={() => {
        setLoading(true);
        const newItem = { ...item };
        newItem.emoticons = emoticonNames;
        newItem.noteIcon = selectedNoteIcon;
        remote("updateItem", ({ item: newItem }: UpdateItemRequest))
          .then(({ item: resultItem }: UpdateItemResponse) => {
            setAppState(appState => appState.items[itemId] = resultItem);
            setSubScreen("Main");
          }).catch(error => {
          setError(true);
          setTimeout(() => {
            setSubScreen("Main");
          }, 5000);
        });
      }}>
      <Text style={{
        ...css.button,
        ...margin(3),
        width: wp(50),
        backgroundColor: colors.successBg,
        textAlign: "center",
        color: "white",
        fontSize: wp(5),
      }}>Modifier</Text>
    </TouchableOpacity>
  </View>);

}
