//@flow

import { avatarsNoDef } from "../../model/avatars/avatar";
import { emoticons } from "../../model/emoticons/emoticons";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import { noteIcons } from "../../model/noteIcons/noteIcons";
import type { ItemDescription, ItemSearch, TheItem } from "../../model/TheItem";
import type { TheUser } from "../../model/TheUser";
import { pseudoRnd } from "../util";

const thumbUrls = ["http://fr.web.img3.acsta.net/c_310_420/pictures/19/04/05/16/00/1578857.jpg", "https://fr.web.img4.acsta.net/c_310_420/medias/nmedia/18/95/04/04/20535567.jpg", "https://fr.web.img4.acsta.net/c_310_420/medias/nmedia/18/95/04/04/20535567.jpg", "https://fr.web.img2.acsta.net/c_310_420/pictures/15/10/18/18/56/052074.jpg", "https://fr.web.img5.acsta.net/c_310_420/pictures/20/10/02/12/21/3764004.png", "https://fr.web.img4.acsta.net/c_310_420/pictures/17/05/18/09/06/313747.jpg"];

const titles = ["The Mandalorian", "Star Trek: Discovery", "Star Trek: Picard", "Rogue One: A Star Wars Story", "Star Trek Into Darkness", "A Star Is Born", "Star-Crossed", "Star Wars Rebels"];

const pseudos = ["Mathilde", "Mathilde Tramoni", "Laurent75"];

const pickRandomColor = () => {
  let color = "#";
  const hexs = "0123456789ABCDE";
  for (let i = 0; i < 6; i++) {
    color += hexs[Math.floor(pseudoRnd() * hexs.length)];
  }
  return color;
};

const pickRandomKeys = (fullObj: {}, max?: number) => {
  const randomArray = [];
  Object.keys(fullObj).forEach((key) => {
    if (max && max <= randomArray.length) {
      return;
    }
    if (pseudoRnd() > 0.5) randomArray.push(key);
  });
  return randomArray;
};

const pickRandomFromArray = (fullArray) => {
  return fullArray[Math.floor(pseudoRnd() * fullArray.length)];
};

const pickRandomKeyFromObj = (fullObj) => {
  const randomArray = Object.keys(fullObj);
  return randomArray[Math.floor(pseudoRnd() * randomArray.length)];
};


export const generateMyItems = ({
  nbr,
  itemId,
  recommendedByEmail,
  recommendedToEmails,
}: {
  nbr: number,
  itemId?: string,
  recommendedByEmail: string,
  recommendedToEmails: { [email: string]: TheUser },
}) => {
  const items = {};
  const now = Date.now();

  for (let i = 0; i < nbr; i++) {
    const item: TheItem = {
      itemId: itemId ? itemId : Math.round(pseudoRnd() * 10000) + "",
      recommendedByEmail: recommendedByEmail,
      emoticons: pickRandomKeys(emoticons, 6),
      noteValue: "" + Math.round(pseudoRnd() * 50) / 10,
      publicationDate: "3 octobre 2018",
      summary: "Jackson Maine, musicien chevronné, découvre Ally, une chanteuse qui a du mal à percer. Alors que la jeune femme est sur le point de renoncer à faire carrière, Jackson tombe amoureux d'elle et la propulse sur le devant de la scène. Bientôt éclipsé par le succès d'Ally, il vit de plus en plus de mal son propre déclin…",
      recommendedToEmails: pickRandomKeys(recommendedToEmails),
      thumbUrl: pickRandomFromArray(thumbUrls),
      title: pickRandomFromArray(titles),
      noteIcon: pickRandomKeyFromObj(noteIcons),
      type: pickRandomKeyFromObj(itemTypes),
      creationDate: now - Math.floor(pseudoRnd() * 60 * 1000),
      watchedEmails: pickRandomKeys(recommendedToEmails),
      wantToWatchEmails: pickRandomKeys(recommendedToEmails),
      seenEmails: pickRandomKeys(recommendedToEmails),
    };
    items[item.itemId] = item;
  }
  return items;
};

export const generateOtherItems = ({
  nbr,
  recommendedToEmail,
  recommendedByEmails,
}: {
  nbr: number,
  recommendedToEmail: string,
  recommendedByEmails: { [email: string]: TheUser },
}): { [itemId: string]: TheItem } => {
  const items = {};
  const now = Date.now();
  for (let i = 0; i < nbr; i++) {
    const item = {
      itemId: Math.round(pseudoRnd() * 10000) + "",
      recommendedByEmail: pickRandomKeyFromObj(recommendedByEmails),
      emoticons: pickRandomKeys(emoticons, 6),
      noteValue: Math.round(pseudoRnd() * 50) / 10,
      publicationDate: "3 octobre 2018",
      summary: "Jackson Maine, musicien chevronné, découvre Ally, une chanteuse qui a du mal à percer. Alors que la jeune femme est sur le point de renoncer à faire carrière, Jackson tombe amoureux d'elle et la propulse sur le devant de la scène. Bientôt éclipsé par le succès d'Ally, il vit de plus en plus de mal son propre déclin…",
      recommendedToEmails: [recommendedToEmail],
      thumbUrl: pickRandomFromArray(thumbUrls),
      title: pickRandomFromArray(titles),
      noteIcon: pickRandomKeyFromObj(noteIcons),
      type: pickRandomKeyFromObj(itemTypes),
      creationDate: now - Math.floor(pseudoRnd() * 60 * 1000),
      watchedEmails: pseudoRnd() < 0.6 ? [recommendedToEmail] : [],
      wantToWatchEmails: pseudoRnd() < 0.5 ? [recommendedToEmail] : [],
      seenEmails: pseudoRnd() < 0.4 ? [recommendedToEmail] : [],
    };
    items[item.itemId] = item;
  }
  return items;
};

export const generateItemSearches = ({
  nbr,
}: {
  nbr: number,
}): Array<ItemSearch> => {
  const itemSearches = [];
  for (let i = 0; i < nbr; i++) {
    const description: ItemDescription = {
      noteValue: "" + Math.round(pseudoRnd() * 50) / 10,
      publicationDate: "3 octobre 2018",
      summary: "Jackson Maine, musicien chevronné, découvre Ally, une chanteuse qui a du mal à percer. Alors que la jeune femme est sur le point de renoncer à faire carrière, Jackson tombe amoureux d'elle et la propulse sur le devant de la scène. Bientôt éclipsé par le succès d'Ally, il vit de plus en plus de mal son propre déclin…",
      thumbUrl: pickRandomFromArray(thumbUrls),
      title: pickRandomFromArray(titles),
      type: pickRandomKeyFromObj(itemTypes),
    };
    itemSearches.push({ description });
  }
  return itemSearches;
};

export const generateContacts = ({
  nbr,
  contactEmails = ["user@shapito.com"],
}: {
  nbr: number,
  contactEmails?: Array<string>
}) => {
  const contacts = {};

  for (let i = 1; i <= nbr; i++) {
    const contact = {
      email: "shapito" + i + "@gmail.com",
      pseudo: pickRandomFromArray(pseudos),
      "avatar": "defaultAvatar",
      contactEmails,
      ...(pseudoRnd() > 0.2) ? {
        color: pickRandomColor(),
        avatar: pickRandomKeyFromObj(avatarsNoDef),
      } : {},

    };
    contacts[contact.email] = contact;
  }
  return contacts;
}

