//@flow

const {
  throwError,
} = require("./commons/util");
const { checkUser } = require("./commons/checkUser");
const functions = require("firebase-functions");
const { firestore } = require("./commons/firebase");

import type { Emoticon } from "../../../client/model/emoticons/emoticons";
import type { NoteIcon } from "../../../client/model/noteIcons/noteIcons";
import type { ItemDescription, TheItem } from "../../../client/model/TheItem";
import type { TheUser } from "../../../client/model/TheUser";

export type AddItemRequest = {|
  description: ItemDescription,
  noteIcon: NoteIcon,
  emoticons: Array<Emoticon>,
|};
export type AddItemResponse = { item: TheItem, user: TheUser };

exports.addItemFn = async function addItemFn ({
  description,
  noteIcon,
  emoticons,
  user,
  now = Date.now(),
}: {
  description: ItemDescription,
  noteIcon: NoteIcon,
  emoticons: Array<Emoticon>,
  user: TheUser,
  now: number,
}): Promise<AddItemResponse> {
  // generate and add id for the item
  const itemRef = await firestore.collection("TheItems").doc();
  const newItem: TheItem = {
    ...description,
    noteIcon,
    emoticons,
    itemId: itemRef.id,
    recommendedByEmail: user.email,
    creationDate: now,
    watchedEmails: [],
    recommendedToEmails: [],
    wantToWatchEmails: [],
    seenEmails: [],
  };

  const itemResult = await itemRef.set(newItem);
  // console.log("new item: ",item);
  user.recommendedByMeItemIds = [...user.recommendedByMeItemIds, newItem.itemId];

  await firestore.collection("TheUsers").doc(user.email).set(user);

  return { item: newItem, user };
};

exports.addItem = functions.https.onCall(async (request, context) => {
  return await exports.addItemFn({
    ...request,
    user: await checkUser({ request, context }),
  });

});
