//@flow
import type { TheUser } from "../../../../client/model/TheUser";

const downloadText = "Avec Shapito partager les films, livres ou jeux que vous aimez.<br/> " +
  "Pour télécharger l'application cliquez sur le lien ci-dessous :<br/> " +
  "- <a href='https://play.google.com/store/apps/details?id=tech.equipage.shapito'>Shapito pour Android</a><br/>" +
  // "- <a href='https://apps.apple.com/fr/app/shapito/id'>Shapito pour Iphone</a>" +
  "<br/>" +
  "Shapito est une application open source réalisée par <a href='https://equipage.tech'>equipage.tech</a><br/>" +
  "code source sur github: <a href='https://github.com/beauvaisbruno/shapito'>github.com/beauvaisbruno/shapito</a><br/><br/>";

const gif = "<img src='https://equipage.tech/shapito.gif'/><br/>";

exports.getInvitationEmail = (user: TheUser): string => {
  return user.pseudo + " vous a invité à utiliser l'application Shapito.<br/> "
    + downloadText + gif;
};

exports.getRecommendationEmail = ({ user, contact }: { user: TheUser, contact: TheUser }) => {

  let email = user.pseudo +
    " vous a fait une une recommandation avec l'application Shapito.<br/><br/>";

  //contact has not installed yet the app
  if (contact.avatar === "defaultAvatar") {
    email += downloadText;
  }

  return email + gif;
};
