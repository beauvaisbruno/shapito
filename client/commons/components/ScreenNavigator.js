//@flow
import type { Node } from "react";
import React from "react";

import { View } from "react-native";
import { useUpdate } from "../util";
import { DisconnectedMessage } from "./DisconnectedMessage";
import type { ScreenName } from "../screensLoader";

const updateRef = { current: undefined };
const navigatorRef: {
  currentScreen?: {
    screenName: ScreenName,
    screenProps: {},
  },
  prevScreen?: {
    screenName: ScreenName,
    screenProps: {},
  }
} = {};

export const setScreen = (screenName: ScreenName, screenProps?: {} = {}) => {
  console.log("setScreen: ", screenName, screenProps);
  // console.trace();
  if (navigatorRef.currentScreen) {
    navigatorRef.prevScreen = navigatorRef.currentScreen;
  }

  navigatorRef.currentScreen = { screenName, screenProps };
  if (updateRef.current) updateRef.current();
};

export const getCurrentScreen: ()=> ScreenName = () => {
  if (!navigatorRef.currentScreen) {
    throw Error("currentScreen is undefined");
  }
  return navigatorRef.currentScreen.screenName;
};

export const isCurrentScreen: (ScreenName)=> boolean = (screen) => {
  if (!navigatorRef.currentScreen) {
    throw Error("currentScreen is undefined");
  }
  return screen === navigatorRef.currentScreen.screenName;
};

export const prevScreen = () => {
  console.log("prevScreen");
  if (!navigatorRef.prevScreen) {
    console.error("no previous screen");
    setScreen("MyProfile");
    return;
  }
  setScreen(navigatorRef.prevScreen.screenName, navigatorRef.prevScreen.screenProps);
};

export function ScreenNavigator ({
  screens,
  initScreen = "SplashScreen",
  initScreenProp = {},
  splashScreen = false,
}: {
  screens: { [ScreenName]: ()=>Node },
  initScreen?: ScreenName,
  initScreenProp?: {},
  splashScreen?: boolean
}): Node {

  if (!navigatorRef.currentScreen) {
    navigatorRef.currentScreen = {
      screenName: initScreen,
      screenProps: initScreenProp,
    };
  }

  updateRef.current = useUpdate();
  // console.log("currentScreen: ", currentScreenRef.current, ", splashScreen: ", splashScreen);

  if (splashScreen) return <screens.SplashScreen />;

  let Screen = screens[navigatorRef.currentScreen.screenName];
  if (Screen === undefined) {
    console.error("Unknown screenName: ", navigatorRef.currentScreen.screenName,
      ", current screens: ", screens);
    return <screens.MyProfile />;
  }
  return <View style={{ flex: 1 }} testID={navigatorRef.currentScreen.screenName}>
    <DisconnectedMessage />
    <Screen {...navigatorRef.currentScreen.screenProps} />
  </View>
    ;
}

