//@flow
const nodemailer = require("nodemailer");
const functions = require("firebase-functions");

exports.sendEmail = async ({
  email,
  subject,
  body,
}: {
  email: string,
  subject: string,
  body: string,
}) => {
  //Do not place your password here, your risk to commit it ! Instead use .runtimeconfig.json
  //see https://firebase.google.com/docs/functions/local-emulator#set_up_functions_configuration_optional
  let gmailEmail;
  let gmailPassword;
  try {
    if (functions.config().gmail) {
      gmailEmail = functions.config().gmail.email;
      gmailPassword = functions.config().gmail.password;
    }
  } catch (error) {
    console.error(error);
  }
  if (!gmailEmail || !gmailPassword) {
    console.error("*******************************\n" +
      "No email credential is provided. to set credential run this command \n" +
      "firebase functions:config:set gmail.email=\"changeByYourEmail\" gmail.password=\"changeByYourPassword\"\n" +
      "Then set the gmail account as less secure app.\n" +
      "To use with local emulator run\n" +
      "cd ./server/functions/ && firebase functions:config:get > .runtimeconfig.json");
    return;
  }
  // console.log("gmailEmail: ", gmailEmail);
  // console.log("gmailPassword: ", gmailPassword);
  try {
    const mailTransport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: gmailEmail,
        pass: gmailPassword,
      },
    });

    await mailTransport.sendMail({
      subject,
      text: body,
      html: body,
      to: email,
    });
    // console.log("Email sen to "+email);
    // console.log("email sent to ", email, " subject: ", subject);

  } catch (error) {
    console.error("sendEmail error: ", error, ", email: ", email, ", subject: ", subject);
  }
};
