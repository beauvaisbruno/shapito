//@flow
import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { margin, wp } from "../../commons/css";
import type { MyProfileSubscreen } from "./MyProfile";

const iconStyle = {
  height: wp(10),
  width: wp(10), ...margin(1),
};
const btnStyle = (highlight: boolean) => ({
  flexDirection: "row",
  alignItems: "center",
  width: wp(45.9), // backgroundColor:"#dbdbdb",
  borderWidth: wp(0.5),
  borderRadius: wp(10),
  ...margin(2),
  marginBottom: 0,
  borderColor: highlight ? "#000" : "#dbdbdb",
});

const textStyle = {
  fontSize: wp(4),
  flex: 1,
  flexWrap: "wrap",
};

export const ProfileTabMenu = ({
  setSubScreen,
  subScreen,
}: {
  setSubScreen: (MyProfileSubscreen)=>void,
  subScreen: MyProfileSubscreen,
}) => {
  function isCurrentSubscreen (pSubScreen: MyProfileSubscreen) {
    return subScreen === pSubScreen;
  }

  return (<View style={{
    flexDirection: "row",
    flexWrap: "wrap",
    width: wp(100),
    marginBottom: wp(2),
  }}>
    <TouchableOpacity
      style={btnStyle(isCurrentSubscreen("recommendedForMe"))}
      onPress={() => setSubScreen("recommendedForMe")}
    >
      <Image style={iconStyle} source={require("../../commons/img/ok.png")} />
      <Text style={textStyle}>{"Recommandés pour moi"}</Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={btnStyle(isCurrentSubscreen("recommendedByMe"))}
      onPress={() => setSubScreen("recommendedByMe")}
    >
      <Image style={iconStyle} source={require("../../commons/img/recommandation.png")} />
      <Text style={textStyle}>{"Je recommande"}</Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={btnStyle(isCurrentSubscreen("wantToWatch"))}
      onPress={() => setSubScreen("wantToWatch")}
    >
      <Image style={iconStyle} source={require("../../commons/img/interest.png")} />
      <Text style={textStyle}>{"Ça m'intéresse"}</Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={btnStyle(isCurrentSubscreen("watched"))}
      onPress={() => setSubScreen("watched")}
    >
      <Image style={iconStyle} source={require("../../commons/img/eye.png")} />
      <Text style={textStyle}>{"Déjà vu"}</Text>
    </TouchableOpacity>
  </View>);
};
