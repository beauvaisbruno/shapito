//@flow

import { setJSExceptionHandler, setNativeExceptionHandler } from "react-native-exception-handler";
import { logout } from "./requests/connection";

export const setExceptionHandlers = () => {
  setNativeExceptionHandler(
    (error) => {
      console.log("globalNative error : ", error);
      logout();
    },
    false,
    true,
  );

  setJSExceptionHandler((error, isFatal) => {
    console.log("globalJS isFatal: ", isFatal, ", error: ", error);
    if (isFatal) {
      logout();
    }
  }, false);
};
