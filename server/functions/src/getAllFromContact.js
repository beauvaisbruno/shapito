//@flow
import type { TheItem } from "../../../client/model/TheItem";
import type { TheUser } from "../../../client/model/TheUser";

const functions = require("firebase-functions");
const { getAllItems } = require("./commons/getAllItems");
const { getAllContacts } = require("./commons/getAllContacts");
const {
  throwError,

} = require("./commons/util");
const { checkUser } = require("./commons/checkUser");

export type GetAllFromContactRequest = {| contact: TheUser |}
export type GetAllFromContactResponse = {|
  contacts: { [email: string]: TheUser },
  items: { [itemId: string]: TheItem },
  user: TheUser
|};

exports.getAllFromContactFn = async ({
  user,
  contact,
}: {
  user: TheUser,
  ...GetAllFromContactRequest,
}) => {

  if (!user.contactEmails.includes(contact.email)) {
    throwError("user not allowed to get description from this contact", {
      user,
      contact,
    });
  }
  console.log("contact: ", contact);

  return ({
    contacts: await getAllContacts(contact.contactEmails),
    items: await getAllItems(contact),
    user,
  }: GetAllFromContactResponse);
};

exports.getAllFromContact = functions.https.onCall(async (request, context) => {
  return await exports.getAllFromContactFn({
    ...request,
    user: await checkUser({ request, context }),
  });

});
