//@flow

import type { TheUser } from "../../../../client/model/TheUser";

const {
  throwError,
} = require("./util");
const { firestore } = require("./firebase");

exports.getAllContacts = async (contactEmails: Array<string>): Promise<{ [email: string]: TheUser }> => {
  const contacts = {};

  const docRefs = [];
  console.log("getAll contactEmails: ", contactEmails);
  contactEmails.forEach((email) => {
    docRefs.push(firestore.collection("TheUsers").doc(email));
  });

  // console.log("getAll contacts docRefs: ", docRefs.length);
  if (docRefs.length > 0) {
    const docs = await firestore.getAll(...docRefs);

    docs.forEach((doc) => {
      // console.log("contacts exists: ", doc.exists, ", id: ", doc.id);
      if (!doc.exists) {
        console.warn("contacts not exist: ", doc);
        return;
      }
      const data = (doc.data(): TheUser);
      // console.log("contacts data: ", data);
      contacts[data.email] = data;
    });
  }
  // console.log("contacts: ", contacts);
  return contacts;
};
