//@flow
import React from "react";
import { Image, TouchableOpacity, View } from "react-native";
import type { DeleteItemRequest, DeleteItemResponse } from "../../../server/functions/src/deleteItem";
import { setAppState, useAppState } from "../../commons/appState";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { margin, wp } from "../../commons/css";
import { useLoader } from "../../commons/hooks";
import { remote } from "../../commons/requests/remote";
import { emoticons } from "../../model/emoticons/emoticons";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import { noteIcons } from "../../model/noteIcons/noteIcons";
import type { MyItemSubScreen } from "./MyItem";

export const Icons = ({
  itemId,
  setSubScreen,
}: {
  itemId: string,
  setSubScreen: (MyItemSubScreen)=>void,
}) => {
  const item = useAppState(appState => appState.items[itemId]);
  const { Loader, setLoading, setError } = useLoader();
  const rows = [];

  item.emoticons.forEach((emoticon) => {
    rows.push(<Image
      key={emoticon}
      style={{
        width: wp(15),
        height: wp(15),
        // ...margin(3),
      }}
      source={emoticons[emoticon].icon}
    />);
  });

  const topIconStyle = {
    width: wp(15),
    height: wp(15), ...margin(5),
  };

  const btnStyle = {
    width: wp(10),
    height: wp(10), ...margin(3),
  };
  return (<View style={{ flex: 1 }}>
    <View style={{
      flexDirection: "row",
    }}>
      <Image source={itemTypes[item.type].icon}
             style={topIconStyle} />
      <Image source={noteIcons[item.noteIcon].icon}
             style={topIconStyle} />
    </View>
    <View style={{
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "space-evenly",
    }}>
      {rows}
    </View>
    <Loader />
    <View style={{
      flexDirection: "row",
      justifyContent: "space-evenly",
    }}>
      <TouchableOpacity onPress={() => {
        setSubScreen("IconsEditor");
      }}>
        <Image source={require("../../commons/img/edit.png")} style={btnStyle} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => {
        setLoading(true);
        remote("deleteItem", ({ itemId }: DeleteItemRequest))
          .then(({ user }: DeleteItemResponse) => {
            setAppState(appState => appState.user = user);
            setScreen("MyProfile", { initSubScreen: "recommendedByMe" });
          }).catch(() => {
          setError(true);
        });
      }}>
        <Image
          source={require("../../commons/img/deleteBtn.png")} style={btnStyle}
        />
      </TouchableOpacity>
    </View>
  </View>);
};
