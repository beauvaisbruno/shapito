const { firestore } = require("../commons/firebase");

const resetCollection = async (collectionPath) => {
  const batch = firestore.batch();
  const all = await firestore.collection(collectionPath).listDocuments();
  all.map((row) => {
    batch.delete(row);
  });
  await batch.commit();
};
export const resetFirestore = async () => {
  await resetCollection("TheUsers");
  await resetCollection("TheItems");
};
