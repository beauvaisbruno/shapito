//@flow
import { PermissionsAndroid } from "react-native";
import { getAllWithoutPhotos } from "react-native-contacts";
import type { NewContact } from "../../model/TheUser";
import { getAppState } from "../appState";
import { DEV } from "../DEV";

export let osContacts = [];

if (DEV.mockOsContacts) {
  osContacts = [{
    emailAddresses: [{ email: "shapitotest.2@gmail.com" }],
    givenName: "Test",
    familyName: "Pseudo",
  }];
  for (let i = 0; i < 8; i++) {
    osContacts.push({
      emailAddresses: [{ email: "contact" + i + "@shapito.com" }],
      givenName: "Test" + i,
    });
  }
}

export const getOsContacts: ()=>Promise<Array<NewContact>> = async () => {

  await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
    "title": "Contacts",
    "message": "Voir vos contacts.",
    "buttonPositive": "Accepter",
  });
  // console.log("getAllWithoutPhotos...");
  let completeContacts = await getAllWithoutPhotos();
  console.log("getAllWithoutPhotos completeContacts : ", completeContacts.length);
  if (DEV.mockOsContacts) completeContacts = osContacts;
  const contacts = getAppState(appState => appState.contacts);
  const shortContacts: Array<NewContact> = [];
  completeContacts.forEach((contact) => {
    try {
      if (contact.emailAddresses && contact.emailAddresses.length > 0) {
        const email = contact.emailAddresses[0].email;
        if (!email) {
          return;
        }
        if (contacts[email]) return;
        if (email.includes("@")) {
          shortContacts.push({
            email,
            pseudo: (contact.givenName +
              ((contact.familyName) ? " " + contact.familyName : "")).trim(),
          });
        }
      }
    } catch (error) {
      console.warn("getOsContacts error: ", error);
    }
  });
  if (DEV.mockOsContacts) console.log("getAllWithoutPhotos shotContacts: ", shortContacts);
  return shortContacts;
};
