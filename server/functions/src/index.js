const { addItem } = require("./addItem");
const { updateItem } = require("./updateItem");
const { updateItemFromContact } = require("./updateItemFromContact");
const { updateRecommendation } = require("./updateRecommendation");
const { deleteItem } = require("./deleteItem");
const { getAll } = require("./getAll");
const { getAllFromContact } = require("./getAllFromContact");
const { createOrUpdateProfile } = require("./createOrUpdateProfile");
const { addContact } = require("./addContact");
// const { simple } = require("./simple");
const { throwError } = require("./commons/util");

try {
  module.exports = {
    getAll,
    getAllFromContact,
    addContact,
    createOrUpdateProfile,
    addItem,
    updateItem,
    deleteItem,
    updateItemFromContact,
    updateRecommendation,
    // simple,
  };
} catch (error) {
  throwError("error: ", error);
}



