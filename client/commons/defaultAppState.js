//@flow
import type { TheItem } from "../model/TheItem";
import type { TheUser } from "../model/TheUser";
import DEV from "./DEV";
import { getRandomColor } from "./util";

export type AuthProvider = "email" | "google";

export type AppState = {|
  user: TheUser,
  items: { [string]: TheItem },
  contacts: { [string]: TheUser },
  password: ?string,
  authProvider: ?AuthProvider,
  lastGetAllTime?: number,
|}

export const defAppState: AppState = {
  user: {
    email: DEV.fillForm ? "shapitotest.1@gmail.com" : "",
    pseudo: DEV.fillForm ? "Bruno" : "",
    color: getRandomColor(),
    avatar: "defaultAvatar",
    contactEmails: [],
    recommendedForMeItemIds: [],
    recommendedByMeItemIds: [],
  },
  items: {},
  contacts: {},
  password: null,
  authProvider: null,
};

export const defaultAppState = defAppState;
