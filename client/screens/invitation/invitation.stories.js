//@flow
import { storiesOf } from "@storybook/react-native";
import React from "react";
import { MockScreen } from "../../commons/story/MockScreen";

storiesOf("Invitation", module).add("Invitation", () => {
  return (<MockScreen
    {...{
      screen: "Invitation",
      appState: {},
      remote: {
        addContact: (contact) => ({
          user: {},
          contact,
        }),
      },
    }}
  />);
});
