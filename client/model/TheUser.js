//@flow

import type { Avatar } from "./avatars/avatar";

export type NewContact = {|
  pseudo: string,
  email: string,
|}

export type Profile = {|
  pseudo: string,
  color: string,
  avatar: Avatar,
|}
export type TheUser = {|
  ...Profile,
  email: string,
  uid?: string,
  contactEmails: Array<string>,
  recommendedForMeItemIds: Array<string>,
  recommendedByMeItemIds: Array<string>,
|}
