//@flow

import React from "react";
import { Image, ScrollView, TouchableOpacity, View } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import { getAppState, useAppState } from "../../commons/appState";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { wp } from "../../commons/css";
import { lightenColor2 } from "../../commons/util";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import { Icons } from "./Icons";
import type { MyItemSubScreen } from "./MyItem";
import { MyItemAvatarsList } from "./MyItemAvatarsList";
import { MyItemEmptyListLinks } from "./MyItemEmptyListLinks";

export const Main = ({
  itemId,
  setSubScreen,
}: {
  itemId: string,
  setSubScreen: (MyItemSubScreen)=>void,
}) => {
  const item = useAppState(appState => appState.items[itemId]);
  const user = getAppState(appState => appState.user);
  console.log("item: ", item);

  return (<View style={{ flex: 1 }}>
    <ScrollView
      style={{ flex: 1 }}
      contentContainerStyle={{
        paddingBottom: wp(20),
      }}
    >
      <View style={{
        flexDirection: "row",
        width: wp(100),
        backgroundColor: lightenColor2(user.color),
      }}>
        <Icons {...{
          itemId,
          setSubScreen,
        }} />
        <AutoHeightImage
          source={{ uri: item.thumbUrl }}
          width={wp(50)}
          fallbackSource={itemTypes[item.type].defaultThumb}
        />
      </View>
      {item.recommendedToEmails.length ===
      0 &&
      <MyItemEmptyListLinks {...{
        item,
        setSubScreen,
      }} />}
      <MyItemAvatarsList {...{
        field: "recommendedToEmails",
        itemId,
        setSubScreen,
        icon: require("../../commons/img/recommandation.png"),
        label: "Recommandé pour",
      }} />
      <MyItemAvatarsList {...{
        field: "wantToWatchEmails",
        itemId,
        setSubScreen,
        icon: require("../../commons/img/interest.png"),
        label: "Ils sont intéressés",
      }} />
      <MyItemAvatarsList {...{
        field: "watchedEmails",
        itemId,
        setSubScreen,
        icon: require("../../commons/img/eye.png"),
        label: "Ils connaissent déjà",
      }} />
      <MyItemAvatarsList {...{
        field: "seenEmails",
        itemId,
        setSubScreen,
        icon: require("../../commons/img/ok.png"),
        label: "Ils ont vu la recommandation",
      }} />
    </ScrollView>
    <View
      style={{
        position: "absolute",
        bottom: 0,
        width: wp(100),
        justifyContent: "space-evenly",
        flexDirection: "row",
        paddingBottom: wp(2),
        paddingTop: wp(2),
        backgroundColor: "rgba(255,255,255,0.8)",
      }}
    >
      <TouchableOpacity
        onPress={() => {
          setScreen("MyProfile");
        }}
      >
        <Image
          source={require("../../commons/img/back.png")}
          style={{
            width: wp(15),
            height: wp(15),
          }}
        />
      </TouchableOpacity>
    </View>
  </View>);
};
