//@flow

import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import type { NewContact, TheUser } from "../../model/TheUser";
import { colors, margin, wp } from "../css";
import { useLoader } from "../hooks";
import { addContact } from "../requests/addContact";

export function OsContact ({
  contact,
  onContactSelected,
}: {
  contact: NewContact,
  onContactSelected: (TheUser)=>void,
}) {
  const { Loader, setLoading, setError } = useLoader();

  const btnStyle = {
    width: wp(15),
    height: wp(15), ...margin(2),
  };
  //lsp https://youtrack.jetbrains.com/issue/WEB-32803

  return <View
    key={contact.email}
    style={{
      width: wp(100),
      borderBottomColor: colors.lineBorder,
      borderBottomWidth: wp(0.1),
      flexDirection: "row",
      alignItems: "center",
    }}>
    <View style={{ flex: 1, ...margin(2) }}>
      <Text style={{
        fontSize: wp(5),
        fontWeight: "bold",
      }}>{contact.pseudo}</Text>
      <Text style={{ fontSize: wp(5) }}>{contact.email}</Text>
      <Loader />
    </View>
    <TouchableOpacity onPress={() => {
      setLoading(true);
      addContact(contact).then((resultContact) => {
        setLoading(false);
        onContactSelected(resultContact);
      }).catch(error => {
        setError(true);
      });
    }}>
      <Image source={require("../img/inviteFriend.png")}
             style={btnStyle}
      />
    </TouchableOpacity>
  </View>;
}
