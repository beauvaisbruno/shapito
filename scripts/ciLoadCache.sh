#!/bin/sh
mkdir -p ~/.cache/node_modules
cp -r ~/.cache/node_modules ~/clone/node_modules

mkdir -p ~/.cache/server/functions/node_modules
cp -r ~/.cache/server/functions/node_modules ~/clone/server/functions/node_modules

mkdir -p ~/.cache/android/.gradle
cp -r ~/.cache/android/.gradle ~/clone/android/.gradle
