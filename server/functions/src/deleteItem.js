import type { TheItem } from "../../../client/model/TheItem";
import type { TheUser } from "../../../client/model/TheUser";

const {
  throwError,

} = require("./commons/util");
const { checkUser } = require("./commons/checkUser");
const functions = require("firebase-functions");
const { removeFromFieldArr } = require("./commons/util");
const { getAllContacts } = require("./commons/getAllContacts");
const { firestore } = require("./commons/firebase");

export type DeleteItemRequest = { itemId: string };
export type DeleteItemResponse = { user: TheUser };

async function deleteItemFn ({
  itemId,
  user,
}: {
  ...DeleteItemRequest,
  user: TheUser
}): DeleteItemResponse {
  console.log("!user.recommendedByMeItemIds.includes(itemId)");
  if (!user.recommendedByMeItemIds.includes(itemId)) {
    throwError("deleteItemFn, recommendedByMeItemIds", {
      user,
      itemId,
    });
  }

  const resultItemGet = await firestore.collection("TheItems").doc(itemId).get();

  if (!resultItemGet.exists) {
    throwError("deleteItemFn, item doesn't exist", {
      user,
      itemId,
    });
  }

  const item: TheItem = resultItemGet.data();
  if (item.recommendedByEmail !== user.email) {
    throwError("deleteItemFn, Wrong recommendedByEmail", {
      user,
      item,
    });
  }

  const contacts = await getAllContacts(item.recommendedToEmails);
  const setBatch = firestore.batch();
  Object.keys(contacts).forEach((email) => {
    const contact = removeFromFieldArr(contacts[email], "recommendedForMeItemIds", itemId);
    console.log("contact: ", contact);
    setBatch.set(
      firestore.collection("TheUsers").doc(email),
      contact,
    );
  });
  await setBatch.commit();

  user = removeFromFieldArr(user, "recommendedByMeItemIds", itemId);
  const resultUserSet = await firestore.collection("TheUsers").doc(user.email).set(user);

  const resultItemDelete = await firestore.collection("TheItems").doc(itemId).delete();

  return { user };
}

exports.deleteItem = functions.https.onCall(async (request, context) => {
  return deleteItemFn({
    ...request,
    user: await checkUser({ request, context }),
  });
});
