//@flow

import { configure, getStorybookUI } from "@storybook/react-native";
import React from "react";
import { loadStories } from "./storyLoader";
// eslint-disable-next-line

let storyLoaded = false;
configure(() => {
  try {
    if (!storyLoaded) {
        loadStories();
      storyLoaded = true;
    }
  } catch (error) {
    console.log("loadStories error: ", error);
  }
}, module);

export const StorybookUI = getStorybookUI({
  asyncStorage: require("@react-native-async-storage/async-storage").default,
});

