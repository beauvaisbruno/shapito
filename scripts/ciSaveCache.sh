#!/bin/sh
rm -r ~/.cache/node_modules
cp -r ~/clone/node_modules ~/.cache/node_modules

rm -r ~/.cache/server/functions/node_modules
cp -r ~/clone/server/functions/node_modules ~/.cache/server/functions/node_modules

rm -r ~/.cache/android/.gradle
cp -r ~/clone/android/.gradle ~/.cache/android/.gradle
