/* eslint-disable no-undef */

import mockAsyncStorage from "@react-native-async-storage/async-storage/jest/async-storage-mock";

jest.mock("@react-native-async-storage/async-storage", () => mockAsyncStorage);
jest.mock("@react-native-firebase/auth", () => jest.fn());
jest.mock("@react-native-community/google-signin", () => ({ GoogleSignin: jest.fn() }));
jest.mock("@react-native-firebase/functions", () => {
  return () => ({
    useFunctionsEmulator: jest.fn(),
  });
});
jest.mock("@react-native-community/netinfo", () => ({ addEventListener: jest.fn() }));

