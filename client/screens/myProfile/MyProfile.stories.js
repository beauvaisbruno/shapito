//@flow

import { storiesOf } from "@storybook/react-native";
import React from "react";
import { generateContacts, generateMyItems, generateOtherItems } from "../../commons/story/generate";
import { MockScreen } from "../../commons/story/MockScreen";
import { pseudoRndReset } from "../../commons/util";
import type { MyProfileSubscreen } from "./MyProfile";

const user = {
  email: "shapitotest.1@gmail.com",
  color: "#2143bc",
  avatar: "marie",
  pseudo: "Bruno",
  updateTime: Date.now() - 1000,
  recommendedForMeItemIds: [],
  recommendedByMeItemIds: [],
};

pseudoRndReset();
const contacts = generateContacts({ nbr: 5 });
const myItems = generateMyItems({
  nbr: 5,
  recommendedByEmail: user.email,
  recommendedToEmails: contacts,
});

const otherItems = generateOtherItems({
  nbr: 15,
  recommendedToEmail: user.email,
  recommendedByEmails: contacts,
});

const items = {
  ...myItems, ...otherItems,
};

user.recommendedForMeItemIds = Object.keys(otherItems);
user.recommendedByMeItemIds = Object.keys(myItems);

storiesOf("MyProfile", module)
  .add("RecommendedForMe", () => {
    return (<MockScreen
      {...{
        screen: "MyProfile",
        remote: {
          getAll: {
            user,
            contacts,
            items,
          },
        },
      }}
    />);
  })
  .add("RecommendedForMe empty", () => {
    return (<MockScreen
      {...{
        screen: "MyProfile",
        remote: {
          getAll: {
            user: { ...user, recommendedForMeItemIds: [] },
            contacts: {},
            items: {},
          },
        },
      }}
    />);
  })
  .add("RecommendedByMe", () => {
    return (<MockScreen
      {...{
        screen: "MyProfile",
        remote: {
          getAll: {
            user: user,
            contacts,
            items,
          },
        },
        screenProps: {
          initSubScreen: ("recommendedByMe": MyProfileSubscreen),
        },
      }}
    />);
  })
  .add("RecommendedByMe empty", () => {
    return (<MockScreen
      {...{
        screen: "MyProfile",
        remote: {
          getAll: {
            user: { ...user, recommendedByMeItemIds: [] },
            contacts: {},
            items: {},
          },
        },
        screenProps: {
          initSubScreen: ("recommendedByMe": MyProfileSubscreen),
        },
      }}
    />);
  })
;
