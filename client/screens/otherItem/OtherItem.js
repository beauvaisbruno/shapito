//@flow

import React, { useEffect } from "react";
import { Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import type { UpdateItemFromContactRequest, UpdateItemFromContactResponse } from "../../../server/functions/src/updateItemFromContact";
import { getAppState, setAppState, useAppState } from "../../commons/appState";
import { prevScreen } from "../../commons/components/ScreenNavigator";
import { colors, margin, padding, wp } from "../../commons/css";
import { useLoader } from "../../commons/hooks";
import { remote } from "../../commons/requests/remote";
import { ago, lightenColor, lightenColor2 } from "../../commons/util";
import { avatars } from "../../model/avatars/avatar";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import { scrapTypes } from "../../model/scrapTypes/scrapTypes";
import type { TheItem } from "../../model/TheItem";
import type { TheUser } from "../../model/TheUser";
import { Icons } from "./Icons";

const Header = ({ itemId, contact }
  : { itemId: string, contact: TheUser }) => {
  const item = useAppState(appState => appState.items[itemId]);
  return (<View style={{
    backgroundColor: lightenColor(contact.color),
    flexDirection: "row",
    alignItems: "center",
  }}>

    <Image source={avatars[contact.avatar] || avatars.defaultAvatar}
           style={{
             width: wp(20),
             height: wp(20), ...margin(2),
           }} />

    <Text
      style={{
        fontSize: item.title.length < 20 ? wp(9) : wp(7),
        textAlign: "center",
        flex: 1,
      }}>{item.title}
    </Text>
  </View>);
};

function Infos ({
  contact,
  item,
}: {
  contact: TheUser,
  item: TheItem,
}) {
  let NoteComp;
  let PublicationDateComp;
  if (item.scrapType) {
    if (item.noteValue) NoteComp = scrapTypes[item.scrapType].NoteComp;
    if (item.publicationDate) PublicationDateComp = scrapTypes[item.scrapType].PublicationComp;
  }

  const recommendedByMe = item.recommendedByEmail === getAppState(appState => appState.user.email);
  const marginTop = wp(2);

  return <View style={{
    marginLeft: wp(2),
    marginRight: wp(2),
  }}>
    <Text style={{
      fontSize: wp(4),
      marginTop,
    }}>
      {"Proposé par " + (recommendedByMe ? "vous" : contact.pseudo) + ", "
      + ago(item.creationDate) + "."}
    </Text>
    {NoteComp && <NoteComp {...{
      value: (item.noteValue: any),
      pc: 5,
      style: { marginTop },
    }} />}
    {PublicationDateComp && <PublicationDateComp {...{
      value: (item.publicationDate: any),
      pc: 5,
      style: { marginTop },
    }} />}
    <Text style={{
      fontSize: wp(5),
      marginTop,
    }}>
      {"Résumé"}
    </Text>
    <Text style={{
      fontSize: wp(3.5),
      textAlign: "justify",
    }}>
      {item.summary}
    </Text>
  </View>;
}

function ActionBtns ({ item }
  : { item: TheItem }) {
  const { Loader, setLoading, setError } = useLoader();
  const user = getAppState(appState => appState.user);
  const iconStyle = {
    height: wp(12),
    width: wp(12),
  };
  const btnStyle = {
    ...padding(2),
    borderRadius: wp(5),
    marginTop: wp(2),
    width: wp(45),
    alignItems: "center",
    borderWidth: wp(1), // borderColor: contact.color
    borderColor: colors.lineBorder,
  };
  const txtStyle = { fontSize: wp(4) };

  function updateItem (fieldToFill) {
    setLoading(true);
    // console.log("updateItem fieldToMerge: ", fieldToFill);
    remote("updateItemFromContact", ({
      fieldToFill,
      itemId: item.itemId,
    }: UpdateItemFromContactRequest)).then(({ item }: UpdateItemFromContactResponse) => {
      setLoading(false);
      setAppState(appState => {
        appState.items[item.itemId] = item;
      });
    }).catch(error => {
      setError(true);
    });
  }

  let showWantToWatchButton = true;
  let showWatchedButton = true;
  if (item.wantToWatchEmails.includes(user.email)) {
    showWantToWatchButton = false;
  }
  if (item.watchedEmails.includes(user.email)) {
    showWantToWatchButton = false;
    showWatchedButton = false;
  }

  return <View style={{ flex: 1, alignItems: "center" }}>
    <Loader />
    <View style={{
      flexDirection: "row",
      justifyContent: "space-evenly",
      alignItems: "center",
    }}>

      {showWantToWatchButton && <TouchableOpacity
        style={btnStyle}
        onPress={() => {
          updateItem("wantToWatchEmails");
        }}>
        <Image style={iconStyle} source={require("../../commons/img/interest.png")} />
        <Text style={txtStyle}>
          {"Ça m'intéresse"}
        </Text>
      </TouchableOpacity>}
      {showWatchedButton && <TouchableOpacity
        style={btnStyle}
        onPress={() => {
          updateItem("watchedEmails");
        }}>
        <Image
          style={iconStyle}
          source={require("../../commons/img/eye.png")} />
        <Text style={txtStyle}>
          {"Je l'ai déjà vue"}
        </Text>
      </TouchableOpacity>}
    </View>
  </View>;
}

export const OtherItem = ({ itemId }
  : { itemId: string }) => {
  console.log("itemId: ", itemId);

  const item = useAppState(appState => appState.items[itemId]);
  const contact = getAppState(appState => appState.contacts[item.recommendedByEmail]);

  const recommendedForMeItemIds = getAppState(appState => appState.user.recommendedForMeItemIds);
  const isRecommendedToMe = recommendedForMeItemIds.includes(itemId);

  useEffect(() => {
    if (isRecommendedToMe) {
      remote("updateItemFromContact", ({
        fieldToFill: "seenEmails",
        itemId: item.itemId,
      }: UpdateItemFromContactRequest)).then(({ item }: UpdateItemFromContactResponse) => {
        setAppState(appState => {
          appState.items[item.itemId] = item;
        });
      }).catch(error => {
        //do nothing special
      });
    }
  }, []);

  return (<View style={{ flex: 1 }}>
    <Header {...{ itemId, contact }} />
    <ScrollView style={{ flex: 1 }} contentContainerStyle={{ paddingBottom: wp(20) }}>
      <View style={{
        flexDirection: "row",
        width: wp(100),
        backgroundColor: lightenColor2(contact.color),
      }}>
        <Icons {...{
          itemId,
        }} />
        <AutoHeightImage
          source={{ uri: item.thumbUrl }}
          width={wp(50)}
          fallbackSource={itemTypes[item.type].defaultThumb}
        />
      </View>
      <Infos {...{
        contact,
        item,

      }} />
      {isRecommendedToMe && <ActionBtns {...{ item }} />}
    </ScrollView>
    <View
      style={{
        position: "absolute",
        bottom: 0,
        width: wp(100),
        justifyContent: "space-evenly",
        flexDirection: "row",
        paddingBottom: wp(2),
        paddingTop: wp(2),
        backgroundColor: "rgba(255,255,255,0.8)",
      }}
    >
      <TouchableOpacity
        onPress={() => {
          prevScreen();
        }}
      >
        <Image
          source={require("../../commons/img/back.png")}
          style={{
            width: wp(15),
            height: wp(15),
          }}
        />
      </TouchableOpacity>
    </View>
  </View>);
};

