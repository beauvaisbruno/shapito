//@flow
import React from "react";
import { Image, TouchableOpacity, View } from "react-native";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { wp } from "../../commons/css";

export const InvitationMenu = ({ back }: { back: ()=>void }) => {
  const btnStyle = {
    width: wp(15),
    height: wp(15),
  };
  return <View
    style={{
      position: "absolute",
      bottom: 0,
      width: wp(100),
      justifyContent: "space-evenly",
      flexDirection: "row",
      marginBottom: wp(2),
      marginTop: wp(2),
      backgroundColor: "rgba(255,255,255,0.8)",
    }}
  >
    <TouchableOpacity
      onPress={() => {
        if (back) {
          back();
        }
        setScreen("Friends");
      }}
    >
      <Image source={require("../../commons/img/back.png")}
             style={btnStyle}
      />
    </TouchableOpacity>
  </View>;
};
