// import axios from "react-native-axios";

const axios = require("react-native-axios");
const cheerio = require("react-native-cheerio");
// Cheerio
//request-promise
//https://cheerio.js.org/
const iconv = require("iconv-lite");

const $log = (text) => {
  console.log(text.replace(/^\s*[\r\n]/gm, "").replace(/^ +/gm, ""));
};

function toUtf8 (text) {
  // text =  iconv.decode(text, "latin1");
  // text =  iconv.decode(text, "ISO-8859-1");
  // text=  iconv.encode(text, "utf8");
  return text;
}

const get = async () => {

  const { data: jsonArr } = await axios.get(
    "https://www.babelio.com/aj_recherche.php?id_user=undefined&isMobile=false&term=star");

  const book = jsonArr[0];
  const info = {};
  info.title = book.titre;
  info.thumbUrl = book.couverture;
  info.externalLinkUrl = "https://www.babelio.com" + book.url;
  // const { data: html } = await axios.get(info.externalLinkUrl);

  const response = await axios(
    { method: "GET", url: info.externalLinkUrl, responseType: "arraybuffer" });
  const data = response.data.toString("latin1");

  // console.log("data: ",data);

  const html = data.toString("utf8");

  const $ = cheerio.load(html, {
    // xml: {
    //   normalizeWhitespace: true,
    // },
  });

  info.noteValue = $(".rating").text();
  info.thumbUrl = "https://www.babelio.com" + $(".livre_con img").attr("src");
  info.summary = $(".livre_resume").text();

  const match = /\(([^)]+)\)/.exec($(".livre_refs").text()).pop();
  if (match && !match.includes(-1)) {
    info.publicationDate = match;
  }

  console.log("info: ", info);
};
get().then(() => {
  console.log("done");
}).catch((error) => console.log("error: ", error));
