//@flow
import React, { useEffect, useState } from "react";
import { Image, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import { useAppState } from "../../commons/appState";
import { OsContact } from "../../commons/components/OsContact";
import { colors, css, margin, padding, wp } from "../../commons/css";
import { useLoader } from "../../commons/hooks";
import { addContact } from "../../commons/requests/addContact";
import { getOsContacts } from "../../commons/requests/getOsContacts";
import { isEmail } from "../../commons/util";
import type { NewContact, TheUser } from "../../model/TheUser";
import { InvitationMenu } from "./InvitationMenu";

function emailToPseudo (email) {
  email = email.trim();
  email = email.toLowerCase();
  return email.substring(0, email.indexOf("@"));
}

export const Invitation = ({ onContactSelected = () => {}, back = () => {} }
  : { onContactSelected: (TheUser)=>void, back: () => void }) => {

  // const contacts = useAppState(appState => appState.contacts);
  const userContactEmails = useAppState(appState => appState.user.contactEmails);
  const [osContacts, setOsContacts] = useState([]);
  const [filter, setFilter] = useState("");
  const { Loader, setLoading, setError } = useLoader();

  const filterIsValidEmail = isEmail(filter);

  const refresh = () => {
    setLoading(true);
    getOsContacts().then((pOsContacts) => {
      setLoading(false);
      setOsContacts(pOsContacts);
    }).catch((error) => {
      setError("Impossible d'afficher votre carnet d'adresse :(");
      console.log("request PermissionsAndroid error: ", error);
    });
  };

  useEffect(() => {
    refresh();
  }, []);

  let filteredOsContacts: Array<NewContact> = [];
  if (filter.trim() !== "") {
    const lowerCaseFilter = filter.toLowerCase();
    filteredOsContacts = osContacts.filter(({ email, pseudo }) => {
      if (email.toLowerCase().includes(lowerCaseFilter)) return true;
      if (pseudo.toLowerCase().includes(lowerCaseFilter)) return true;
      return false;
    });
  } else {
    filteredOsContacts = osContacts;
  }

  filteredOsContacts.sort((contact1, contact2) => {
    return contact1.email.localeCompare(contact2.email);
  });

  const rows = [];
  filteredOsContacts.forEach((contact) => {
    if (userContactEmails.includes(contact.email)) return;
    rows.push(<OsContact {...{
      contact,
      key: contact.email,
      onContactSelected,
    }} />);
  });

  return (<View style={{ flex: 1, alignItems: "center" }}>
    <View style={{
      flex: 0, flexDirection: "row",
      ...margin(2),
    }}>
      <TextInput
        style={{
          ...css.input,
          flex: 1,
        }}
        onChangeText={text => setFilter(text)}
        value={filter}
        placeholder={"email@example.com"}
        autoCompleteType={"email"}
      />
    </View>
    <Loader />
    {filterIsValidEmail && <View style={{
      // flex: 1,
      alignItems: "center",
      justifyContent: "space-evenly",
    }}>
      <TouchableOpacity
        style={{
          justifyContent: "center",
          alignItems: "center",
          width: wp(75),
          backgroundColor: colors.lightBg,
          ...padding(5),
          borderRadius: wp(5),
        }}
        onPress={() => {
          setLoading(true);
          addContact({
            email: filter.toLowerCase(),
            pseudo: emailToPseudo(filter),
          }).then((resultContact) => {
            setLoading(false);
            console.log("addContact resultContact: ", resultContact);
            onContactSelected(resultContact);
          }).catch(error => {
            setError(true);
          });
        }}
      >
        <Text style={{
          fontSize: wp(5),
          textAlign: "center",
        }}>
          {"Inviter " + filter.toLowerCase()}
        </Text>
        <Image source={require("../../commons/img/inviteFriend.png")}
               style={{
                 marginTop: wp(2),
                 width: wp(15),
                 height: wp(15),
               }}
        />
      </TouchableOpacity>
    </View>}

    {rows.length > 0 && <ScrollView
      style={{ flex: 1 }}
      contentContainerStyle={{
        paddingBottom: wp(20),
      }}
    >
      <View style={{ alignItems: "center" }}>
        {rows}
      </View>
    </ScrollView>}
    <InvitationMenu {...{ refresh, back }} />
  </View>);
};

