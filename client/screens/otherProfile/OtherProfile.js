//@flow
import React, { useEffect } from "react";
import { RefreshControl, View } from "react-native";
import type { GetAllFromContactRequest, GetAllFromContactResponse } from "../../../server/functions/src/getAllFromContact";
import { getAppRootState, setAppState, useAppState } from "../../commons/appState";
import { ServerErrorMsg } from "../../commons/components/ServerErrorMsg";
import { useLoader, useOnConnected, useSubScreen } from "../../commons/hooks";
import { remote } from "../../commons/requests/remote";
import type { TheUser } from "../../model/TheUser";
import { ItemsList } from "./ItemsList";
import { OtherProfileHeader } from "./OtherProfileHeader";
import { OtherProfileMenu } from "./OtherProfileMenu";

import { OtherProfileTabMenu } from "./OtherProfileTabMenu";

export type OtherProfileSubScreen =
  "watched"
  | "recommendedByHim"
  | "wantToWatch"
  | "recommendedForMe"

export const OtherProfile = ({
    initSubScreen = "recommendedByHim",
    contact,
  }: {
    initSubScreen: OtherProfileSubScreen,
    contact: TheUser
  },
) => {
  const { loading, error, setLoading, setError } = useLoader();
  const user = useAppState(appState => appState.user);

  function refresh () {
    setLoading(true);
    remote("getAllFromContact", ({ contact }: GetAllFromContactRequest))
      .then(({ contacts, items }: GetAllFromContactResponse) => {
        setLoading(false);
        setAppState((appState) => {
          console.log("setAppState inside, appState: ", getAppRootState());
          appState.contacts = { ...appState.contacts, ...contacts };
          appState.items = { ...appState.items, ...items };
        });
        console.log("setAppState, appState: ", getAppRootState());
      }).catch((error) => {
      setLoading(false);
      //connexion unavailable is handled by DisconnectedMessage
      if (error.code !== "unavailable") {
        setError(true);
      }
    });
  }

  useOnConnected(() => {
    refresh();
  });

  useEffect(() => {
    refresh();
  }, []);

  const refreshControl = <RefreshControl refreshing={loading !== false} onRefresh={() => {
    console.log(" RefreshControl refresh");
    refresh();
  }} />;

  const { SubScreen, subScreen, setSubScreen } = useSubScreen(initSubScreen, {
    recommendedByHim: () => <ItemsList {...{
      refreshControl,
      contact,
      emptyMessage: contact.pseudo + " n'a fait aucune recommandation pour l'instant.",
      itemsFilter: (item) => item.recommendedByEmail === contact.email,
    }} />,
    recommendedForMe: () => <ItemsList {...{
      refreshControl,
      contact,
      emptyMessage: contact.pseudo + " ne vous a fait aucune recommandation pour l'instant.",
      itemsFilter: (item) => item.recommendedByEmail === contact.email
        && item.recommendedToEmails.includes(user.email),
    }} />,
    wantToWatch: () => <ItemsList {...{
      refreshControl,
      contact,
      emptyMessage: contact.pseudo + " n'a indiqué s'intéresser à aucun film pour l'instant.",
      itemsFilter: (item) => item.wantToWatchEmails.includes(contact.email),
    }} />,
    watched: () => <ItemsList {...{
      refreshControl,
      contact,
      emptyMessage: contact.pseudo + " n'a vue aucun des films qui lui a été recommandé.",
      itemsFilter: (item) => item.watchedEmails.includes(contact.email),
    }} />,
  });

  return (<View style={{ flex: 1 }}>
    <OtherProfileHeader {...{ contact }} />
    <OtherProfileTabMenu {...{
      setSubScreen,
      subScreen,
    }} />
    {error && <ServerErrorMsg />}
    {SubScreen}
    <OtherProfileMenu />
  </View>);
};


