//@flow

import { scrapBook } from "../../screens/createItem/scrapBook";
import { scrapFilm } from "../../screens/createItem/scrapFilm";
import { scrapSerie } from "../../screens/createItem/scrapSerie";
import type { ItemSearch } from "../TheItem";

export type ItemTypeRow = {
  labelList: string,
  label: string,
  labelOf: string,
  thisLabel: string,
  icon: number,
  defaultThumb: number,
  scraper: ({ title: string })=>Promise<Array<ItemSearch>>,
}

export const itemTypes: { [string]: ItemTypeRow } = {
  "film": {
    labelList: "Un Film",
    label: "Film",
    labelOf: "du Film",
    thisLabel: "ce Film",
    icon: require("./filmBig.png"),
    defaultThumb: require("./filmThumb.png"),
    scraper: scrapFilm,
  },
  "serie": {
    labelList: "Une Série",
    label: "Série",
    labelOf: "de la Série",
    thisLabel: "cette Série",
    icon: require("./serieBig.png"),
    defaultThumb: require("./serieThumb.png"),
    scraper: scrapSerie,
  },
  "book": {
    labelList: "Un Livre",
    label: "Livre",
    labelOf: "du Livre",
    thisLabel: "ce Livre",
    icon: require("./bookBig.png"),
    defaultThumb: require("./bookThumb.png"),
    scraper: scrapBook,
  },
};
export type ItemType = $Keys<typeof itemTypes>;
