const scripts = require("./package-scripts");

const getCmds = (pScripts, cmds, parentKeys = "") => {
  Object.keys(pScripts).forEach((key) => {
    try {
      const row = pScripts[key];
      if (typeof row === "string") {
        return;
      }
      if (row.description) {
        const path = (parentKeys + key).replace(".default", "");
        cmds[path] = "nps " + path;
      } else {
        getCmds(row, cmds, parentKeys + key + ".");
      }
    } catch (error) {
      console.log("key: ", key, ", row:", pScripts[key], ", pScripts: ", pScripts);
      console.error(error);
    }
  });
};
const newScripts = {
  "help": "nps ",
  "update_scripts": "node scripts/updateScripts.js",
};
getCmds(scripts.scripts, newScripts);
console.log("scripts: ", newScripts);

const pjson = require("../package.json");
pjson.scripts = newScripts;

// console.log(JSON.stringify(pjson, null, 2));
const fs = require("fs");
fs.writeFileSync("./package.json", JSON.stringify(pjson, null, 2));
console.log("write " + Object.keys(newScripts).length + " cmds to package.json");

