//@flow
import React, { useEffect, useState } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import type { AddItemRequest, AddItemResponse } from "../../../server/functions/src/addItem";
import { setAppState } from "../../commons/appState";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { colors, css, margin, wp } from "../../commons/css";
import { remote } from "../../commons/requests/remote";
import type { ItemType } from "../../model/ItemTypes/itemTypes";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import { EmoticonSelector } from "./EmoticonSelector";
import { ItemSearcher } from "./ItemScraper";
import { NoteSelector } from "./NoteSelector";

const TypeSelector = ({ setType }: { setType: (ItemType)=>void }) => {
  const rows = [];
  Object.keys(itemTypes).forEach((type) => {
    const item = itemTypes[type];
    rows.push(<TouchableOpacity
      style={{ ...margin(wp(2)) }} key={type}
      onPress={() => {
        setType(type);
      }}
    >
      <Image source={item.icon} style={{
        width: wp(25),
        height: wp(25),
      }} />
      <Text style={{
        fontSize: wp(5),
        color: colors.text,
        textAlign: "center",
      }}>{item.labelList}</Text>
    </TouchableOpacity>);
  });

  return (<View style={{
    justifyContent: "center",
    flex: 1,
  }}>
    <View>
      <Text style={{
        fontSize: wp(7),
        color: colors.text,
        textAlign: "center",
      }}>Vous souhaitez ajouter</Text>
      <View style={{
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "flex-end",
        flexWrap: "wrap",
      }}>
        {rows}
      </View>
    </View>
  </View>);
};

export const CreateItem = (props: {||}) => {

  const [type, setType] = useState();
  const [description, setDescription] = useState();
  const [noteIcon, setNoteIcon] = useState();
  const [emoticons, setEmoticons] = (useState());
  const [error, setError] = useState();

  function addItem () {
    setError(false);
    if (emoticons && description && noteIcon) {
      remote("addItem", ({
        description,
        noteIcon,
        emoticons,
      }: AddItemRequest)).then(({ item, user }: AddItemResponse) => {
        setAppState((appState) => {
          appState.items = {
            ...appState.items,
            [item.itemId]: item,
          };
          appState.user = user;
        });
        setScreen("MyItem", { itemId: item.itemId });
      }).catch(error => {
        setError(true);
      });
    }
  }

  useEffect(() => {
    addItem();
  }, [emoticons]);

  if (!type) {
    return <TypeSelector {...{ setType }} />;
  }
  if (!description) {
    return <ItemSearcher {...{
      type,
      setDescription,
    }} />;
  }
  if (!noteIcon) {
    return <NoteSelector {...{
      description,
      type,
      setNoteIcon,
    }} />;
  }
  if (!emoticons) {
    return <EmoticonSelector {...{
      description,
      type,
      setEmoticons,
    }} />;
  }
  if (error) {
    return (<View style={{
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    }}>
      <Text
        style={{
          fontSize: wp(5),
          color: colors.errorMsg,
          textAlign: "center",
          width: wp(90),
        }}
      >
        Impossible d'ajouter cette recommandation. Vérifier votre connexion.
      </Text>
      <View style={{
        justifyContent: "space-evenly",
        flexDirection: "row",
        width: wp(100),
      }}>
        <TouchableOpacity
          onPress={() => {
            addItem();
          }}
        >
          <Text
            style={{
              ...css.button,
              backgroundColor: colors.buttonBg,
              width: wp(40),
            }}
          >
            Réessayer
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setScreen("MyProfile");
          }}
        >
          <Text
            style={{
              ...css.button,
              backgroundColor: colors.buttonBg,
              width: wp(40),
            }}
          >
            Abandonner
          </Text>
        </TouchableOpacity>
      </View>
    </View>);
  }

  return (<View style={{
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  }}>
    <AutoHeightImage
      source={{ uri: description.thumbUrl }}
      width={wp(50)}
      fallbackSource={itemTypes[type].defaultThumb}
    />
    <Text style={{
      fontSize: wp(5),
      color: colors.text,
      textAlign: "center",
      width: wp(75),
    }}>
      {"Ajout de " + description.title + "..."}
    </Text>
    <AutoHeightImage
      style={{ alignSelf: "center" }}
      width={wp(30)}
      source={require("../../commons/img/loading.gif")} />
  </View>);
};
