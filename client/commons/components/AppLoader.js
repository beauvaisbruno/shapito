//@flow

import React, { useEffect, useState } from "react";
import { BackHandler } from "react-native";
import { getAppRootState, getAppState, initAppState, rehydrateAppStateAsync } from "../appState";
import type { AuthProvider } from "../defaultAppState";
import { defaultAppState } from "../defaultAppState";
import { setExceptionHandlers } from "../exceptionsHandler";
import { emailSigninSilently, googleSigninSilently } from "../requests/connection";
import { loadScreens } from "../screensLoader";
import { isCurrentScreen, ScreenNavigator, setScreen } from "./ScreenNavigator";

setExceptionHandlers();

export default function AppLoader () {
  const [splashScreen, setSplashScreen] = useState(true);

  useEffect(() => {
    const backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
        if (isCurrentScreen("MyProfile")) {
          BackHandler.exitApp();
        } else {
          setScreen("MyProfile");
        }
      return true;
      },
    );
    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    initAppState(defaultAppState);
    rehydrateAppStateAsync().then(() => {
      console.log("appState: ", getAppRootState());

      const authProvider = getAppState(appState => appState.authProvider);
      if (authProvider === ("email": AuthProvider)) emailSigninSilently();
      if (authProvider === ("google": AuthProvider)) googleSigninSilently();
      if (!authProvider) setScreen("Auth");
    });
  }, []);

  useEffect(() => {
    setTimeout(() => {
      setSplashScreen(false);
    }, 2000);
  }, []);

  return <ScreenNavigator {...{
    screens: loadScreens(),
    splashScreen,
  }} />;
}
