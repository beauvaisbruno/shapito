//@flow

import React, { useEffect, useState } from "react";
import { Text } from "react-native";
import { initAppState } from "../appState";
import { ScreenNavigator } from "../components/ScreenNavigator";
import { defaultAppState } from "../defaultAppState";
import type { RemoteMocks } from "../requests/remote";
import { setStoryMockRemote } from "../requests/remote";
import type { ScreenName } from "../screensLoader";
import { loadScreens } from "../screensLoader";

export const MockScreen = ({
  appState = {},
  screen = "SplashScreen",
  screenProps = {},
  remote = {},
}: {
  appState?: any,
  screen?: ScreenName,
  screenProps?: any,
  remote?: RemoteMocks,
}) => {
  const [ready, setReady] = useState(false);
  useEffect(() => {
    initAppState({ ...defaultAppState, ...appState });
    setStoryMockRemote(remote);
    setReady(true);
  }, []);
  if (!ready) return <Text>story loading...</Text>;
  return (<ScreenNavigator {...{
    screens: loadScreens(),
    initScreen: screen,
    initScreenProp: screenProps,
  }} />);
};
