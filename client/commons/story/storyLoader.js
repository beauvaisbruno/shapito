// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories () {
  require("../../screens/createItem/createItem.stories");
  require("../../screens/friends/friends.stories");
  require("../../screens/invitation/invitation.stories");
  require("../../screens/myItem/MyItem.stories");
  require("../../screens/myProfile/MyProfile.stories");
  require("../../screens/otherItem/OtherItem.stories");
  require("../../screens/otherProfile/OtherProfile.stories");
  require("../../screens/profileEdit/profileEdit.stories");
}

const stories = [
  "../../screens/createItem/createItem.stories",
  "../../screens/friends/friends.stories",
  "../../screens/invitation/invitation.stories",
  "../../screens/myItem/MyItem.stories",
  "../../screens/myProfile/MyProfile.stories",
  "../../screens/otherItem/OtherItem.stories",
  "../../screens/otherProfile/OtherProfile.stories",
  "../../screens/profileEdit/profileEdit.stories",
];

module.exports = {
  loadStories,
  stories,
};
