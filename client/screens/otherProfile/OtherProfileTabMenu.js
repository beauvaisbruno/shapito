//@flow
import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { getAppState } from "../../commons/appState";
import { margin, wp } from "../../commons/css";
import { avatars } from "../../model/avatars/avatar";
import type { OtherProfileSubScreen } from "./OtherProfile";

export const recommendedForMe = "recommendedForMe";
export const recommendedByHim = "recommendedByHim";
export const wantToWatch = "wantToWatch";
export const watched = "watched";

const iconStyle = {
  height: wp(10),
  width: wp(10), ...margin(1),
};
const btnStyle = (highlight) => ({
  flexDirection: "row",
  alignItems: "center",
  width: wp(45.9),
  borderWidth: wp(0.5),
  borderRadius: wp(10),
  ...margin(2),
  marginBottom: 0,
  borderColor: highlight ? "#000" : "#dbdbdb",
});
const textStyle = {
  fontSize: wp(3.5),
  flex: 1,
  flexWrap: "wrap",
};

export const OtherProfileTabMenu = ({
  setSubScreen,
  subScreen,
}: {
  setSubScreen: (OtherProfileSubScreen)=>void,
  subScreen: OtherProfileSubScreen,
}) => {
  const avatar = getAppState(appState => appState.user.avatar);
  return (<View style={{
    flexDirection: "row",
    flexWrap: "wrap",
    width: wp(100),
    marginBottom: wp(2),
  }}>
    <TouchableOpacity
      style={btnStyle(subScreen === recommendedByHim)}
      onPress={() => setSubScreen(recommendedByHim)}
    >
      <Image style={iconStyle} source={require("../../commons/img/recommandation.png")} />
      <Text style={textStyle}>{"Toutes ses recommandations"}</Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={btnStyle(subScreen === recommendedForMe)}
      onPress={() => setSubScreen(recommendedForMe)}
    >
      <Image
        source={avatar ? avatars[avatar] : avatars.defaultAvatar}
        style={iconStyle}
      />
      <Text style={textStyle}>{"Recommandations pour vous"}</Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={btnStyle(subScreen === wantToWatch)}
      onPress={() => setSubScreen(wantToWatch)}
    >
      <Image style={iconStyle} source={require("../../commons/img/interest.png")} />
      <Text style={textStyle}>{"Ça l'intéresse"}</Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={btnStyle(subScreen === watched)}
      onPress={() => setSubScreen(watched)}
    >
      <Image style={iconStyle} source={require("../../commons/img/eye.png")} />
      <Text style={textStyle}>{"Il a déjà vu"}</Text>
    </TouchableOpacity>
  </View>);
};

