//@flow

import functions from "@react-native-firebase/functions";
import clonedeep from "lodash.clonedeep";
import { getAppState } from "../appState";
import { DEV } from "../DEV";

export type RemoteMocks = { [googleFunction: string]: {} | ({})=>{} };
let storyRemoteMockData: RemoteMocks = {};

export function setStoryMockRemote (pStoryRemoteMockData: RemoteMocks) {
  storyRemoteMockData = pStoryRemoteMockData;
}

/**
 * request.email is used to authenticate the user.
 * @param googleFunction
 * @param request
 * @returns {Promise<Object>}
 */
export async function remote (googleFunction: string, request: {} = {}): Promise<any> {
  console.log(">" + googleFunction);
  // console.log(">" + googleFunction, request);
  if (DEV.story) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const remoteDataMock = storyRemoteMockData[googleFunction];
        console.log("<" + googleFunction);
        if (!remoteDataMock) {
          reject({ error: "no remote data mock for " + googleFunction });
        }
        if (typeof remoteDataMock === "function") {
          // console.log("<" + googleFunction, remoteDataMock(request));
          resolve(clonedeep(remoteDataMock(request)));
        } else {
          // console.log("<" + googleFunction, remoteDataMock);
          resolve(clonedeep(remoteDataMock));
        }
      }, DEV.networkDelay || 1);
    });
  }

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      functions().httpsCallable(googleFunction)({
        ...request,
        email: getAppState(appState => appState.user.email),
      })
        .then(res => {
          console.log("<" + googleFunction);
          // console.log("<" + googleFunction + ": ", res.data);
          resolve(res.data);
        })
        .catch(error => {
          if (error.code === "unavailable") {
            console.log("<" + googleFunction + " unavailable");
          } else {
            console.log("<" + googleFunction + " error", error);
          }
          reject(error);
        });
    }, DEV.networkDelay || 1);
  });
}
