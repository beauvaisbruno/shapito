//@flow
import type { Node } from "react";
import React from "react";
import { Image, Text, View } from "react-native";
import type { ViewStyle } from "react-native/Libraries/StyleSheet/StyleSheet";
import { wp } from "../../commons/css";

export type ScrapTypeRow = {
  NoteComp: ({
    value: string,
    pc: number,
    style: ViewStyle,
  })=>Node,
  PublicationComp: ({
    value: string,
    pc: number,
    style: ViewStyle,
  })=>Node,
}

export const scrapTypes: { [string]: ScrapTypeRow } = {
  allocine: {
    NoteComp: ({
      value,
      pc = 5,
      style = {},
    }) => {
      return (<View style={style}>
        <Text style={{ fontSize: wp(pc) }}>
          {"Note Allocine : " + value}
          <Image style={{
            height: wp(pc),
            width: wp(pc),
          }} source={require("./allocine.png")} />
        </Text>
      </View>);
    },
    PublicationComp: ({
      value,
      pc = 5,
      style = {},
    }) => {
      return (<View style={style}>
        <Text style={{ fontSize: wp(pc) }}>
          {"Sortie : " + value}
        </Text>
      </View>);
    },
  },
  babelio: {
    NoteComp: ({
      value,
      pc = 5,
      style = {},
    }) => {
      return (<View style={style}>
        <Text style={{ fontSize: wp(pc) }}>
          {"Note Babelio : " + value}
          <Image style={{
            height: wp(pc),
            width: wp(pc),
          }} source={require("./babelio.png")} />
        </Text>
      </View>);
    },
    PublicationComp: ({
      value,
      pc = 5,
      style = {},
    }) => {
      return (<View style={style}>
        <Text style={{ fontSize: wp(pc) }}>
          {"Publié le " + value}
        </Text>
      </View>);
    },
  },
};

export type ScrapType = $Keys<typeof scrapTypes>;



