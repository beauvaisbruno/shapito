//@flow
import React from "react";
import { ScrollView, View } from "react-native";
import { useAppState } from "../../commons/appState";
import { LinkToOsContacts } from "../../commons/components/LinkToOsContacts";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { wp } from "../../commons/css";
import type { TheUser } from "../../model/TheUser";
import { FriendContact } from "./FriendContact";
import { FriendMenu } from "./FriendMenu";

export const Friends = ({
  onContactSelected = (contact) => {
    setScreen("OtherProfile", { contact });
  },
}: {
  onContactSelected?: (TheUser)=>void,
}) => {
  const allContacts = useAppState(appState => appState.contacts);
  const userContactEmails = useAppState(appState => appState.user.contactEmails);

  const rows = [];
  userContactEmails.forEach((email) => {
    try {
      rows.push(<FriendContact {...{
        contact: allContacts[email],
        key: email,
        onContactSelected,
      }} />);
    } catch (error) {
      console.error("error: ", error, ", email: ", email);
    }
  });

  return (<View style={{ flex: 1 }}>
    <ScrollView
      style={{ flex: 1 }}
      contentContainerStyle={{
        paddingBottom: wp(20),
      }}
    >
      <View style={{ alignItems: "center" }}>
        {rows}
        <LinkToOsContacts {...{
          empty: rows.length === 0,
          onPress: () => {
            setScreen("Invitation");
          },
        }} />
      </View>
    </ScrollView>
    <FriendMenu {...{
      onContactSelected,
    }} />
  </View>);
};

