//@flow
import React from "react";
import { Image, TouchableOpacity, View } from "react-native";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { wp } from "../../commons/css";
import { logout } from "../../commons/requests/connection";

export const ProfileMenu = (props: {||}) => {
  const btnStyle = {
    width: wp(15),
    height: wp(15),
  };
  return <View
    style={{
      position: "absolute",
      bottom: 0,
      width: wp(100),
      justifyContent: "space-evenly",
      flexDirection: "row",
      paddingBottom: wp(2),
      paddingTop: wp(2),
      backgroundColor: "rgba(255,255,255,0.8)",
    }}
  >
    <TouchableOpacity
      onPress={() => {
        logout();
      }}
    >
      <Image source={require("../../commons/img/logout.png")}
             style={btnStyle}
      />
    </TouchableOpacity>
    <TouchableOpacity
      onPress={() => {
        setScreen("Friends");
      }}
    >
      <Image source={require("../../commons/img/contact.png")}
             style={btnStyle}
      />
    </TouchableOpacity>
    <TouchableOpacity
      onPress={() => {
        setScreen("CreateItem");
      }}
    >
      <Image source={require("../../commons/img/createBtn.png")}
             style={btnStyle}
      />
    </TouchableOpacity>
  </View>;
};
