//@flow
import type { Node } from "react";
import { Auth } from "../screens/auth/Auth";
import { CreateItem } from "../screens/createItem/CreateItem";
import { Friends } from "../screens/friends/Friends";
import { Invitation } from "../screens/invitation/Invitation";
import { MyItem } from "../screens/myItem/MyItem";
import { MyProfile } from "../screens/myProfile/MyProfile";
import { OtherItem } from "../screens/otherItem/OtherItem";
import { OtherProfile } from "../screens/otherProfile/OtherProfile";
import { ProfileEdit } from "../screens/profileEdit/ProfileEdit";
import { SplashScreen } from "../screens/splashScreen/SplashScreen";

export const loadScreens: () => { [ScreenName]: (any)=>Node } = () => {
  return {
    SplashScreen,
    Auth,
    MyProfile,
    ProfileEdit,
    OtherProfile,
    Friends,
    Invitation,
    CreateItem,
    MyItem,
    OtherItem,
  };
};

export type ScreenName = "SplashScreen"
  | "Auth"
  | "MyProfile"
  | "ProfileEdit"
  | "OtherProfile"
  | "Friends"
  | "Invitation"
  | "CreateItem"
  | "MyItem"
  | "OtherItem"
  ;

