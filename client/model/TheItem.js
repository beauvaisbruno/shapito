//@flow

import type { Emoticon } from "./emoticons/emoticons";
import type { ItemType } from "./ItemTypes/itemTypes";
import type { NoteIcon } from "./noteIcons/noteIcons";
import type { ScrapType } from "./scrapTypes/scrapTypes";

export type ItemDescription = {|
  title: string,
  type: ItemType,
  thumbUrl?: string,
  scrapType?: ScrapType,
  externalLinkUrl?: string,
  noteValue?: string,
  summary?: string,
  publicationDate?: string,
|}

export type ItemSearch = {|
  description: ItemDescription,
  cheerioEl?: any,
  scrapOne?: () => Promise<ItemDescription>
|}

export type TheItemArrayProps = {|
  recommendedToEmails: Array<string>,
  emoticons: Array<Emoticon>,
  seenEmails: Array<string>,  //the recommendation has been seen
  wantToWatchEmails: Array<string>,
  watchedEmails: Array<string>, //the item has been watched
|}

export type TheItem = {|
  itemId: string,
  ...ItemDescription,
  recommendedByEmail: string,
  noteIcon: NoteIcon,
  creationDate: number,
  ...TheItemArrayProps
|}


