//@flow

import { Buffer } from "buffer";
import TimeAgo from "javascript-time-ago";
import en from "javascript-time-ago/locale/en";
import fr from "javascript-time-ago/locale/fr";
import React, { useState } from "react";
import DEV from "./DEV";

try {
  // var iconv = require('iconv-lite');
  //TODO TypeError: undefined is not an object (evaluating 'Buffer.prototype')
  require("safer-buffer").Buffer;
} catch (error) {
  console.log("iconv-lite error: ", error);
}

let m_w = 123456789;
let m_z = 987654321;

export function pseudoRndReset () {
  m_w = 123456789;
  m_z = 987654321;
}

/**
 * Create a pseudo aleatoire number
 * @returns {number}
 */

//https://en.wikipedia.org/wiki/Multiply-with-carry_pseudorandom_number_generator
export function pseudoRnd (): number {
  m_z = (36969 * (m_z & 65535) + (m_z >> 16)) & 0xffffffff;
  m_w = (18000 * (m_w & 65535) + (m_w >> 16)) & 0xffffffff;
  let result = ((m_z << 16) + (m_w & 65535)) >>> 0;
  result /= 4294967296;
  return result;
}

/**
 * Object.values return mixed type.
 * Use this function instead.
 * @param objs
 * @returns {[]}
 */
export const values = <Row> (obj: { [string]: Row }): Array<Row> => {
  return Object.keys(obj).map(key => obj[key]);
};

//https://www.w3resource.com/javascript/form/email-validation.php
export const isEmail = (email: string): boolean => {
  return /^[^@]+@\w+(\.\w+)+\w$/.test(email);
};

/**
 * Fetch api and axio lib are not supporting iso-8859-1
 * @param url
 * @returns {Promise<String>}
 */
export const fetchIso8859_1 = (url: string): Promise<string> => {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();

    request.onload = () => {
      if (request.status === 200) {
        try {
          const { decode } = require("iconv-lite");
          resolve(decode(Buffer.from(request.response), "iso-8859-1"));
        } catch (error) {
          console.log("iconv-lite error: ", error);
          reject(new Error(error));
        }
      } else {
        reject(new Error(request.statusText));
      }
    };
    request.onerror = () => reject(new Error(request.statusText));
    request.responseType = "arraybuffer";
    request.open("GET", url);
    request.setRequestHeader("Content-type", "application/json; charset=iso-8859-1");
    request.send();
  });
};

export const mergeToFieldArr = (obj: {}, key: string, value: any) => {
  let field = obj[key];
  if (!field) field = [];
  if (!field.includes(value)) field.push(value);
  obj[key] = field;
  return obj;
};

let timeAgo;

export function ago (datetime: number): string {
  if (!timeAgo) {
    TimeAgo.setDefaultLocale(en);
    TimeAgo.addLocale(fr);
    timeAgo = new TimeAgo("fr");
  }
  return timeAgo.format(new Date(datetime));
}

export function darkenColor (color: string, percent: number = 0.7): string {
  // if (!color) {
  //   console.warn("darkenColor, color: ", color);
  //   return "black";
  // }
  let R = parseInt(color.substring(1, 3), 16);
  let G = parseInt(color.substring(3, 5), 16);
  let B = parseInt(color.substring(5, 7), 16);
  const darken = (oldValue): string => {
    let hexValue = parseInt(oldValue * percent).toString(16);
    if (hexValue.length === 1) hexValue = "0" + hexValue;
    return hexValue;
  };
  return "#" + darken(R) + darken(G) + darken(B);
}

export function lightenColor2 (color: string): string {
  return lightenColor(color, 0.7);
}

export function lightenColor (color: string, percent: number = 0.8): string {
  // if (!color) {
  //   console.warn("lightenColor, color: ", color);
  //   return "whitesmoke";
  // }
  let R = parseInt(color.substring(1, 3), 16);
  let G = parseInt(color.substring(3, 5), 16);
  let B = parseInt(color.substring(5, 7), 16);
  const lighten = (oldValue) => {
    let integer = oldValue + parseInt((255 - oldValue) * percent);
    let hexValue = Math.min(integer, 255).toString(16);
    if (hexValue.length === 1) hexValue = "0" + hexValue;
    return hexValue;
  };
  return "#" + lighten(R) + lighten(G) + lighten(B);
}

export function useUpdate () {
  const update = useState()[1];
  return () => {
    update(Date.now());
  };
}

export function getRandomColor () {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    let number = Math.random();
    if (DEV.story) {
      number = pseudoRnd();
    }
    color += letters[Math.floor(number * 16)];
  }
  return color;
}
