//@flow

import auth from "@react-native-firebase/auth";
import React, { useEffect, useState } from "react";
import { ScrollView, Text, TextInput, TouchableOpacity } from "react-native";
import { setAppState } from "../../commons/appState";
import { colors, css, wp } from "../../commons/css";
import consts from "../../commons/DEV";
import DEV from "../../commons/DEV";
import { useKeyboard, useLoader } from "../../commons/hooks";
import { onConnectionWithEmailSuccess } from "../../commons/requests/connection";

function errorCodeToMsg (code: string): string {
  //https://firebase.google.com/docs/reference/js/firebase.User
  if (code === "auth/invalid-credential") return "Email incorrect";
  if (code === "auth/wrong-password") return "Mot de passe incorrect";
  if (code === "auth/email-already-in-use") return "Email déjà utilisé";
  if (code === "auth/weak-password,") return "mot de passe trop simple";
  return "Connexion impossible";
}

const ResetEmail = ({ email }: { email: string }) => {
  const { Loader, setLoading, setError } = useLoader();
  const [success, setSuccess] = useState(false);
  const msgStyle = {
    marginTop: wp(5),
    fontSize: wp(5),
    textAlign: "center",
    width: wp(90),
  };
  return (<>
    <Loader />
    {success && (<Text style={{
      ...msgStyle,
      color: colors.successMsg,
    }}>
      {"Un email vous a été envoyé avec des instructions"}
    </Text>)}
    <TouchableOpacity
      onPress={() => {
        setLoading("Envoi d'un email en cours...");
        auth()
          .sendPasswordResetEmail(email)
          .then(function () {
            console.log("sendPasswordResetEmail success");
            setLoading(false);
            setSuccess(true);
          })
          .catch(resetError => {
            console.error("sendPasswordResetEmail error: ", resetError);
            setError("Impossible d'envoyer un email.");
          });
      }}
    >
      <Text
        style={{
          marginTop: wp(5),
          fontSize: wp(5),
          color: colors.link,
          textDecorationLine: "underline",
        }}
      >
        Mot de passe oublié
      </Text>
    </TouchableOpacity>
  </>);
};

function connect ({ setLoading, setError, email, password }) {

  setLoading(true);
  const onSigninWithEmailSuccess = () => {
    onConnectionWithEmailSuccess(email, password);
  };

  console.log("connexion email: ", email, ", password: ", password);

  if (consts.useLocalFirebaseServer) {
    auth().useEmulator("http://" + consts.firebaseDevServerIp + ":9099");
  }
  //https://firebase.google.com/docs/auth/web/password-auth
  auth()
    .signInWithEmailAndPassword(email, password)
    .then(user => {
      console.log("Auth signInWithEmailAndPassword success, email: ", email, ", password: ",
        password);
      onSigninWithEmailSuccess();
    })
    .catch(signInError => {
      console.log("Auth signInWithEmailAndPassword errorCode: ", signInError.errorCode,
        ", email: ", email, ", password: ", password);

      if (signInError.code === "auth/user-not-found") { //https://firebase.google.com/docs/reference/js/firebase.User
        console.log("createUserWithEmailAndPassword...");
        auth().createUserWithEmailAndPassword(email, password)
          .then(userCredential => {
            console.log("createUserWithEmailAndPassword, user: ", userCredential);
            setAppState(appState => appState.user.uid = userCredential.user.uid);
            onSigninWithEmailSuccess();
          })
          .catch(createUserError => {
            console.error("createUserWithEmailAndPassword, error: ", createUserError);
            setError(errorCodeToMsg(signInError.code));
          });
      } else {
        setError(errorCodeToMsg(signInError.code));
      }
    });
}

export const EmailConnection = ({ scrollRef }:
  { scrollRef: { current: React$ElementRef<typeof ScrollView> } }) => {

  const [showEmailForm, setShowEmailForm] = useState(false);
  const [email, setEmail] = useState(DEV.fillForm ? "shapitotest.1@gmail.com" : "");
  // const [email, setEmail] = useState(DEV.fillForm ? "shapitotest.2@gmail.com" : "");
  const [password, setPassword] = useState(DEV.fillForm ? "shapito" : "");
  const { Loader, loading, error, setLoading, setError } = useLoader();
  const { isKeyboard } = useKeyboard();

  useEffect(() => {
    scrollRef.current.scrollToEnd({ animated: true });
  }, [isKeyboard]);

  return (<>
    {showEmailForm && (<>
      <TextInput
        testID="connectionEmailInput"
        style={{
          ...css.input,
          width: wp(90),
          marginTop: wp(5),
        }}
        onChangeText={text => setEmail(text)}
        value={email}
        placeholder={"email@example.com"}
        autoCompleteType={"email"}
      />
      <TextInput
        testID="connectionPasswordInput"
        style={{
          ...css.input,
          width: wp(90),
          marginTop: wp(5),
        }}
        onChangeText={text => setPassword(text)}
        autoCompleteType={"password"}
        placeholder={"Mot de passe"}
        value={password}
      />
      {error && <ResetEmail {...{ email }} />}
      {error && <Loader />}
    </>)}
    <TouchableOpacity
      testID="connectWithEmailBtn"
      disabled={loading !== false}
      onPress={() => {
        if (!showEmailForm) {
          setShowEmailForm(true);
          return;
        }
        connect(
          {
            setLoading,
            setError,
            email,
            password,
          });
      }}
    >
      {loading && (
        <Text
          style={{
            backgroundColor: "#73ec88",
            ...css.button,
            width: wp(90),
          }}
        >
          Connexion...
        </Text>)}
      {!loading && (<Text
        style={{
          ...css.button,
          backgroundColor: "#73ec88",
          width: wp(90),
        }}
      >
        Connexion avec votre email
      </Text>)
      }
    </TouchableOpacity>
  </>);
};
