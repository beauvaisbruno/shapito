//@flow

import { GoogleSignin } from "@react-native-community/google-signin";
import auth from "@react-native-firebase/auth";
import type { Avatar } from "../../model/avatars/avatar";
import { getAppState, initAppState, persistAppState, setAppState } from "../appState";
import { setScreen } from "../components/ScreenNavigator";
import type { AuthProvider } from "../defaultAppState";
import { defaultAppState } from "../defaultAppState";
import { getAll } from "./getAll";

export function selectLaunchScreen () {
  const user = getAppState(appState => appState.user);
  console.log("selectLaunchScreen, user: ", user);
  if (user.avatar === ("defaultAvatar": Avatar)) {
    setScreen("ProfileEdit");
  } else {
    setScreen("MyProfile");
  }
}

export function requestAllAndSelectLaunchScreen () {
  const user = getAppState(appState => appState.user);
  if (user.avatar === ("defaultAvatar": Avatar)) {
    getAll(true)
      .then(() => {
        selectLaunchScreen();
      }).catch((error) => {
      console.log("requestAllAndSelectLaunchScreen error: ", error);
      selectLaunchScreen();
    });
  } else {
    setScreen("MyProfile");
  }
}

//client_type 3 (=web) inside google-service.json
//https://github.com/react-native-google-signin/google-signin#notes
const WEB_CLIENT_ID = "972760892037-ns4e7u2477a690riplgo973r17b0m981.apps.googleusercontent.com";

export async function googleSigninSilently () {
  try {
    GoogleSignin.configure({
      webClientId: WEB_CLIENT_ID,
    });
    const userInfo = await GoogleSignin.signInSilently();
    // console.log("userInfo: ", userInfo);
    const googleCredential = auth.GoogleAuthProvider.credential(userInfo.idToken);
    // console.log("googleCredential: ", googleCredential);
    const userCredential = await auth().signInWithCredential(googleCredential);
    // console.log("userCredential: ", userCredential);
    if (!userInfo) {
      // console.log("googleSigninSilently error, userInfo: ", userInfo);
      setScreen("Auth");
      return;
    }
    console.log("googleSigninSilently, user: ", userInfo);
    setAppState(appState => {
      appState.user.email = userInfo.user.email;
      appState.authProvider = "google";
    });
    // console.log("googleSigninSilently done");
    requestAllAndSelectLaunchScreen();
  } catch (error) {
    console.log("googleSigninSilently, error: ", error);
    setScreen("Auth");
  }
}

export function emailSigninSilently () {
  const email = getAppState(appState => appState.user.email);
  const password = getAppState(appState => appState.password);
  if (!email || !password) {
    logout();
  } else {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(user => {
        console.log("emailSigninSilently, email: ", email, ", password: ", password);
        requestAllAndSelectLaunchScreen();
      })
      .catch(error => {
        console.log("emailSigninSilently error.code: ", error.code);
        if (error.code === "auth/unknown") {
          setScreen("MyProfile");
        } else {
          setScreen("Auth");
        }
      });
  }
}

export async function googleSignin () {
  GoogleSignin.configure({
    webClientId: WEB_CLIENT_ID,
  });
  await GoogleSignin.hasPlayServices();
  const userInfo = await GoogleSignin.signIn();
  // console.log("userInfo: ", userInfo);
  const googleCredential = auth.GoogleAuthProvider.credential(userInfo.idToken);
  // console.log("googleCredential: ", googleCredential);
  const userCredential = await auth().signInWithCredential(googleCredential);
  // console.log("userCredential: ", userCredential);
  setAppState(appState => {
    appState.user.uid = userCredential.user.uid;
    appState.user.email = userInfo.user.email;
    appState.authProvider = "google";
  });

  requestAllAndSelectLaunchScreen();
}

export function onConnectionWithEmailSuccess (email: string, password: string) {
  setAppState(appState => {
    appState.user.email = email;
    appState.password = password;
    appState.authProvider = "email";
  });
  requestAllAndSelectLaunchScreen();
}

const resetAppStateAndChangeScreen = (callback) => {
  initAppState(defaultAppState);
  persistAppState(defaultAppState);
  setScreen("Auth");
  if (callback) callback();
};

export function logout (callback?: ()=>void) {
  console.log("logout");
  try {
    let authProvider = getAppState(appState => appState.authProvider);
    if (authProvider === ("email": AuthProvider)) {

      auth()
        .signOut()
        .then(() => {
          resetAppStateAndChangeScreen(callback);
        });
    }
    if (authProvider === ("google": AuthProvider)) {
      GoogleSignin.revokeAccess().then(() => {
        console.log("GoogleSignin revokeAccess done");
        GoogleSignin.signOut().then(() => {
          console.log("GoogleSignin signOut done");
          resetAppStateAndChangeScreen(callback);
        });
      });
    }
  } catch (error) {
    console.log("logout error: ", error);
    resetAppStateAndChangeScreen(callback);
  }
}
