//@flow

import auth from "@react-native-firebase/auth";
import functions from "@react-native-firebase/functions";
import { LogBox } from "react-native";

LogBox.ignoreLogs(["Require cycle: node_modules"]);
LogBox.ignoreLogs(["Can't perform a React state update"]);

let consts = {
  // story: true,
  // firebaseDevServerIp: "10.0.2.2", // emulator
  firebaseDevServerIp: "192.168.43.56",
  useLocalFirebaseServer: true,
  // fillForm: true,
  mockOsContacts: true,
  networkDelay: 1,
};
if (!__DEV__ || process.env.JEST_WORKER_ID) consts = {};
if (consts.useLocalFirebaseServer) {
  console.log("useAuthEmulator: ", "http://" + consts.firebaseDevServerIp + ":9099");
  auth().useEmulator("http://" + consts.firebaseDevServerIp + ":9099");
}
if (consts.useLocalFirebaseServer) {
  console.log("useFunctionsEmulator: ", "http://" + consts.firebaseDevServerIp + ":5001");
  functions().useFunctionsEmulator("http://" + consts.firebaseDevServerIp + ":5001");
}

export const DEV = (consts: any);
export default DEV;
