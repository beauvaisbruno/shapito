//@flow
import React, { useEffect } from "react";
import { RefreshControl, View } from "react-native";
import { useAppState } from "../../commons/appState";
import { ServerErrorMsg } from "../../commons/components/ServerErrorMsg";
import { useLoader, useOnConnected, useSubScreen } from "../../commons/hooks";
import { getAll } from "../../commons/requests/getAll";
import { ItemsList } from "./ItemsList";
import { MyProfileHeader } from "./MyProfileHeader";
import { ProfileMenu } from "./ProfileMenu";
import { ProfileTabMenu } from "./ProfileTabMenu";
import { RecommendedByMe } from "./RecommendedByMe";

export type MyProfileSubscreen = "recommendedByMe" | "watched" | "wantToWatch" | "recommendedForMe";

export const MyProfile = ({ initSubScreen = "recommendedForMe" }: { initSubScreen: MyProfileSubscreen }) => {

  const { loading, error, setLoading, setError } = useLoader();
  const user = useAppState(appState => appState.user);

  function refresh (force = false) {
    setLoading(true);
    getAll(force).then(() => {
      setLoading(false);
    }).catch((error) => {
      setLoading(false);
      //connexion unavailable is handled by DisconnectedMessage
      if (error.code !== "unavailable") {
        setError(true);
        console.log("getAll error", error);
      }
    });
  }

  useOnConnected(() => {
    refresh();
  });

  useEffect(() => {
    refresh();
  }, []);

  const refreshControl = <RefreshControl
    refreshing={loading !== false}
    onRefresh={() => {
      console.log("RefreshControl refresh");
      refresh(true);
    }} />;

  const { SubScreen, subScreen, setSubScreen } = useSubScreen(initSubScreen, {
    recommendedForMe: () => <ItemsList {...{
      refreshControl,
      contact: user,
      emptyMessage: "Vous n'avez aucune recommandation pour l'instant.",
      itemsFilter: (item) => user.recommendedForMeItemIds.includes(item.itemId)
        && !item.watchedEmails.includes(user.email),
    }} />,
    recommendedByMe: () => <RecommendedByMe {...{ refreshControl }} />,
    wantToWatch: () => <ItemsList {...{
      refreshControl,
      contact: user,
      emptyMessage: "Vous n'avez indiqué vous intéresser à aucune recommandation pour l'instant.",
      itemsFilter: (item) => user.recommendedForMeItemIds.includes(item.itemId)
        && !item.watchedEmails.includes(user.email)
        && item.wantToWatchEmails.includes(user.email),
    }} />,
    watched: () => <ItemsList {...{
      refreshControl,
      contact: user,
      emptyMessage: "Vous n'avez indiqué ne connaitre aucune des recommandations pour l'instant.",
      itemsFilter: (item) => user.recommendedForMeItemIds.includes(item.itemId)
        && item.watchedEmails.includes(user.email),
    }} />,
  });

  return (<View style={{ flex: 1 }}>
    <MyProfileHeader />
    <ProfileTabMenu {...{
      setSubScreen,
      subScreen,
    }} />
    {error && <ServerErrorMsg />}
    {SubScreen}
    <ProfileMenu />
  </View>);
};
