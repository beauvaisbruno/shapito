//@flow

import React from "react";
import { Text } from "react-native";
import { colors, wp } from "../css";

export const ServerErrorMsg = (props: {||}) => {
  return <Text
    style={{
      textAlign: "center",
      width: wp(100),
      color: "white",
      backgroundColor: colors.errorMsg,
    }}>
    {"Connexion au server impossible :("}
  </Text>;
};
