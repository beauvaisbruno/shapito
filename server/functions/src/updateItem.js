//@flow
import type { TheItem } from "../../../client/model/TheItem";
import type { TheUser } from "../../../client/model/TheUser";

const {
  throwError,

} = require("./commons/util");
const { checkUser } = require("./commons/checkUser");
const functions = require("firebase-functions");
const { firestore } = require("./commons/firebase");

export type UpdateItemRequest = {| item: TheItem |};
export type UpdateItemResponse = {| item: TheItem |};

async function updateItemFn ({
  item,
  user,
}: {
  user: TheUser,
  ...UpdateItemRequest
}) {
  // console.log("updateItemFn...");

  if (item.recommendedByEmail !== user.email) {
    throwError("updateItemFn, Wrong recommendedByEmail", {
      user,
      item,
    });
  }
  let result;
  try {
    result = await firestore.collection("TheItems").doc(item.itemId).set(item);
    return ({ item }: UpdateItemResponse);
  } catch (error) {
    throwError("updateItemFn error", {
      // result,
      error,
      user,
      item,
    });
  }
  throw Error("unreachable");
}

exports.updateItem = functions.https.onCall(async (request, context) => {
  return await updateItemFn({
    ...request,
    user: await checkUser({ request, context }),
  });
});
