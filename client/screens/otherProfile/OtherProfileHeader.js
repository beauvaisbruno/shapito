//@flow
import React from "react";
import { Image, Text, View } from "react-native";
import { colors, margin, padding, wp } from "../../commons/css";
import { lightenColor } from "../../commons/util";
import { avatars } from "../../model/avatars/avatar";
import type { TheUser } from "../../model/TheUser";

export const OtherProfileHeader = ({ contact }:
  { contact: TheUser }) => {
  const {
    avatar,
    color,
    pseudo,
    email,
  } = contact;

  return (<View
    style={{
      minHeight: wp(25),
      width: wp(100),
      flexDirection: "row",
      alignSelf: "center",
      backgroundColor: color ? lightenColor(color) : colors.userNoColor,
    }}
  >
    <Image
      source={avatar ? avatars[avatar] : avatars.defaultAvatar}
      style={{
        width: wp(21),
        height: wp(21),
        ...padding(1),
        ...margin(2),
      }}
    />
    <View
      style={{
        justifyContent: "space-evenly",
        alignSelf: "center",
        width: wp(50),
        flex: 1,
      }}
    >
      <Text
        style={{
          fontSize: wp(10),
          color: colors.text,
          textAlign: "center",
        }}
      >
        {pseudo}
      </Text>
      <Text
        style={{
          fontSize: wp(4),
          color: colors.text,
          textAlign: "center",
        }}
      >
        {email}
      </Text>
    </View>
  </View>);
};

