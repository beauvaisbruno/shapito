# Shapito

High velocity & Cross-platform setup for [devops](https://www.atlassian.com/devops) teams.

![Build Status](https://img.shields.io/badge/codemagic-passing-brightgreen.svg)
![Codacy Badge](https://img.shields.io/badge/code%20quality-A-brightgreen.svg)
![licence MIT](https://img.shields.io/badge/license-MIT-green.svg)

![react native](./asset/img/header.png)

<a target="_blank" href='https://play.google.com/store/apps/details?id=tech.equipage.shapito'>
<img width="200" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>

## Technologies

- [React Native](https://facebook.github.io/react-native/) to build an app for both Android and iOS
- [ECMAScript 2018](https://en.wikipedia.org/wiki/ECMAScript#9th_Edition_-_ECMAScript_2018),
  [Node.js](https://nodejs.org/en/) and [Flow types](https://flow.org/) to code fast and safely
- No boilerplate code with only functional components, [React Hooks](https://reactjs.org/docs/hooks-intro.html)
  and simple [global state management](client/commons/appState.js) (no [Redux](https://redux.js.org/)).
- [Storybook](https://storybook.js.org/) to build bulletproof UI (isolation & specification)
- Component tests with [Storyshot](https://storybook.js.org/docs/react/workflows/snapshot-testing)
  ([Jest](https://jestjs.io/) & [Enzyme](https://enzymejs.github.io/enzyme/))
- Serverless Api with [Google Function](https://firebase.google.com/docs/functions)
- Easy and secure authentification flow with [Firebase Authentication](https://firebase.google.com/docs/auth)
- [Firestore](https://firebase.google.com/docs/firestore) scalable noSQL database with realtime update and offline
  support
- Server Api tests with [Jest](https://jestjs.io/)
  and [Firebase emulator](https://firebase.google.com/docs/emulator-suite) to tests locally
- Gray box end-to-end testing with [Detox](https://github.com/wix/Detox)
- [Continuous integration](https://www.atlassian.com/continuous-delivery) with
  [Codemagic](https://codemagic.io/start/) to test, build and deploy for both Google Play and App Store

![react native](./asset/img/rn.png)
![flow](./asset/img/flow.png)
![storybook](./asset/img/storybook.png)
![jest](./asset/img/jest.png)
![gcloud](./asset/img/gcloud.png)
![firebase](./asset/img/firebase.png)
![detox](./asset/img/detox.png)
![codemagic](./asset/img/codemagic.png)

## Getting started

*We assume your environnement is ready to develop with react
native ([guide](https://reactnative.dev/docs/environment-setup)).*

### Install dependencies

`yarn install`

`cd ./server/functions && yarn install`

### Show all available scripts

`nps help` [see all scripts](scripts/scripts.md)

### Run project on android device

`nps android.start` or `react-native run-android`

### Run tests

`nps story.test_coverage`

`nps firebase.test_coverage`

`nps e2e.build_test_screenshots`

### Continuous Integration

*You must have linked your git repository with your codemagic project.*

Run tests on codemagic server

`git checkout some_story`

`git push`

Run tests and deploy on firebase and app stores.

`git checkout master`

`git tag x.x.x && git push x.x.x`

### [Gitlab Workflow](https://about.gitlab.com/blog/2016/10/25/gitlab-workflow-an-overview/)

![Gitlab Workflow](asset/img/workflow.png)

- Maintainer
  - Create issue with the story to implement
  - Eventually link an UI prototype created on [figma](figma.com)
- Developer
  - Create a merge request from the issue (auto create a dedicated branch)
  - Checkout the dedicated branch
  - Implement the story and push commits => trigger tests on codemagic server)
  - Eventually rebase, resolve conflicts, clean commit history
  - On label board move the issue on waiting for review
- Reviewer
  - Eventually annotate codes and send feeback
  - Approve and move the issue on label board to reviewed
- Maintainer
  - Merge to master
  - push release version tag => trigger tests and deploy
