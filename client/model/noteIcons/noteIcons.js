//@flow

export const noteIcons: {
  [NoteIcon]: {
    label: string,
    icon: number,
  }
} = {
  fun: {
    label: "Sympa",
    icon: require("./funBig.png"),
  },
  excellent: {
    label: "Excellent",
    icon: require("./excellentBig.png"),
  },
  masterpiece: {
    label: "Chef d'oeuvre",
    icon: require("./masterpieceBig.png"),
  },
};

export type NoteIcon = $Keys<typeof noteIcons>;
