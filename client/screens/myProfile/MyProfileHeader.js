//@flow
import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { useAppState } from "../../commons/appState";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { colors, margin, padding, wp } from "../../commons/css";
import { lightenColor } from "../../commons/util";
import { avatars } from "../../model/avatars/avatar";

export const MyProfileHeader = (props: {||}) => {
  const {
    avatar,
    color,
    pseudo,
    email,
  } = useAppState(appState => appState.user);
  return (<View
    style={{
      minHeight: wp(25),
      width: wp(100),
      flexDirection: "row",
      justifyContent: "space-between",
      alignSelf: "center",
      backgroundColor: lightenColor(color),
    }}
  >
    <Image
      testID="myProfileAvatar"
      source={avatars[avatar]}
      accessibilityLabel={avatar}
      style={{
        width: wp(21),
        height: wp(21),
        // borderColor: color,
        // borderWidth: wp(1),
        borderRadius: wp(21),
        ...padding(1),
        ...margin(2),
      }}
    />
    <View
      style={{
        justifyContent: "space-evenly",
        alignSelf: "center",
        width: wp(50),
      }}
    >
      <Text
        testID="myProfilePseudo"
        style={{
          fontSize: wp(10),
          color: colors.text,
          textAlign: "center",
        }}
      >
        {pseudo}
      </Text>
      <Text
        testID="myProfileEmail"
        style={{
          fontSize: wp(4),
          color: colors.text,
          textAlign: "center",
        }}
      >
        {email}
      </Text>
    </View>
    <TouchableOpacity
      onPress={() => {
        setScreen("ProfileEdit");
      }}
    >
      <Image
        source={require("../../commons/img/edit.png")}
        style={{
          width: wp(15),
          height: wp(15), ...margin(5),
        }}
      />
    </TouchableOpacity>
  </View>);
};

