//https://github.com/sezna/nps
//run 'npx update_scripts' to export to scripts sections of package.json

//You need to setup the react native environnement to run these scripts
//https://reactnative.dev/docs/environment-setup

//Sugar functions
const util = require("nps-utils");
const _ = (script, description) => ({ script, description });
const seq = (...args) => util.series(...args);
const par = (...args) => util.concurrent(...args);

const metro = (() => {
  const s = {
    kill: "kill-port 8081",//kill previous server
    reverse: "adb reverse tcp:8081 tcp:8081",//start adb server if none present
    start_reset_cache: "react-native start --host 0.0.0.0 --reset-cache",//serve javascript in debug mode
    start_cache: "react-native start --host 0.0.0.0",//serve javascript in debug mode reuse cache
    //https://www.npmjs.com/package/react-devtools
    devtools: _("adb reverse tcp:8097 tcp:8097 && react-devtools",
      "Open standalone react devtools. Debug must be enabled : run android:menu and tap debug. "),
    print_debug_keystore: "keytool -list -v -keystore \"%USERPROFILE%/.android/debug.keystore\" -alias androiddebugkey -storepass android -keypass android",
    print_prod_keystore: "keytool -list -v -keystore \"android/app/my-upload-key.keystore\"",
  };
  s.default = _(seq(s.kill, s.reverse, s.start_reset_cache),
    "Serve javascript in debug mode. Start adb server if none present");
  return s;
})();

const android = (() => {
  const s = {
    start: _("react-native run-android",
      "Start metro server. Compile, (re)install and launch native app"),
    reload: _("adb shell input text \"RR\"", "Reload javascript"),
    open_menu: _("adb shell input keyevent 82",
      "Open react native menu on app. Similar to shake the phone"),
    clean: "cd android && gradlew clean",
    release: _("react-native run-android --variant=release",
      "Build and launch apk in production mode"),
    build_release: seq("cd android", "gradlew assembleRelease", "cd .."),
    start_adb: seq("adb kill-server", "adb start-server"),
    devices_list: "adb devices",
    //Pixel_API_28_AOSP_DETOX created following https://github.com/wix/Detox/blob/master/docs/Introduction.AndroidDevEnv.md
    avd: ("emulator -avd Pixel_API_28_AOSP", "Start an android emulator."),
    avd_headless: "emulator -avd Pixel_API_28_AOSP -verbose -no-window -no-audio -gpu swiftshader_indirect",
  };
  return s;
})();

const stories = (() => {
  const s = {};
  s.update = _(
    "rnstl --pattern ./client/**/*.stories.{js,jsx} --outputFile client/commons/story/storyLoader.js",
    "Update the stories list in storyLoader.js. Run this command when you add a new story file.");
  const update = s.update.script;
  s.test = _(seq(update, "jest client/commons/story/storyshots.test.js --runInBand"),
    "Check stories against previous snapshots.");
  s.test_coverage = _(
    seq(update, "jest client/commons/story/storyshots.test.js --runInBand --coverage --verbose"),
    "Create coverage rapport at ./coverage/index.html");
  s.test_coverage_zip = seq(s.test_coverage.script, "zip -r client_coverage.zip coverage");
  s.snapshots = _(seq(update, "jest client/commons/story/storyshots.test.js --runInBand -u"),
    "Take a snapshot for each story.");
  return s;
})();

//Serverless platform with database(firestore), authentification, google functions (~https endpoints)
//https://rnfirebase.io/functions/writing-deploying-functions

// //for CI (detox) support, run once 'cd server/function && firebase login' on local machine
const firebaseLoginTokenPath = "server/login.token";
// let token = "";
// const fs = require("fs");
// if (fs.existsSync(firebaseLoginTokenPath))
//   token = " --token "+fs.readFileSync(firebaseLoginTokenPath);

const firebase = (() => {
  // const ci_token = "--token ";
  const cd = "cd ./server/functions";

  //These ports are used by the metro server and firebase
  const stop = seq("kill-port 8080", "kill-port 4000", "kill-port 5001");
  const s = {
    stop,
    // Codes change on google functions are automatically reloaded.
    emulator_import: seq(stop, cd,
      "firebase emulators:start --only functions,firestore,auth --import=./database"),
    emulator_no_import: seq(stop, cd,
      "firebase emulators:start --only functions,firestore,auth --token 1//03emtVnmcXO6xCgYIARAAGAMSNwF-L9Ire7hW92GeHy5FXoiGAINxcAgwPkrzU8zAun86KQTYBN21MpH-FFvwqz3FTRZEI0LAr30"),
    firestore_no_import: seq(stop, cd, "firebase emulators:start --only firestore"),
    //Js must transpiled to support flow syntax with babel.
    compile: seq(cd, "npx babel src --out-dir dist --source-maps"),
    // Transpile on js file change.
    watch: seq(cd, "npx babel src --out-dir dist --source-maps --watch"),
  };
  s.start_no_import = _(par({ babel: s.watch, firebase: s.emulator_no_import }),
    "Start firebase emulator with an empty database.");
  s.start_import = _(par({ babel: s.watch, firebase: s.emulator_import }),
    "Start firebase emulator with the last persisted database.");
  s.export = _(seq(cd, "firebase emulators:export --force ./database"),
    "Export database");
  s.deploy = _(seq(s.compile, "firebase functions:config:get",
    "firebase deploy --only functions"),
    "Deploy your google functions. Before check your environnement values are defined (firebase functions:config:get).");

  s.test_no_database = _(seq(cd, "jest src/__test__/ --runInBand"),
    "Tests google functions. Emulator must be started ");
  s.test_coverage_no_database = _(seq(cd, "jest src/__test__/ --runInBand --coverage --verbose"),
    "Tests google functions. Emulator must be started ");
  s.test = _(seq(stop, s.compile,
    "firebase emulators:exec --only functions,firestore \"cd ../../ && npx nps firebase.test_no_database\""),
    "Start firebase emulator then Tests google functions.");
  // s.test_coverage = seq(stop, s.compile,
  //   "firebase emulators:exec --only functions,firestore \"cd ../../ && npx nps firebase.test_coverage_no_database\"");
  s.test_coverage = seq(stop, s.compile, par({
      firebase: "firebase emulators:start --only functions,firestore || echo \"firebase has been killed\"",
      test: seq("jest src/__test__/ --runInBand --coverage --verbose", stop),
    },
  ));

  s.test_coverage_zip = seq(s.test_coverage, "zip -r server_coverage.zip coverage");

  return s;
})();

//End to end tests with detox. https://github.com/wix/Detox/tree/master/docs
//Only android
const e2e = (() => {

  /* Windows User Only */
  // Detox doesn't support Windows. You need to install :
  // - Install Windows Linux Subsystem 2 (wsl) + Ubuntu 20.04.1.
  //   https://docs.microsoft.com/en-us/windows/wsl/install-win10
  // - Setup Android environnement
  //   https://github.com/wix/Detox/blob/master/docs/Introduction.AndroidDevEnv.md

  const wsl = {
    adb_start: seq("adb kill-server", "adb -a nodaemon server start"),
    server_start: seq("kill-port 8099", "node e2e/DetoxServer.js"),
    test_usb: "bash -c -i \". ~/.profile ; ./scripts/e2e_wsl_usb.sh\"",
    test_emulator: "bash -c -i \". ~/.profile ; ./scripts/e2e_wsl_emulator.sh\"",
    devices_list: "bash -c -i \". ~/.profile ; adb devices\"",
  };
  const startAllServers = {
    adb: wsl.adb_start,
    detox_server: wsl.server_start,
    firebase: firebase.start_no_import.script,
    //adb server must be started for the following cmds
    metro: metro.default.script,
  };
  const startAllServersDescription = "Start servers: detox, adb, metro and firebase emulator. " +
    "Before you must run once 'nps e2e.wsl.build'.";
  wsl.start_usb = _(par({
      ...startAllServers,
      test: wsl.test_usb,
    }),
    "Start end to end test. Before you must connect a device to your host machine using usb cable. "
    + startAllServersDescription);

  wsl.restart_usb = _(par({
    //Start with empty firestore between tests.
    firebase: firebase.start_no_import.script,
    detox_test: wsl.test_usb,
  }), "Restart end to end test on usb attached device. Restart only firebase server.");

  //Before you must start an emulator: 'nps android.avd'
  wsl.start_emulator = par({
    ...startAllServers,
    test: wsl.test_emulator,
  });

  //Restart end to end test on emulator. Restart only firebase server.
  wsl.restart_attached = par({
    //Start with empty firestore between tests.
    firebase: firebase.start_no_import.script,
    detox_test: wsl.test_emulator,
  });

  /* All Operating System */
  const s = {
    wsl,
    build_debug: _(
      "cd android && gradlew assembleDebug assembleAndroidTest -DtestBuildType=debug && cd ..",
      "Build apk in debug mode. Run only if native code change."),
    build_release: "cd android && gradlew assembleRelease assembleAndroidTest -DtestBuildType=release && cd ..",
  };
  s.test_no_build = par({
    adb: "adb start-server",
    avd: "emulator -avd emulator -verbose -no-window -no-audio -gpu swiftshader_indirect || echo \"avd has been killed\"",
    firebase: firebase.start_no_import.script + " || echo \"firebase has been killed\"",
    metro: metro.default.script + " || echo \"metro has been killed\"",
    test: seq("detox test -l trace --configuration android.release --take-screenshots all",
      "kill-port 8080", //kill metro
      "kill-port 5001", //kill firebase
      "adb devices | grep emulator | cut -f1 | while read line; do adb -s $line emu kill; done"), //kill emulators
  });
  s.zip_screenshots = "zip -r screenshots.zip artifacts";

  s.build_test_screenshots = seq(
    s.build_release,
    s.test_no_build);

  s.build_test_zip = seq(
    s.build_test_screenshots,
    s.zip_screenshots);

  return s;
})();

const ci = (() => {
  const s = {
    decode_env_vars: seq(
      //compatibility with windows
      "chmod +x android/gradlew",
      "echo $FIREBASE_CREDENTIAL | base64 --decode > server/functions/shapito-de217-9eb33618b68e.json",
      "echo $ANDROID_GOOGLE_SERVICES | base64 --decode > android/app/google-services.json",
      "echo $ANDROID_KEYSTORE_PROPS | base64 --decode > android/keystore.properties",
      "echo $ANDROID_KEYSTORE | base64 --decode > android/app/my-upload-key.keystore",
    ),
    //Help to debug on CI machine
    //TODO add to .profile_bash
    //export PATH=\"./node_modules/.bin:$PATH\"
    //export PATH=\"./:$PATH\"
    //nps completion
  };

  return s;
})();

const packageScripts = {
  scripts: {
    metro,
    android,
    stories,
    e2e,
    firebase,
    ci,
  },
};
console.warn(
  "To see all available commands run 'nps'. You may have a look at scripts/package-scripts.js for further documentations.");
module.exports = packageScripts;
