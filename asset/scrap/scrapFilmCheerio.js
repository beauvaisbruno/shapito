// import axios from "react-native-axios";
const axios = require("react-native-axios");
const cheerio = require("react-native-cheerio");
// Cheerio
//request-promise
//https://cheerio.js.org/

const $log = (text) => {
  console.log(text.replace(/^\s*[\r\n]/gm, "").replace(/^ +/gm, ""));
};

axios.get("https://www.allocine.fr/recherche/?q=lost")
  // axios.get("https://equipage.tech")
  .then(function (response) {
    // console.log(response.data);
    const $ = cheerio.load(response.data);
    const movies = $("section[class='section movies-results'] li");
    const infos = [];
    movies.each(function (i, elem) {
      const info = {};
      infos.push(info);
      // const title = $(this).html();
      $log($(this).html());
      //   info.thumb = $(this).find(".thumbnail-container").children("img").eq(0).attr("data-src");
      // $log($(this).find(".thumbnail-container").children('img').eq(0).attr('data-src'));
      //   info.title = $(this).find(".meta-title-link").text();
      // $log($(this).find(".meta-title-link").text());
      // info.publicationDate = $(this).find(".meta-body .date").text();
      // info.link = $(this).find(".meta-title-link").eq(0).attr("href");
      // info.link = $(this).find(".meta-title").html();
      // info.link = $(this).find(".synopsis .content-txt").text();
      $(this).find(".rating-item-content").each(function (i, elem) {
        const noteType = $(this).find(".rating-title").text();
        if (noteType.toLowerCase().includes("spect")) {
          info.note = elem.find(".stareval-note").text();
        }

      });
      // info.summary = $(this).find(".synopsis .content-txt").text().trim();

      // $log($(this).find("img").html());
      return false;
    });
    console.log("description: ", infos);
  })
  .catch(function (error) {
    console.log(error);
  });

