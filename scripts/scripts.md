# Available Scripts

`nps metro.kill`

`nps metro.reverse`

`nps metro.start_reset_cache`

`nps metro.start_cache`

`nps metro.devtools`

*Open standalone react devtools. Debug must be enabled : run android:menu and tap debug.*

`nps metro`

*Serve javascript in debug mode. Start adb server if none present*

`nps android.start`

*Start metro server. Compile, (re)install and launch native app*

`nps android.reload`

*Reload javascript*

`nps android.open_menu`

*Open react native menu on app. Similar to shake the phone*

`nps android.clean`

`nps android.release`

*Build and launch apk in production mode*

`nps android.build_release`

`nps android.start_adb`

`nps android.devices_list`

`nps android.avd`

`nps android.avd_headless`

`nps stories.update`

*Update the stories list in storyLoader.js. Run this command when you add a new story file.*

`nps stories.test`

*Check stories against previous snapshots.*

`nps stories.test_coverage`

*Create coverage rapport at ./coverage/index.html*

`nps stories.test_coverage_zip`

`nps stories.snapshots`

*Take a snapshot for each story.*

`nps e2e.wsl.adb_start`

`nps e2e.wsl.server_start`

`nps e2e.wsl.test_usb`

`nps e2e.wsl.test_emulator`

`nps e2e.wsl.devices_list`

`nps e2e.wsl.start_usb`

*Start end to end test. Before you must connect a device to your host machine using usb cable. Start servers: detox,
adb, metro and firebase emulator. Before you must run once 'nps e2e.ws l.build'.*

`nps e2e.wsl.restart_usb`

*Restart end to end test on usb attached device. Restart only firebase server.*

`nps e2e.wsl.start_emulator`

`nps e2e.wsl.restart_attached`

`nps e2e.build_debug`

*Build apk in debug mode. Run only if native code change.*

`nps e2e.build_release`

`nps e2e.test_no_build`

`nps e2e.zip_screenshots`

`nps e2e.build_test_screenshots`

`nps e2e.build_test_zip`

`nps firebase.stop`

`nps firebase.emulator_import`

`nps firebase.emulator_no_import`

`nps firebase.firestore_no_import`

`nps firebase.compile`

`nps firebase.watch`

`nps firebase.start_no_import`

*Start firebase emulator with an empty database.*

`nps firebase.start_import`

*Start firebase emulator with the last persisted database.*

`nps firebase.export`

*Export database*

`nps firebase.deploy`

*Deploy your google functions. Before check your environnement values are defined (firebase functions:config:get).*

`nps firebase.test_no_database`

*Tests google functions. Emulator must be started*

`nps firebase.test_coverage_no_database`

*Tests google functions. Emulator must be started*

`nps firebase.test`

*Start firebase emulator then Tests google functions.*

`nps firebase.test_coverage`

`nps firebase.test_coverage_zip`

`nps ci.decode_env_vars`
