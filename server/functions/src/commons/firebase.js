//@flow

// https://firebase.google.com/docs/functions/write-firebase-functions
const admin = require("firebase-admin");
// console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
// console.trace();
if (process.env.JEST_WORKER_ID) {
  process.env.FIRESTORE_EMULATOR_HOST = "localhost:8080";
}
admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  projectId: "shapito-de217",
});
exports.admin = admin;

exports.firestore = admin.firestore();
