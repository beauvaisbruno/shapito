//@flow

const functions = require("firebase-functions");

exports.throwError = (msg: string, error: any) => {
  if (process.env.FUNCTIONS_EMULATOR || process.env.JEST_WORKER_ID) {
    console.error("error: ", msg, error);
    console.trace();
    throw Error(msg);
  } else {
    if (error && error.error) functions.logger.error(error.error);
    functions.logger.error(msg, { error });
    throw Error(msg + "\n" + JSON.stringify(error));
  }
};

exports.mergeToFieldArr = <T:{}> (obj: T, key: string, value: any): T => {
  let field = obj[key];
  if (!field) field = [];
  if (!field.includes(value)) field.push(value);
  obj[key] = field;
  return obj;
};

exports.removeFromFieldArr = <T:{}> (obj: T, key: string, value: any): T => {
  let field = obj[key];
  if (!field || !field.includes(value)) return obj;
  field.splice(field.indexOf(value), 1);
  return obj;
};
