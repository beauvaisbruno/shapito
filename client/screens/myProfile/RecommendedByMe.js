//@flow

import type { Element } from "react";
import React from "react";
import { Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import { useAppState } from "../../commons/appState";
import { setScreen } from "../../commons/components/ScreenNavigator";
import { colors, margin, padding, wp } from "../../commons/css";
import { lightenColor, lightenColor2 } from "../../commons/util";
import { avatars } from "../../model/avatars/avatar";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import type { TheItem, TheItemArrayProps } from "../../model/TheItem";
import type { TheUser } from "../../model/TheUser";
import { EmptyListLinks } from "./EmptyListLinks";

const iconPc = 7.3;
const iconStyle = {
  height: wp(iconPc),
  width: wp(iconPc), ...margin(0.5),
};

function ListAvatars ({
  listKey,
  icon,
  item,
  contacts,
}: {
  listKey: $Keys<TheItemArrayProps>,
  icon: number,
  item: TheItem,
  contacts: { [email: string]: TheUser },
}) {
  let rows = [];
  if (item[listKey]) {

    item[listKey].forEach((email) => {
      const contact = contacts[email];
      if (!contact) {
        console.warn("ByMe, unknown contact: ", item.recommendedByEmail, ", listKey: ", listKey,
          ", item: ", item);
        return;
      }
      rows.push(<View
        key={rows.length}
        style={{
          ...(item[listKey].length > 3 ? { width: wp(iconPc) / 2 } : {}),
        }}>
        <Image
          key={email} style={{
          height: wp(iconPc),
          width: wp(iconPc), ...margin(0.5),
        }}
          source={avatars[contact.avatar] || avatars.defaultAvatar} />
      </View>);
    });
  }

  if (rows.length > 4) {
    rows = rows.slice(0, 4);
    rows.push(<View key={"plus"} style={{
      ...(item[listKey].length > 3 ? { width: wp(iconPc) / 2 } : {}),
    }}>
      <Image
        key={"plus"} style={{
        height: wp(iconPc),
        width: wp(iconPc), ...margin(0.5),
        opacity: 0.5,
      }}
        source={require("../../commons/img/createBtn.png")} />
    </View>);
  }
  if (rows.length === 0) {
    return null;
  }

  return <View style={{ flexDirection: "row" }}>
    <Image key={"icon"} style={iconStyle}
           source={icon} />
    <View style={{ flexDirection: "row" }}>
      {rows}
    </View>
  </View>;
}

export const RecommendedByMe = ({ refreshControl }: { refreshControl: Element<any> }) => {
  const items = useAppState(appState => appState.items);
  const user = useAppState(appState => appState.user);
  const contacts = useAppState(appState => appState.contacts);

  const recommendedItems = [];
  user.recommendedByMeItemIds.forEach((itemId) => {
    if (items[itemId]) {
      recommendedItems.push(items[itemId]);
    } else {
      console.error("RecommendedByMe, unknown itemId: ", itemId);
    }
  });

  const sortedItem = recommendedItems.sort((item1, item2) => {
    // show most recent first
    return item2.creationDate - item1.creationDate;
  });

  let itemsCount = 0;
  const rows = [];
  // console.log("recommendedByMe: ",sortedItem.length);
  sortedItem.forEach(item => {

    let backgroundColor = lightenColor(user.color);
    //to always have adjacent card with diffrent background color
    if (itemsCount % 4 === 0 || itemsCount % 4 === 3) {
      backgroundColor = lightenColor2(user.color);
    }
    itemsCount++;

    rows.push(<TouchableOpacity
      key={item.itemId}
      style={{
        width: wp(50),
        minHeight: wp(35),
        backgroundColor,
      }}
      onPress={() => {
        setScreen("MyItem", {
          itemId: item.itemId,
        });
      }}>
      <View style={{ flexDirection: "row" }}>
        <View style={{ flex: 1, ...padding(1) }}>
          <Text
            style={{
              fontSize: wp(4),
              flexWrap: "wrap",
              textAlign: "center",
            }}>
            {item.title}
          </Text>
          {item.recommendedToEmails.length === 0 && <Text
            style={{
              fontSize: wp(4),
              flexWrap: "wrap",
              textAlign: "center",
              fontStyle: "italic",
              marginTop: wp(2),
            }}>
            {"Aucune recommandation."}
          </Text>}
          <ListAvatars {...{
            key: "recommendedToEmails",
            listKey: "recommendedToEmails",
            item,
            contacts,
            icon: require("../../commons/img/recommandation.png"),
          }} />
          <ListAvatars {...{
            key: "watchedEmails",
            listKey: "watchedEmails",
            item,
            contacts,
            icon: require("../../commons/img/eye.png"),
          }} />
          <ListAvatars {...{
            key: "wantToWatchEmails",
            listKey: "wantToWatchEmails",
            item,
            contacts,
            icon: require("../../commons/img/interest.png"),
          }} />
          <ListAvatars {...{
            key: "seenEmails",
            listKey: "seenEmails",
            item,
            contacts,
            icon: require("../../commons/img/ok.png"),
          }} />
        </View>

        <View>
          <AutoHeightImage
            source={{ uri: item.thumbUrl }}
            width={wp(15)}
            fallbackSource={itemTypes[item.type].defaultThumb}
          />

        </View>
      </View>
    </TouchableOpacity>);
  });

  if (rows.length === 0) {
    return <ScrollView
      style={{
        flex: 1,
        flexGrow: 1,
      }}
      contentContainerStyle={{
        flex: 1,
        alignItems: "center",
        paddingBottom: wp(20),
        flexGrow: 1,
      }}
    >
      <Text style={{
        fontSize: wp(5),
        fontStyle: "italic",
        color: colors.textGrey,
        textAlign: "center",
        width: wp(90),
      }}>
        {"Vous n'avez effectué aucune recommandation pour l'instant."}</Text>
      <EmptyListLinks />
    </ScrollView>;
  }

  return (<ScrollView
    {...{
      persistentScrollbar: true,
      refreshControl,
    }}
    style={{
      flex: 1,
      flexGrow: 1,
    }}
  >
    <View style={{
      flex: 1,
      flexDirection: "row",
      flexWrap: "wrap",
      paddingBottom: wp(20),
    }}>
      {rows}
    </View>
  </ScrollView>);
};

