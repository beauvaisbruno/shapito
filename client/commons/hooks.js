//@flow
import NetInfo from "@react-native-community/netinfo";
import type { Node } from "react";
import React, { useEffect, useRef, useState } from "react";
import { Dimensions, Keyboard, Text, View } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import type { ViewStyle } from "react-native/Libraries/StyleSheet/StyleSheet";
import { colors, wp } from "./css";

export function useSubScreen<T: { [string]: ((string)=>void)=>any }> (
  defaultSubScreen: $Keys<T>,
  subScreens: T,
): {
  SubScreen: Node,
  setSubScreen: ($Keys<T>)=>void,
  subScreen: $Keys<T>,
} {
  const [subScreen, setSubScreen] = useState(defaultSubScreen);
  const SubScreen = subScreens[subScreen](setSubScreen);
  return { setSubScreen, SubScreen, subScreen };
}

/**
 * Callback is called when wifi is back
 * @param callback
 */
export const useOnConnected = (callback: ()=>void): void => {
  const isConnectedRef = useRef(true);
  useEffect(() => {
    return NetInfo.addEventListener(state => {
      if (state.isConnected) {
        if (isConnectedRef.current === false) {
          callback();
        }
      }
      isConnectedRef.current = state.isConnected;
    });
  }, []);
};

export const useKeyboard = (): {| screenHeight: number, isKeyboard: boolean |} => {
  const [screenHeight, setHeight] = useState(0);
  const isKeyboard = screenHeight > 0;
  // console.log("searchHeight: ", searchHeight);
  useEffect(() => {
    const keyboardShowListener = Keyboard.addListener("keyboardDidShow", event => {
      setHeight(Dimensions.get("window").height - event.endCoordinates.height);
    });
    const keyboardHideListener = Keyboard.addListener("keyboardDidHide", event => {
      setHeight(0);
    });
    return () => {
      keyboardShowListener.remove();
      keyboardHideListener.remove();
    };
  }, []);
  return { screenHeight, isKeyboard };
};

export type LoaderType = ({ style?: ViewStyle })=>Node;

/**
 * Merge loading and error states and return a Comp to display a message or a spinner
 * @returns {{Loader: Node, setLoading, setError, loading, error}}
 */
export const useLoader = () => {

  const [loading: boolean | string, setLoading] = useState(false);
  const [error: boolean | string, setError] = useState(false);

  const Error: LoaderType = ({ style }: { style?: ViewStyle }) => {
    let text = "Operation impossible :(";
    if (typeof error === "string") {
      text = error;
    }
    return <View {...{
      style: {
        marginTop: wp(5),
        ...(style ? style : ({}: any)),
      },
    }}>
      <Text style={{ fontSize: wp(5), color: colors.errorMsg, textAlign: "center" }}>{text}</Text>
    </View>;
  };
  const Loading: LoaderType = ({ style }: { style?: ViewStyle }) => {
    // let text = "Chargement...";
    let text;
    if (typeof loading === "string") {
      text = loading;
    }
    return <View
      {...(style ? style : ({}: any))}
    >
      <AutoHeightImage
        style={{ alignSelf: "center" }}
        width={wp(30)}
        source={require("./img/loading.gif")} />
      {text && <Text style={{ fontSize: wp(5), textAlign: "center" }}>{text}</Text>}
    </View>;
  };

  const selectComp = (): LoaderType => {
    if (error) return Error;
    if (loading) return Loading;
    return () => null;
  };

  return {
    Loader: selectComp(),
    loading,
    error,
    setLoading: (value: boolean | string) => {
      setLoading(value);
      if (value) setError(false);
    }, setError: (value: boolean | string) => {
      setError(value);
      if (value) setLoading(false);
    },
  };
};
