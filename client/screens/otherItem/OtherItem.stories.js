//@flow
import { storiesOf } from "@storybook/react-native";
import React from "react";
import { getAppState } from "../../commons/appState";
import { generateContacts, generateMyItems } from "../../commons/story/generate";
import { MockScreen } from "../../commons/story/MockScreen";
import { mergeToFieldArr, pseudoRndReset } from "../../commons/util";

const user = {
  email: "shapitotest.1@gmail.com",
  color: "#2143bc",
  avatar: "marie",
  pseudo: "Bruno",
  updateTime: Date.now() - 1000,
  recommendedForMeItemIds: [],
  recommendedByMeItemIds: [],
};

pseudoRndReset();
const contacts = generateContacts({
  nbr: 1,
  contactEmails: ["contact@shapito.com"],
});

const items = generateMyItems({
  nbr: 1,
  itemId: "someItemId",
  recommendedByEmail: Object.keys(contacts)[0],
  recommendedToEmails: { [user.email]: (user: any) },
});
user.recommendedByMeItemIds = Object.keys(items);

const appState = {
  user,
  items,
  contacts,
};

storiesOf("OtherItem", module).add("OtherItem", () => {
  return (<MockScreen
    {...{
      screen: "OtherItem",
      appState,
      screenProps: {
        itemId: "someItemId",
      },
      remote: {
        updateItemFromContact: ({
          fieldToFill,
          itemId,
        }) => {
          let item = getAppState(appState => appState.items[itemId]);
          item = mergeToFieldArr({ ...item }, fieldToFill, user.email);
          return {

            item,
          };
        },
      },
    }}
  />);
});
