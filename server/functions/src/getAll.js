//@flow
import type { TheItem } from "../../../client/model/TheItem";
import type { TheUser } from "../../../client/model/TheUser";

const functions = require("firebase-functions");

const { getAllItems } = require("./commons/getAllItems");
const { getAllContacts } = require("./commons/getAllContacts");
const { checkUser } = require("./commons/checkUser");

export type GetAllResponse = {
  contacts: { [email: string]: TheUser },
  items: { [itemId: string]: TheItem },
  user: TheUser
};

exports.getAllFn = async ({
  user,
}: {
  user: TheUser
}) => {
  // console.log(user);
  return ({
    contacts: await getAllContacts(user.contactEmails),
    items: await getAllItems(user),
    user,
  }: GetAllResponse);
};

exports.getAll = functions.https.onCall(async (request, context) => {
  return await exports.getAllFn({
    user: await checkUser({ request, context }),
  });
});
