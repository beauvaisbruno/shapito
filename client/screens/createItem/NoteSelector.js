//@flow
import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { colors, margin, wp } from "../../commons/css";
import type { ItemType } from "../../model/ItemTypes/itemTypes";
import { itemTypes } from "../../model/ItemTypes/itemTypes";
import type { NoteIcon } from "../../model/noteIcons/noteIcons";
import { noteIcons } from "../../model/noteIcons/noteIcons";
import type { ItemDescription } from "../../model/TheItem";
import { CancelBtn } from "./CancelBtn";

export const NoteSelector = ({
    description,
    type,
    setNoteIcon,
  }: {
    description: ItemDescription,
    type: ItemType,
    setNoteIcon: (NoteIcon)=>void,
  },
) => {
// console.log("description.thumbUrl: ",description.thumbUrl);
  const rows = [];
  Object.keys(noteIcons).forEach((icon) => {
    const item = noteIcons[icon];
    rows.push(<TouchableOpacity
      style={{
        ...margin(wp(2)),
        width: wp(32),
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
      }} key={icon}
      onPress={() => {
        setNoteIcon(icon);
      }}
    >
      <Image source={item.icon} style={{
        width: wp(32),
        height: wp(32),
      }} />
      <Text style={{
        width: wp(32),
        fontSize: wp(5),
        color: colors.text,
        textAlign: "center",
      }}>{item.label}</Text>
    </TouchableOpacity>);
  });

  return (<View style={{
    flex: 1,
    alignItems: "center",
  }}>
    <View style={{
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center", ...margin(2),
      width: wp(96),
    }}>
      <Image style={{
        width: wp(15),
        height: wp(15),
      }} source={itemTypes[type].icon} />
      <Text style={{
        fontSize: description.title.length > 14 ? wp(5) : wp(10),
        color: colors.text,
      }}>{description.title}</Text>
      <CancelBtn />
    </View>
    <Text style={{
      fontSize: wp(7),
      color: colors.text,
      textAlign: "center",
    }}>Quelle est votre recommandation ?</Text>
    <View style={{
      flexDirection: "row",
      justifyContent: "space-evenly",
      alignItems: "flex-end", // flexWrap: "wrap",
      width: wp(100),
    }}>
      {rows}
    </View>
    {description.thumbUrl && <Image
      style={{
        width: wp(75),
        height: wp(75),
        resizeMode: "contain",
      }}
      source={{ uri: description.thumbUrl }}
    />}
  </View>);
};

