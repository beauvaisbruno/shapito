//@flow

import NetInfo from "@react-native-community/netinfo";
import React, { useEffect, useState } from "react";
import { Text } from "react-native";
import { colors, wp } from "../css";

export function DisconnectedMessage (props: {||}) {
  const [isConnected, setIsConnected] = useState(true);
  useEffect(() => {
    return NetInfo.addEventListener(state => {
      setIsConnected(state.isConnected);
    });
  }, []);
  if (isConnected) {
    return null;
  }
  return <Text
    style={{
      textAlign: "center",
      width: wp(100),
      color: "white",
      backgroundColor: colors.errorMsg,
    }}>
    {"Vous êtes hors ligne :("}
  </Text>;
}

